﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Repositories;

namespace Sppages.Repositories.Menus
{

    public class MenuRepository : Repository<Menu>
    {
        public MenuRepository(ApplicationDbContext context) : base(context)
        {
        }
    }    
}