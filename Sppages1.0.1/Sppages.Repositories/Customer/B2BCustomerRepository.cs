﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Sppages.Repositories.Customer
{
    
    public class B2BCustomerRepository : Repository<B2BCustomer>
    {
        public B2BCustomerRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}