﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Sppages.Repositories.Master
{
    
    public class B2CCustomerRepository : Repository<B2CCustomer>
    {
        public B2CCustomerRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}