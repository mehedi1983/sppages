﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Sppages.Repositories.Ad
{
    
    public class AdImageRepository : Repository<AdImage>
    {
        public AdImageRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}