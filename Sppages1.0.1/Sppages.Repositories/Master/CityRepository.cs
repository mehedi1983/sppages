﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Sppages.Repositories.Master
{
    
    public class CityRepository : Repository<City>
    {
        public CityRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}