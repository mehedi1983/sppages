﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Sppages.Repositories.Master
{
    
    public class CommentsRepository : Repository<Comments>
    {
        public CommentsRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}