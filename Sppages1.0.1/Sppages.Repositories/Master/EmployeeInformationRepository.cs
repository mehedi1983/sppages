﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Company;
using Sppages.Repositories;

namespace Sppages.Repositories.Master
{
    
    public class EmployeeInformationRepository : Repository<EmployeeInformation>
    {
        public EmployeeInformationRepository(ApplicationDbContext context) : base(context)
        {
        }
    }

}