﻿using System;
using System.Collections.Generic;
using System.Text;
using Sppages.Model.Menus;
using System.Collections.Generic;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Sppages.Model.Utility;

namespace Sppages.Model.Utility
{
    public class DTReturnContainer
    {
        public meta meta { get; set; }
        public object data { get; set; }
    }

    public class DTSearchReturnContainer
    {
        public int TotalRowsCount { get; set; }
        public object Data { get; set; }
    }

}
