﻿using System;
using System.Collections.Generic;
using System.Text;
using Sppages.Data.Entities.Ad;
using Sppages.Model.Master;

namespace Sppages.Model.Customer
{
    public class B2BCustomerModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string LocationId { get; set; }

        public string LocationName { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Website { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public List<AdPost> AdPostModels { get; set; }

    }
}
