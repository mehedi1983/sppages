﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sppages.Model.Ad
{
    public class AdPostSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string FeaturedImageUrl { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
        public DateTime? PublishDate { get; set; }

        public string LongDescription { get; set; }
        public string FeaturedImage { get; set; }
        public List<AdImageModel> Images { get; set; }

    }
}
