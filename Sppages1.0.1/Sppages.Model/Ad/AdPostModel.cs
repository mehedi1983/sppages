﻿using System;
using System.Collections.Generic;

namespace Sppages.Model.Ad
{
    public class AdPostModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
        public string B2BCustomerId { get; set; }
        public string B2BCustomerName { get; set; }
        public int? Age { get; set; }
        public string LongDescription { get; set; }
        public string Status { get; set; }
        public string SeoTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
        public List<AdImageModel> AdImageModels { get; set; }
        public List<AdPostTagModel> AdPostTagModels { get; set; }
    }

    
}