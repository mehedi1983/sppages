﻿using System;

namespace Sppages.Model.Ad
{
    public class AdPostTagModel
    {
        public string Id { get; set; }
        public string AdPostId { get; set; }
        public string AdPostName { get; set; }
        public string TagId { get; set; }
        public string TagName { get; set; }
        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}