﻿using System;

namespace Sppages.Model.Ad
{
    public class AdImageModel
    {
        public string Id { get; set; }
        public string AdPostId { get; set; }
        public string AdPostName { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
        public bool? IsFeatured { get; set; }
        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}