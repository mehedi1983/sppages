﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Model.Ad;
using Sppages.Model.Customer;
using Sppages.Model.Master;
using Sppages.Model.Menus;

namespace Sppages.Model.Factories
{
    public class ModelFactory: IModelFactory
    {
        #region "Master"

        public AppApplicationUserModel Create(AppApplicationUser obj)
        {
            return new AppApplicationUserModel
            {
                Id = obj.Id,
                ApplicationUserId = obj.Name,
                EmployeeId = obj.EmployeeId,
                Name = (obj.EmployeeInformation == null) ? "" : obj.EmployeeInformation.Name,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public AppApplicationUser Create(AppApplicationUserModel obj)
        {
            return new AppApplicationUser
            {
                Id = obj.Id,
                Name = obj.Name,
                EmployeeId = obj.EmployeeId,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public CommentsModel Create(Comments obj)
        {
            return new CommentsModel
            {
                Id = obj.Id,
                AdPostId = obj.AdPostId,
                AdPostName = (obj.AdPost==null)? "" : obj.AdPost.Name,
                Name = obj.Name,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public Comments Create(CommentsModel obj)
        {
            return new Comments
            {
                Id = obj.Id,
                Name = obj.Name,
                AdPostId = obj.AdPostId,
                EmailAddress = obj.EmailAddress,
                Message = obj.EmailAddress,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public EmployeeInformationModel Create(EmployeeInformation obj)
        {
            return new EmployeeInformationModel
            {
                Id = obj.Id,
                Name = obj.Name,
                EmailAddress = obj.EmailAddress,
                MobileNo = obj.MobileNo,
                Address = obj.Address,
                CityName = obj.CityName,
                ZipCode = obj.ZipCode,
                CountryName = obj.CountryName,
                Picture = obj.Picture,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
                AppApplicationUserModel = new List<AppApplicationUserModel>(obj.AppApplicationUsers.Select(Create)),
            };
        }

        public EmployeeInformation Create(EmployeeInformationModel obj)
        {
            return new EmployeeInformation
            {
                Id = obj.Id,
                Name = obj.Name,
                EmailAddress = obj.EmailAddress,
                MobileNo = obj.MobileNo,
                Address = obj.Address,
                CityName = obj.CityName,
                ZipCode = obj.ZipCode,
                CountryName = obj.CountryName,
                Picture = obj.Picture,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public CityModel Create(City obj)
        {
            return new CityModel
            {
                Id = obj.Id,
                Name = obj.Name,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public City Create(CityModel obj)
        {
            return new City
            {
                Id = obj.Id,
                Name = obj.Name,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public LocationModel Create(Location obj)
        {
            return new LocationModel
            {
                Id = obj.Id,
                Name = obj.Name,
                CityId = obj.CityId,
                CityName = (obj.City == null) ? "" : obj.City.Name,
                MetaDescription = obj.MetaDescription,
                MetaKeywords = obj.MetaKeywords,
                ShortDescription = obj.ShortDescription,
                LongDescription = obj.LongDescription,
                ParentId = obj.ParentId,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
                AdPostModels = new List<AdPostModel>(obj.AdPosts.Select(Create)),
            };
        }

        public Location Create(LocationModel obj)
        {
            return new Location
            {
                Id = obj.Id,
                Name = obj.Name,
                CityId = obj.CityId,
                MetaDescription = obj.MetaDescription,
                MetaKeywords = obj.MetaKeywords,
                ShortDescription = obj.ShortDescription,
                LongDescription = obj.LongDescription,
                ParentId = obj.ParentId,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }
        
        public SubscriberModel Create(Subscriber obj)
        {
            return new SubscriberModel
            {
                Id = obj.Id,
                EmailAddress = obj.EmailAddress,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate
            };
        }

        public Subscriber Create(SubscriberModel obj)
        {
            return new Subscriber
            {
                Id = obj.Id,
                EmailAddress = obj.EmailAddress,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public TagModel Create(Tag obj)
        {
            return new TagModel
            {
                Id = obj.Id,
                Title = obj.Title,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
                AdPostTagModel = new List<AdPostTagModel>(obj.AdPostTags.Select(Create)),
            };
        }

        public Tag Create(TagModel obj)
        {
            return new Tag
            {
                Id = obj.Id,
                Title = obj.Title,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        #endregion

        #region "Customer"

        public B2CCustomerModel Create(B2CCustomer obj)
        {
            return new B2CCustomerModel
            {
                Id = obj.Id,
                Name = obj.Name,
                LocationId = obj.LocationId,
                LocationName = (obj.Location == null) ? "" : obj.Location.Name,
                Email = obj.Email,
                PhoneNumber = obj.PhoneNumber,
                MobileNumber = obj.MobileNumber,
                Website = obj.Website,
                Address = obj.Address,
                ZipCode = obj.ZipCode,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public B2CCustomer Create(B2CCustomerModel obj)
        {
            return new B2CCustomer
            {
                Id = obj.Id,
                Name = obj.Name,
                LocationId = obj.LocationId,
                Email = obj.Email,
                PhoneNumber = obj.PhoneNumber,
                MobileNumber = obj.MobileNumber,
                Website = obj.Website,
                Address = obj.Address,
                ZipCode = obj.ZipCode,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public B2BCustomerModel Create(B2BCustomer obj)
        {
            return new B2BCustomerModel
            {
                Id = obj.Id,
                Name = obj.Name,
                LocationId = obj.LocationId,
                LocationName = (obj.Location == null) ? "" : obj.Location.Name,
                Email = obj.Email,
                PhoneNumber = obj.PhoneNumber,
                MobileNumber = obj.MobileNumber,
                Website = obj.Website,
                Address = obj.Address,
                ZipCode = obj.ZipCode,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public B2BCustomer Create(B2BCustomerModel obj)
        {
            return new B2BCustomer
            {
                Id = obj.Id,
                Name = obj.Name,
                LocationId = obj.LocationId,
                Email = obj.Email,
                PhoneNumber = obj.PhoneNumber,
                MobileNumber = obj.MobileNumber,
                Website = obj.Website,
                Address = obj.Address,
                ZipCode = obj.ZipCode,
                Status = obj.Status,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        #endregion

        #region "Menu"

        public FooterLinkModel Create(FooterLink obj)
        {
            return new FooterLinkModel
            {
                Id = obj.Id,
                Name = obj.Name,
                Url = obj.Url,
                Status = obj.Status,
                Order = obj.Order,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public FooterLink Create(FooterLinkModel obj)
        {
            return new FooterLink
            {
                Id = obj.Id,
                Name = obj.Name,
                Url = obj.Url,
                Status = obj.Status,
                Order = obj.Order,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public MenuModel Create(Menu obj)
        {
            return new MenuModel
            {
                Id = obj.Id,
                Name = obj.Name,
                Description = obj.Description,
                Status = obj.Status,
                Order = obj.Order,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public Menu Create(MenuModel obj)
        {
            return new Menu
            {
                Id = obj.Id,
                Name = obj.Name,
                Description = obj.Description,
                Status = obj.Status,
                Order = obj.Order,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }
        
        #endregion

        #region "Ad"

        public AdImageModel Create(AdImage obj)
        {
            return new AdImageModel
            {
                Id = obj.Id,
                AdPostId = obj.AdPostId,
                AdPostName = (obj.AdPost == null) ? "" : obj.AdPost.Name,
                Title = obj.Title,
                Url = obj.Url,
                ThumbnailUrl = obj.ThumbnailUrl,
                IsFeatured = obj.IsFeatured,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate
            };
        }

        public AdImage Create(AdImageModel obj)
        {
            return new AdImage
            {
                Id = obj.Id,
                AdPostId = obj.AdPostId,
                Title = obj.Title,
                Url = obj.Url,
                ThumbnailUrl = obj.ThumbnailUrl,
                IsFeatured = obj.IsFeatured,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate,
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public AdPostModel Create(AdPost obj)
        {
            return new AdPostModel
            {
                Id = obj.Id,
                Name = obj.Name,
                LocationId = obj.LocationId,
                LocationName = (obj.Location == null) ? "" : obj.Location.Name,
                B2BCustomerId = obj.B2BCustomerId,
                B2BCustomerName = (obj.B2BCustomer == null) ? "" : obj.B2BCustomer.Name,
                Age = obj.Age,
                LongDescription = obj.LongDescription,
                Status = obj.Status,
                SeoTitle = obj.SeoTitle,
                MetaDescription = obj.MetaDescription,
                MetaKeywords = obj.MetaKeywords,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
                AdImageModels = new List<AdImageModel>(obj.AdImages.Select(Create)),
                AdPostTagModels = new List<AdPostTagModel>(obj.AdPostTags.Select(Create))
            };
        }

        public AdPost Create(AdPostModel obj)
        {
            return new AdPost
            {
                Id = obj.Id,
                B2BCustomerId = obj.B2BCustomerId,
                Name = obj.Name,
                LocationId = obj.LocationId,
                Age = obj.Age,
                LongDescription = obj.LongDescription,
                Status = obj.Status,
                SeoTitle = obj.SeoTitle,
                MetaDescription = obj.MetaDescription,
                MetaKeywords = obj.MetaKeywords,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }

        public AdPostTagModel Create(AdPostTag obj)
        {
            return new AdPostTagModel
            {
                Id = obj.Id,
                AdPostId = obj.AdPostId,
                AdPostName = obj.AdPost.Name,
                TagId = obj.TagId,
                TagName = obj.Tag.Title,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = obj.CreatedDate.Value.ToString("MM/dd/yyyy"),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate
            };
        }

        public AdPostTag Create(AdPostTagModel obj)
        {
            return new AdPostTag
            {
                Id = obj.Id,
                AdPostId = obj.AdPostId,
                TagId = obj.TagId,
                RecStatus = obj.RecStatus,
                CreatedBy = obj.CreatedBy,
                CreatedDate = Convert.ToDateTime(obj.CreatedDate),
                ModifiedBy = obj.ModifiedBy,
                ModifiedDate = obj.ModifiedDate,
            };
        }
        
        #endregion
        
    }
}