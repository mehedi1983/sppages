﻿using System.Collections.Generic;
using System.Linq;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Model.Ad;
using Sppages.Model.Customer;
using Sppages.Model.Master;
using Sppages.Model.Menus;

namespace Sppages.Model.Factories
{
    public interface IModelFactory
    {
        #region "Master"

        AppApplicationUserModel Create(AppApplicationUser obj);

        AppApplicationUser Create(AppApplicationUserModel obj);

        CommentsModel Create(Comments obj);

        Comments Create(CommentsModel obj);

        EmployeeInformationModel Create(EmployeeInformation obj);

        EmployeeInformation Create(EmployeeInformationModel obj);

        CityModel Create(City obj);

        City Create(CityModel obj);

        LocationModel Create(Location obj);

        Location Create(LocationModel obj);

        SubscriberModel Create(Subscriber obj);

        Subscriber Create(SubscriberModel obj);

        TagModel Create(Tag obj);

        Tag Create(TagModel obj);

        #endregion

        #region "Customer"

        B2CCustomerModel Create(B2CCustomer obj);

        B2CCustomer Create(B2CCustomerModel obj);

        B2BCustomerModel Create(B2BCustomer obj);

        B2BCustomer Create(B2BCustomerModel obj);

        #endregion

        #region "Menu"

        FooterLinkModel Create(FooterLink obj);

        FooterLink Create(FooterLinkModel obj);

        MenuModel Create(Menu obj);

        Menu Create(MenuModel obj);

        #endregion

        #region "Ad"

        AdImageModel Create(AdImage obj);

        AdImage Create(AdImageModel obj);

        AdPostModel Create(AdPost obj);

        AdPost Create(AdPostModel obj);

        AdPostTagModel Create(AdPostTag obj);

        AdPostTag Create(AdPostTagModel obj);

        #endregion
    }
}