﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sppages.Model.Menus
{
    public class MenuSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
