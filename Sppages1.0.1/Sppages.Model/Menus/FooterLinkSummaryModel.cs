﻿namespace Sppages.Model.Menus
{
    public class FooterLinkSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}