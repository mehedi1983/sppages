﻿using System;

namespace Sppages.Model.Menus
{
    public class FooterLinkModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }

        public string Status { get; set; }

        public int? Order { get; set; }

        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}