﻿using System;
using System.Collections.Generic;
using System.Text;
using Sppages.Data.Entities.Identity;

namespace Sppages.Model.Master
{
    public class AppApplicationUserModel
    {
        public string Id { get; set; }
        public string ApplicationUserId { get; set; }

        public string EmployeeId { get; set; }

        public string Name { get; set; }

        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}
