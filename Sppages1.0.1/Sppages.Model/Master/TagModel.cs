﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sppages.Model.Ad;

namespace Sppages.Model.Master
{
    public class TagModel
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
        public virtual List<AdPostTagModel> AdPostTagModel { get; set; }
    }
}