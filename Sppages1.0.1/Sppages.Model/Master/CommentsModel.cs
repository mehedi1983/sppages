﻿using System;
using Sppages.Model.Ad;

namespace Sppages.Model.Master
{
    public class CommentsModel
    {
        public string Id { get; set; }

        public string AdPostId { get; set; }

        public string AdPostName { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string Message { get; set; }

        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
        public string PostedDay { get; set; }
        public string PostedMonth { get; set; }
        public string PostedYear { get; set; }

    }
}