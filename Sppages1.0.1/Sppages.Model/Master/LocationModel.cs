﻿using System;
using System.Collections.Generic;
using Sppages.Model.Ad;
using Sppages.Model.Menus;

namespace Sppages.Model.Master
{
    public class LocationModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CityId { get; set; }
        public string CityName { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ParentId { get; set; }
        public string Status { get; set; }
        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
        public List<MenuModel> MenuModels { get; set; }
        public List<FooterLinkModel> FooterLinkModels { get; set; }
        public List<AdPostModel> AdPostModels { get; set; }
    }
}