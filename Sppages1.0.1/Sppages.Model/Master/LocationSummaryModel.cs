﻿using System;

namespace Sppages.Model.Master
{
    public class LocationSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int? NoOfAds { get; set; }
    }

    public class LocSummaryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string LocationId { get; set; }
        public string LocationName { get; set; }
        public DateTime? PublishDate { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string LongDescription { get; set; }
        public string FeaturedImage { get; set; }
        public string Status { get; set; }
    }
}