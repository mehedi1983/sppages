﻿using System;
using System.Collections.Generic;

namespace Sppages.Model.Master
{
    public class CityModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

    }
}