﻿using System;

namespace Sppages.Model.Master
{
    public class SubscriberModel
    {
        public string Id { get; set; }

        public string EmailAddress { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }
    }
}