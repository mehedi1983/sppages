﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Sppages.Model.Master
{
    public class EmployeeInformationModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string MobileNo { get; set; }

        public string Address { get; set; }

        public string CityName { get; set; }

        public string ZipCode { get; set; }

        public string CountryName { get; set; }

        public string Picture { get; set; }

        public string Status { get; set; }

        public string RecStatus { get; set; }

        public string CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public virtual List<AppApplicationUserModel> AppApplicationUserModel { get; set; }
    }
}