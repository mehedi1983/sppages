﻿using System;
using Invoice.Service.Interface;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Repositories;
using Sppages.Repositories.Ad;
using Sppages.Repositories.Master;
using Sppages.Repositories.Menus;

namespace Sppages.Service.Derived
{
    public class MenuService : IMenuService, IDisposable
    {
        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public MenuService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Menu"
        private Repository<FooterLink> _FooterLinkRepository;
        private Repository<Menu> _MenuRepository;
        #endregion

        #region "Location"
        private Repository<Location> _LocationRepository;
        #endregion

        #endregion

        #region "Menu"

        public Repository<FooterLink> FooterLinkRepository
        {
            get { return _FooterLinkRepository ?? (_FooterLinkRepository = new FooterLinkRepository(_context)); }
        }

        public Repository<Menu> MenuRepository
        {
            get { return _MenuRepository ?? (_MenuRepository = new MenuRepository(_context)); }
        }

        #endregion

        #region "Location"

        public Repository<Location> LocationRepository
        {
            get { return _LocationRepository ?? (_LocationRepository = new LocationRepository(_context)); }
        }

        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}