﻿using System;
using Invoice.Service.Interface;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Repositories;
using Sppages.Repositories.Ad;
using Sppages.Repositories.Master;
using Sppages.Repositories.Menus;

namespace Sppages.Service.Derived
{
    public class FrontEndService : IFrontEndService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public FrontEndService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        private Repository<Location> _LocationRepository;
        private Repository<Tag> _TagRepository;
        private Repository<AdPost> _AdPostRepository;
        private Repository<Comments> _CommentsRepository;
        private Repository<AdPostTag> _AdPostTagRepository;
        private Repository<FooterLink> _FooterLinkRepository;
        private Repository<Menu> _MenuRepository;

        public Repository<Menu> MenuRepository
        {
            get { return _MenuRepository ?? (_MenuRepository = new MenuRepository(_context)); }
        }

        public Repository<FooterLink> FooterLinkRepository
        {
            get { return _FooterLinkRepository ?? (_FooterLinkRepository = new FooterLinkRepository(_context)); }
        }

        public Repository<AdPostTag> AdPostTagRepository
        {
            get { return _AdPostTagRepository ?? (_AdPostTagRepository = new AdPostTagRepository(_context)); }
        }

        public Repository<Location> LocationRepository
        {
            get { return _LocationRepository ?? (_LocationRepository = new LocationRepository(_context)); }
        }

        public Repository<Comments> CommentsRepository
        {
            get { return _CommentsRepository ?? (_CommentsRepository = new CommentsRepository(_context)); }
        }

        public Repository<Tag> TagRepository
        {
            get { return _TagRepository ?? (_TagRepository = new TagRepository(_context)); }
        }

        public Repository<AdPost> AdPostRepository
        {
            get { return _AdPostRepository ?? (_AdPostRepository = new AdPostRepository(_context)); }
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}