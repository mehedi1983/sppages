﻿using System;
using Invoice.Service.Interface;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Ad;
using Sppages.Repositories.Master;

namespace Sppages.Service.Derived
{
    public class AdService: IAdService
    {
        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public AdService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Ad"
        private Repository<AdImage> _AdImageRepository;
        private Repository<AdPost> _AdPostRepository;
        private Repository<Tag> _TagRepository;
        private Repository<AdPostTag> _AdPostTagRepository;
        private Repository<Comments> _CommentsRepository;
        #endregion

        #region "Location"
        private Repository<Location> _LocationRepository;
        #endregion

        #endregion

        #region "Ad"

        public Repository<Comments> CommentsRepository
        {
            get { return _CommentsRepository ?? (_CommentsRepository = new CommentsRepository(_context)); }
        }

        public Repository<AdImage> AdImageRepository
        {
            get { return _AdImageRepository ?? (_AdImageRepository = new AdImageRepository(_context)); }
        }

        public Repository<AdPost> AdPostRepository
        {
            get { return _AdPostRepository ?? (_AdPostRepository = new AdPostRepository(_context)); }
        }

        public Repository<Tag> TagRepository
        {
            get { return _TagRepository ?? (_TagRepository = new TagRepository(_context)); }
        }

        public Repository<AdPostTag> AdPostTagRepository
        {
            get { return _AdPostTagRepository ?? (_AdPostTagRepository = new AdPostTagRepository(_context)); }
        }

        #endregion

        #region "Location"
        public Repository<Location> LocationRepository
        {
            get { return _LocationRepository ?? (_LocationRepository = new LocationRepository(_context)); }
        }
        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}