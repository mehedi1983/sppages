﻿using System;
using System.Collections.Generic;
using System.Text;
using Invoice.Service.Interface;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Ad;
using Sppages.Repositories.Master;

namespace Sppages.Service.Derived
{
    public class MasterService : IMasterService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public MasterService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Master"
        private Repository<Tag> _TagRepository;
        private Repository<AppApplicationUser> _AppApplicationUserRepository;
        private Repository<Comments> _CommentsRepository;
        private Repository<EmployeeInformation> _EmployeeInformationRepository;
        private Repository<Location> _LocationRepository;
        private Repository<Subscriber> _SubscriberRepository;
        private Repository<AdPost> _AdPostRepository;
        #endregion

        #endregion

        #region "Master"

        public Repository<AdPost> AdPostRepository
        {
            get { return _AdPostRepository ?? (_AdPostRepository = new AdPostRepository(_context)); }
        }

        public Repository<Tag> TagRepository
        {
            get { return _TagRepository ?? (_TagRepository = new TagRepository(_context)); }
        }

        public Repository<AppApplicationUser> AppApplicationUserRepository
        {
            get { return _AppApplicationUserRepository ?? (_AppApplicationUserRepository = new AppApplicationUserRepository(_context)); }
        }

        public Repository<Comments> CommentsRepository
        {
            get { return _CommentsRepository ?? (_CommentsRepository = new CommentsRepository(_context)); }
        }

        public Repository<EmployeeInformation> EmployeeInformationRepository
        {
            get { return _EmployeeInformationRepository ?? (_EmployeeInformationRepository = new EmployeeInformationRepository(_context)); }
        }

        public Repository<Location> LocationRepository
        {
            get { return _LocationRepository ?? (_LocationRepository = new LocationRepository(_context)); }
        }

        public Repository<Subscriber> SubscriberRepository
        {
            get { return _SubscriberRepository ?? (_SubscriberRepository = new SubscriberRepository(_context)); }
        }

        #endregion

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
