﻿using System;
using Invoice.Service.Interface;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Ad;
using Sppages.Repositories.Customer;
using Sppages.Repositories.Master;

namespace Sppages.Service.Derived
{
    public class B2BCustomerService : IB2BCustomerService
    {
        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public B2BCustomerService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        private Repository<AdPost> _AdPostRepository;
        private Repository<B2BCustomer> _B2BCustomerRepository;

        public Repository<AdPost> AdPostRepository
        {
            get { return _AdPostRepository ?? (_AdPostRepository = new AdPostRepository(_context)); }
        }

        public Repository<B2BCustomer> B2BCustomerRepository
        {
            get { return _B2BCustomerRepository ?? (_B2BCustomerRepository = new B2BCustomerRepository(_context)); }
        }

        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}