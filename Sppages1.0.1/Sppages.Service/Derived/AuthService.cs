﻿using System;
using System.Collections.Generic;
using System.Text;
using Invoice.Service.Interface;
using Microsoft.EntityFrameworkCore;
using Sppages.Data.Entities;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Customer;
using Sppages.Repositories.Master;

namespace Invoice.Service.Derived
{
    public class AuthService: IAuthService, IDisposable
    {

        #region "AppDbContext variable"
        private readonly ApplicationDbContext _context;
        public AuthService(ApplicationDbContext appDbContext)
        {
            _context = appDbContext;
        }
        #endregion

        #region "Private Variable"

        #region "Employee"
        private Repository<EmployeeInformation> _EmployeeInformationRepository;
        private Repository<AppApplicationUser> _AppApplicationUserRepository;

        private Repository<B2CCustomer> _B2CCustomerRepository;
        private Repository<B2BCustomer> _B2BCustomerRepository;
        #endregion

        #endregion

        #region "Master"

        public Repository<EmployeeInformation> EmployeeInformationRepository
        {
            get { return _EmployeeInformationRepository ?? (_EmployeeInformationRepository = new EmployeeInformationRepository(_context)); }
        }

        public Repository<AppApplicationUser> AppApplicationUserRepository
        {
            get { return _AppApplicationUserRepository ?? (_AppApplicationUserRepository = new AppApplicationUserRepository(_context)); }
        }

        #endregion


        #region "Customer"

        public Repository<B2CCustomer> B2CCustomerRepository
        {
            get { return _B2CCustomerRepository ?? (_B2CCustomerRepository = new B2CCustomerRepository(_context)); }
        }

        public Repository<B2BCustomer> B2BCustomerRepository
        {
            get { return _B2BCustomerRepository ?? (_B2BCustomerRepository = new B2BCustomerRepository(_context)); }
        }

        #endregion


        public int Commit()
        {
            return _context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                    }
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
