﻿using System;
using System.Collections.Generic;
using System.Text;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Master;

namespace Invoice.Service.Interface
{
    public interface IAuthService
    {

        #region "Employee Information"
        Repository<EmployeeInformation> EmployeeInformationRepository { get; }
        Repository<AppApplicationUser> AppApplicationUserRepository { get; }

        Repository<B2CCustomer> B2CCustomerRepository { get; }
        Repository<B2BCustomer> B2BCustomerRepository { get; }
        #endregion

        int Commit();
    }
}
