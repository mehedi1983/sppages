﻿using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;
using Sppages.Repositories.Ad;

namespace Invoice.Service.Interface
{
    public interface IMasterService
    {
        #region "Master"
        Repository<AppApplicationUser> AppApplicationUserRepository { get; }
        Repository<Comments> CommentsRepository { get; }
        Repository<EmployeeInformation> EmployeeInformationRepository { get; }
        Repository<Location> LocationRepository { get; }
        Repository<Subscriber> SubscriberRepository { get; }
        Repository<Tag> TagRepository { get; }
        Repository<AdPost> AdPostRepository { get; }
        #endregion

        int Commit();
    }
}