﻿using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Invoice.Service.Interface
{
    public interface IB2BCustomerService
    {
        Repository<AdPost> AdPostRepository { get; }
        Repository<B2BCustomer> B2BCustomerRepository { get; }
        int Commit();
        void Dispose(bool disposing);
        void Dispose();
    }
}