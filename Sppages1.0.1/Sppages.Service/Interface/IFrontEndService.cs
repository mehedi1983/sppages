﻿using System;
using System.Collections.Generic;
using System.Text;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Repositories;

namespace Invoice.Service.Interface
{
    public interface IFrontEndService
    {

        #region ""
        Repository<Location> LocationRepository { get; }
        Repository<Tag> TagRepository { get; }
        Repository<AdPost> AdPostRepository { get; }
        Repository<Comments> CommentsRepository { get; }
        Repository<AdPostTag> AdPostTagRepository { get; }
        Repository<FooterLink> FooterLinkRepository { get; }
        Repository<Menu> MenuRepository { get; }
        #endregion

        int Commit();
    }
}
