﻿using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Master;
using Sppages.Repositories;

namespace Invoice.Service.Interface
{
    public interface IAdService
    {
        Repository<AdImage> AdImageRepository { get; }
        Repository<AdPost> AdPostRepository { get; }
        Repository<Tag> TagRepository { get; }
        Repository<AdPostTag> AdPostTagRepository { get; }
        Repository<Location> LocationRepository { get; }
        Repository<Comments> CommentsRepository { get; }
        int Commit();
        void Dispose(bool disposing);
        void Dispose();
    }
}