﻿using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;
using Sppages.Repositories;

namespace Invoice.Service.Interface
{
    public interface IMenuService
    {
        Repository<FooterLink> FooterLinkRepository { get; }
        Repository<Menu> MenuRepository { get; }
        Repository<Location> LocationRepository { get; }
        int Commit();
        void Dispose(bool disposing);
        void Dispose();
    }
}