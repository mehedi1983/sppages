﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Ad
{
    public class AdPost : AuditableEntity
    {
        public AdPost()
        {
            AdImages = new HashSet<AdImage>();
            AdPostTags = new List<AdPostTag>();
            Commentses = new List<Comments>();
        }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(128)]
        public string B2BCustomerId { get; set; }

        [StringLength(128)]
        public string LocationId { get; set; }

        public int? Age { get; set; }
        public string LongDescription { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(200)]
        public string SeoTitle { get; set; }

        [StringLength(200)]
        public string MetaKeywords { get; set; }

        [StringLength(500)]
        public string MetaDescription { get; set; }

        [ForeignKey("B2BCustomerId")]
        public virtual B2BCustomer B2BCustomer { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        public virtual ICollection<AdImage> AdImages { get; set; }

        public virtual ICollection<AdPostTag> AdPostTags { get; set; }

        public virtual ICollection<Comments> Commentses { get; set; }

    }
}