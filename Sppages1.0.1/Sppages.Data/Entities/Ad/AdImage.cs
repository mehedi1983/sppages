﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;

namespace Sppages.Data.Entities.Ad
{
    public class AdImage : AuditableEntity
    {
        [StringLength(128)]
        public string AdPostId { get; set; }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Url { get; set; }

        [StringLength(500)]
        public string ThumbnailUrl { get; set; }

        public bool? IsFeatured { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [ForeignKey("AdPostId")]
        public virtual AdPost AdPost { get; set; }
    }
}