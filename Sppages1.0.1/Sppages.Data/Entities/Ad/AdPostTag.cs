﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Ad
{
    public class AdPostTag : AuditableEntity
    {
        public AdPostTag()
        {
        }

        [StringLength(128)]
        public string AdPostId { get; set; }

        [StringLength(128)]
        public string TagId { get; set; }

        [ForeignKey("AdPostId")]
        public virtual AdPost AdPost { get; set; }

        [ForeignKey("TagId")]
        public virtual Tag Tag { get; set; }
        
    }
}