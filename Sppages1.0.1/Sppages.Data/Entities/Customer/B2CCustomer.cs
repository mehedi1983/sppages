﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Identity;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Customer
{
    public class B2CCustomer : AuditableEntity
    {
        public B2CCustomer()
        {
            ApplicationUsers = new HashSet<ApplicationUser>();
        }

        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string PhoneNumber { get; set; }

        [StringLength(50)]
        public string MobileNumber { get; set; }

        [StringLength(100)]
        public string Website { get; set; }

        [StringLength(500)]
        public string Address { get; set; }

        [StringLength(50)]
        public string ZipCode { get; set; }

        [StringLength(128)]
        public string LocationId { get; set; }

        [ForeignKey("LocationId")]
        public virtual Location Location { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [JsonIgnore]
        public virtual ICollection<ApplicationUser> ApplicationUsers { get; set; }

    }
}