﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Identity
{
    public class ApplicationUser:IdentityUser
    {

        public ApplicationUser()
        {
            AppApplicationUsers = new HashSet<AppApplicationUser>();
        }

        [StringLength(128)]
        public string B2CCustomerId { get; set; }

        [StringLength(128)]
        public string B2BCustomerId { get; set; }
       
        [ForeignKey("B2CCustomerId")]
        public virtual B2CCustomer B2CCustomer { get; set; }

        [ForeignKey("B2BCustomerId")]
        public virtual B2BCustomer B2BCustomer { get; set; }

        //public string ProfileImagePath { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [MinLength(2), MaxLength(128)]
        public string DisplayName { get; set; }

        public string GetDisplayName()
        {
            return (string.IsNullOrWhiteSpace(DisplayName) ? Email : DisplayName);
        }

        public virtual ICollection<AppApplicationUser> AppApplicationUsers { get; set; }
    }  
    
}
