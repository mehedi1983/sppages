﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Sppages.Data.Entities.Identity
{
    public static class IdentityExtensions
    {

        public static ApplicationUser AdditionalInfo(this IIdentity identity)
        {
            if (identity == null)
            {
                throw new ArgumentNullException("identity");
            }


            var claim1 = ((ClaimsIdentity)identity).FindFirst("FirstName");



            ApplicationUser user = null;
            var claims = ((System.Security.Claims.ClaimsIdentity)identity).Claims;
            //var appUserClaim = claims.
            foreach (var claim in claims)
            {
                if (claim.Type == "ApplicationUser")
                {
                    user = Newtonsoft.Json.JsonConvert.DeserializeObject<ApplicationUser>(claim.Value);
                    break;
                }
            }

            return user;
        }
    }
}
