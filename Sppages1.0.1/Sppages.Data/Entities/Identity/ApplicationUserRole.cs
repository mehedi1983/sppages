﻿using Microsoft.AspNetCore.Identity;

namespace Sppages.Data.Entities.Identity
{
    public class ApplicationUserRole : IdentityRole
    {
        public string Description { get; set; }
    }
}
