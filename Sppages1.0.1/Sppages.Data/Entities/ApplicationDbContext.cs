﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Customer;
using Sppages.Data.Entities.Identity;
using Sppages.Data.Entities.Master;
using Sppages.Data.Entities.Menus;

namespace Sppages.Data.Entities
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        #region "Menu"
        public virtual DbSet<B2BCustomer> B2BCustomers { get; set; }
        public virtual DbSet<B2CCustomer> B2CCustomers { get; set; }
        #endregion

        #region "Ad"
        public virtual DbSet<AdPost> AdPosts { get; set; }
        public virtual DbSet<AdImage> AdImages { get; set; }
        public virtual DbSet<AdPostTag> AdPostTags { get; set; }
        #endregion

        #region "Master"
        public virtual DbSet<AppApplicationUser> AppApplicationUsers { get; set; }
        public virtual DbSet<Comments> Comments { get; set; }
        public virtual DbSet<EmployeeInformation> EmployeeInformations { get; set; }
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Subscriber> Subscribers { get; set; }
        public virtual DbSet<Tag> Tags { get; set; }
        #endregion

        #region "Menu"
        public virtual DbSet<FooterLink> FooterLinks { get; set; }
        public virtual DbSet<Menu> Menus { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region "Customer"
            modelBuilder.Entity<B2BCustomer>().ToTable("B2BCustomer");
            modelBuilder.Entity<B2CCustomer>().ToTable("B2CCustomer");
            #endregion
            
            #region "Ad"
            modelBuilder.Entity<AdPost>().ToTable("AdPost");
            modelBuilder.Entity<AdImage>().ToTable("AdImage");
            modelBuilder.Entity<AdPostTag>().ToTable("AdPostTag");
            #endregion

            #region "Master"
            modelBuilder.Entity<AppApplicationUser>().ToTable("AppApplicationUser");
            modelBuilder.Entity<Comments>().ToTable("Comments");
            modelBuilder.Entity<EmployeeInformation>().ToTable("EmployeeInformation");
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Location>().ToTable("Location");
            modelBuilder.Entity<Subscriber>().ToTable("Subscriber");
            modelBuilder.Entity<Tag>().ToTable("Tag");
            #endregion

            #region "Menu"
            modelBuilder.Entity<FooterLink>().ToTable("FooterLink");
            modelBuilder.Entity<Menu>().ToTable("Menu");
            #endregion

            #region "Seed Data"

            modelBuilder.Entity<ApplicationRole>().HasData(
                new {Id = "1", Name = "SMAdmin", NormalizedName = "SMADMIN"},
                new {Id = "2", Name = "Admin", NormalizedName = "ADMIN"},
                new { Id = "3", Name = "B2BCustomer", NormalizedName = "B2BCustomer" },
                new { Id = "4", Name = "B2CCustomer", NormalizedName = "B2CCustomer" }
                );

            #endregion

            #region "Unique Key"

            modelBuilder.Entity<Location>()
                .HasIndex(p => new { p.Name })
                .IsUnique();

            modelBuilder.Entity<Subscriber>()
                .HasIndex(p => new { p.EmailAddress })
                .IsUnique();

            modelBuilder.Entity<Menu>()
                .HasIndex(p => new { p.Name })
                .IsUnique();

            #endregion

        }
    }
}
