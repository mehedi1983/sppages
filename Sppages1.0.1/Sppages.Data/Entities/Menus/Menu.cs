﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Menus
{
    public class Menu : AuditableEntity
    {
        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public string Description { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(50)]
        public string Order { get; set; }

    }
}