﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Menus
{
    public class FooterLink : AuditableEntity
    {
        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        [StringLength(500)]
        public string Url { get; set; }

        [StringLength(10)]
        public string Status { get; set; }

        public int? Order { get; set; }

    }
}