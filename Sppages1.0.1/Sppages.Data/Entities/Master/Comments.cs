﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Core;

namespace Sppages.Data.Entities.Master
{
   
    public partial class Comments : AuditableEntity
    {

        public Comments()
        {
        }

        [Key]
        [StringLength(128)]
        public string Id { get; set; }

        [Required]
        [StringLength(128)]
        public string AdPostId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string Message { get; set; }

        [ForeignKey("AdPostId")]
        public virtual AdPost AdPost { get; set; }

    }
}
