﻿using System.ComponentModel.DataAnnotations;
using Sppages.Data.Entities.Core;

namespace Sppages.Data.Entities.Master
{
    public class Subscriber : AuditableEntity
    {
        [Required]
        [StringLength(100)]
        public string EmailAddress { get; set; }

        [StringLength(20)]
        public string Status { get; set; }
    }
}