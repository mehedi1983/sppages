﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Company;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Identity;

namespace Sppages.Data.Entities.Master
{
    public class AppApplicationUser : AuditableEntity
    {
        [Required]
        [Column(Order = 1)]
        [StringLength(450)]
        public string ApplicationUserId { get; set; }

        [Required]
        [Column(Order = 2)]
        [StringLength(128)]
        public string EmployeeId { get; set; }

        [Column(Order = 3)]
        [StringLength(100)]
        public string Name { get; set; }

        [ForeignKey("EmployeeId")]
        public virtual EmployeeInformation EmployeeInformation { get; set; }

        [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}