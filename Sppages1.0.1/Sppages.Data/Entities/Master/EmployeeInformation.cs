﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Master;

namespace Sppages.Data.Entities.Company
{
    public partial class EmployeeInformation : AuditableEntity
    {
        public EmployeeInformation()
        {
            AppApplicationUsers = new HashSet<AppApplicationUser>();
        }

        [Required]
        [Column(Order = 1)]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [Column(Order = 2)]
        [StringLength(50)]
        public string EmailAddress { get; set; }

        [Column(Order = 3)]
        [StringLength(50)]
        public string MobileNo { get; set; }

        [Column(Order = 4)]
        [StringLength(500)]
        public string Address { get; set; }

        [Column(Order = 5)]
        [StringLength(100)]
        public string CityName { get; set; }

        [Column(Order = 6)]
        [StringLength(50)]
        public string ZipCode { get; set; }

        [Column(Order = 7)]
        [StringLength(128)]
        public string CountryName { get; set; }

        [Column(Order = 8)]
        [StringLength(500)]
        public string Picture { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public virtual ICollection<AppApplicationUser> AppApplicationUsers { get; set; }
        
    }
}