﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Core;
using Sppages.Data.Entities.Menus;

namespace Sppages.Data.Entities.Master
{
    public class Location : AuditableEntity
    {
        public Location()
        {
            AdPosts = new HashSet<AdPost>();
        }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(100)]
        public string MetaDescription { get; set; }

        [StringLength(100)]
        public string MetaKeywords { get; set; }

        [StringLength(100)]
        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        [StringLength(128)]
        public string ParentId { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(128)]
        public string CityId { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }

        public virtual ICollection<AdPost> AdPosts { get; set; }
    }
}