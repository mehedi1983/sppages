﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Core;

namespace Sppages.Data.Entities.Master
{
    public class Tag : AuditableEntity
    {
        public Tag()
        {
            AdPostTags = new List<AdPostTag>();
        }

        [StringLength(200)]
        public string Title { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        public virtual ICollection<AdPostTag> AdPostTags { get; set; }
    }
}