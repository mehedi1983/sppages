﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Sppages.Data.Entities.Core;

namespace Sppages.Data.Entities.Master
{
    public class City : AuditableEntity
    {
        public City()
        {
            Locations = new HashSet<Location>();
        }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        [JsonIgnore]
        public virtual ICollection<Location> Locations { get; set; }
    }
}