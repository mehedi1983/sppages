﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sppages.Data.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Menu_Location_LocationId",
                table: "Menu");

            migrationBuilder.DropIndex(
                name: "IX_Menu_LocationId",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "ExternalLink",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "LinkWith",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "Menu");

            migrationBuilder.DropColumn(
                name: "ShortDescription",
                table: "Menu");

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Menu",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Menu");

            migrationBuilder.AddColumn<string>(
                name: "ExternalLink",
                table: "Menu",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LinkWith",
                table: "Menu",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LocationId",
                table: "Menu",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ParentId",
                table: "Menu",
                maxLength: 128,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ShortDescription",
                table: "Menu",
                maxLength: 100,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Menu_LocationId",
                table: "Menu",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Menu_Location_LocationId",
                table: "Menu",
                column: "LocationId",
                principalTable: "Location",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
