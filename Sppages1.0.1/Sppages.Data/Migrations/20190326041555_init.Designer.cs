﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Sppages.Data.Entities;

namespace Sppages.Data.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20190326041555_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityRole");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdImage", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("AdPostId")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<bool?>("IsFeatured");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.Property<string>("ThumbnailUrl")
                        .HasMaxLength(500);

                    b.Property<string>("Title")
                        .HasMaxLength(200);

                    b.Property<string>("Url")
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.HasIndex("AdPostId");

                    b.ToTable("AdImage");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdPost", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<int?>("Age");

                    b.Property<string>("B2BCustomerId")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("LocationId")
                        .HasMaxLength(128);

                    b.Property<string>("LongDescription");

                    b.Property<string>("MetaDescription")
                        .HasMaxLength(500);

                    b.Property<string>("MetaKeywords")
                        .HasMaxLength(200);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("SeoTitle")
                        .HasMaxLength(200);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.HasIndex("B2BCustomerId");

                    b.HasIndex("LocationId");

                    b.ToTable("AdPost");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdPostTag", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("AdPostId")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("TagId")
                        .HasMaxLength(128);

                    b.HasKey("Id");

                    b.HasIndex("AdPostId");

                    b.HasIndex("TagId");

                    b.ToTable("AdPostTag");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Company.EmployeeInformation", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("Address")
                        .HasMaxLength(500);

                    b.Property<string>("CityName")
                        .HasMaxLength(100);

                    b.Property<string>("CountryName")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("MobileNo")
                        .HasMaxLength(50);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Picture")
                        .HasMaxLength(500);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(50);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("EmployeeInformation");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Customer.B2BCustomer", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("Address")
                        .HasMaxLength(500);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LocationId")
                        .HasMaxLength(128);

                    b.Property<string>("MobileNumber")
                        .HasMaxLength(50);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(50);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(50);

                    b.Property<string>("Website")
                        .HasMaxLength(100);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("LocationId");

                    b.ToTable("B2BCustomer");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Customer.B2CCustomer", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("Address")
                        .HasMaxLength(500);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("LocationId")
                        .HasMaxLength(128);

                    b.Property<string>("MobileNumber")
                        .HasMaxLength(50);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<string>("PhoneNumber")
                        .HasMaxLength(50);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(50);

                    b.Property<string>("Website")
                        .HasMaxLength(100);

                    b.Property<string>("ZipCode")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("LocationId");

                    b.ToTable("B2CCustomer");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Identity.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("B2BCustomerId")
                        .HasMaxLength(128);

                    b.Property<string>("B2CCustomerId")
                        .HasMaxLength(128);

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("DisplayName")
                        .HasMaxLength(128);

                    b.Property<string>("Email")
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("B2BCustomerId");

                    b.HasIndex("B2CCustomerId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.AppApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("ApplicationUserId")
                        .IsRequired()
                        .HasMaxLength(450);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("EmployeeId")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.HasKey("Id");

                    b.HasIndex("ApplicationUserId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("AppApplicationUser");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.City", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.ToTable("City");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Comments", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("AdPostId")
                        .IsRequired()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("EmailAddress");

                    b.Property<string>("Message");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasMaxLength(100);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.HasKey("Id");

                    b.HasIndex("AdPostId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Location", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CityId")
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("LongDescription");

                    b.Property<string>("MetaDescription")
                        .HasMaxLength(100);

                    b.Property<string>("MetaKeywords")
                        .HasMaxLength(100);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("ParentId")
                        .HasMaxLength(128);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("ShortDescription")
                        .HasMaxLength(100);

                    b.Property<string>("Status")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Location");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Subscriber", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.HasIndex("EmailAddress")
                        .IsUnique();

                    b.ToTable("Subscriber");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Tag", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.Property<string>("Title")
                        .HasMaxLength(200);

                    b.HasKey("Id");

                    b.ToTable("Tag");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Menus.FooterLink", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<int?>("Order");

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("Status")
                        .HasMaxLength(10);

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasMaxLength(500);

                    b.HasKey("Id");

                    b.ToTable("FooterLink");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Menus.Menu", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(128);

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("CreatedDate");

                    b.Property<string>("ExternalLink")
                        .HasMaxLength(300);

                    b.Property<string>("LinkWith")
                        .HasMaxLength(100);

                    b.Property<string>("LocationId")
                        .HasMaxLength(128);

                    b.Property<string>("ModifiedBy")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Order")
                        .HasMaxLength(50);

                    b.Property<string>("ParentId")
                        .HasMaxLength(128);

                    b.Property<string>("RecStatus")
                        .HasMaxLength(2);

                    b.Property<string>("ShortDescription")
                        .HasMaxLength(100);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.HasKey("Id");

                    b.HasIndex("LocationId");

                    b.HasIndex("Name")
                        .IsUnique();

                    b.ToTable("Menu");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Identity.ApplicationRole", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.IdentityRole");


                    b.ToTable("ApplicationRole");

                    b.HasDiscriminator().HasValue("ApplicationRole");

                    b.HasData(
                        new { Id = "1", Name = "SMAdmin", NormalizedName = "SMADMIN" },
                        new { Id = "2", Name = "Admin", NormalizedName = "ADMIN" },
                        new { Id = "3", Name = "B2BCustomer", NormalizedName = "B2BCustomer" },
                        new { Id = "4", Name = "B2CCustomer", NormalizedName = "B2CCustomer" }
                    );
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Sppages.Data.Entities.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Identity.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdImage", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Ad.AdPost", "AdPost")
                        .WithMany("AdImages")
                        .HasForeignKey("AdPostId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdPost", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Customer.B2BCustomer", "B2BCustomer")
                        .WithMany("AdPosts")
                        .HasForeignKey("B2BCustomerId");

                    b.HasOne("Sppages.Data.Entities.Master.Location", "Location")
                        .WithMany("AdPosts")
                        .HasForeignKey("LocationId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Ad.AdPostTag", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Ad.AdPost", "AdPost")
                        .WithMany("AdPostTags")
                        .HasForeignKey("AdPostId");

                    b.HasOne("Sppages.Data.Entities.Master.Tag", "Tag")
                        .WithMany("AdPostTags")
                        .HasForeignKey("TagId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Customer.B2BCustomer", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Master.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Customer.B2CCustomer", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Master.Location", "Location")
                        .WithMany()
                        .HasForeignKey("LocationId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Identity.ApplicationUser", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Customer.B2BCustomer", "B2BCustomer")
                        .WithMany("ApplicationUsers")
                        .HasForeignKey("B2BCustomerId");

                    b.HasOne("Sppages.Data.Entities.Customer.B2CCustomer", "B2CCustomer")
                        .WithMany("ApplicationUsers")
                        .HasForeignKey("B2CCustomerId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.AppApplicationUser", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Identity.ApplicationUser", "ApplicationUser")
                        .WithMany("AppApplicationUsers")
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Sppages.Data.Entities.Company.EmployeeInformation", "EmployeeInformation")
                        .WithMany("AppApplicationUsers")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Comments", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Ad.AdPost", "AdPost")
                        .WithMany("Commentses")
                        .HasForeignKey("AdPostId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Sppages.Data.Entities.Master.Location", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Master.City", "City")
                        .WithMany("Locations")
                        .HasForeignKey("CityId");
                });

            modelBuilder.Entity("Sppages.Data.Entities.Menus.Menu", b =>
                {
                    b.HasOne("Sppages.Data.Entities.Master.Location", "Location")
                        .WithMany("Menus")
                        .HasForeignKey("LocationId");
                });
#pragma warning restore 612, 618
        }
    }
}
