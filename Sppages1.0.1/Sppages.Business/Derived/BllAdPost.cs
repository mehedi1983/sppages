﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.IdentityModel.Protocols;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Model.Ad;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllAdPost: BllAdPostBase
    {
        public BllAdPost(IAdService iAdService, IModelFactory iModelFactory) : base(iAdService, iModelFactory)
        { }

        DataTableFilter _dataTableFilter = new DataTableFilter();

        #region "Ad Post"

        public async Task<List<LocationSummaryModel>> LoadLocation()
        {
            var val =
                    IAdService.LocationRepository.GetData(
                        gd => gd.Status == OperationStatus.ACTIVE).Select(s => new LocationSummaryModel
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).OrderBy(ob => ob.Name).ToList();
            val.Insert(0, new LocationSummaryModel { Id = "", Name = "-Select-" });
            return val;
        }
        
        public async Task<DTReturnContainer> GetAdpostList(IFormCollection formFields)
        {
            var Result = IAdService.AdPostRepository.GetAllIncluding(i => i.Location).ToList();
            var adPostList = new List<AdPostModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name + w.Name + w.Age).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "age")
                    Result = Result.OrderByDescending(ob => ob.Age).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "age")
                    Result = Result.OrderBy(ob => ob.Age).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            adPostList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = adPostList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(adPostList.Count, filterData.perpage);
            adPostList = _dataTableFilter.CurrentPageItems(adPostList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = adPostList;
            return container;
        }

        public async Task<string> GetAdPostId()
        {
            var AdPostId = "";
            var val = IAdService.AdPostRepository.Get();
            if (val.Count > 0)
            {
                AdPostId = "ADI-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            }
            else
            {
                AdPostId = "ADI-100000";
            }
            return AdPostId;
        }

        public async Task<AdPostModel> GetAdPost(string Id)
        {
            return IModelFactory.Create(IAdService.AdPostRepository.GetAllIncluding(gi => gi.Id == Id, i => i.Location).FirstOrDefault());
        }

        public async Task<AdPostModel> Insert(AdPostModel adPostModel)
        {
            var apModel = IModelFactory.Create(adPostModel);
            apModel.RecStatus = OperationStatus.NEW;
            apModel.CreatedDate = DateTime.Now;
            IAdService.AdPostRepository.Insert(apModel);
            IAdService.Commit();
            return adPostModel;
        }

        public async Task<AdPostModel> Update(AdPostModel adPostModel)
        {
            var adPost = IAdService.AdPostRepository.Get(adPostModel.Id);
            adPost.Name = adPostModel.Name;
            adPost.LocationId = adPostModel.LocationId;
            adPost.Age = adPostModel.Age;
            adPost.LongDescription = adPostModel.LongDescription;
            adPost.SeoTitle = adPostModel.SeoTitle;
            adPost.MetaKeywords = adPostModel.MetaKeywords;
            adPost.MetaDescription = adPostModel.MetaDescription;
            adPost.Status = adPostModel.Status;
            adPost.RecStatus = OperationStatus.MODIFY;
            adPost.ModifiedDate = DateTime.Now;
            IAdService.AdPostRepository.Edit(adPost);
            IAdService.Commit();
            return adPostModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IAdService.AdPostRepository.DeleteById(Id);
            if (IAdService.Commit() == 1)
                return true;
            return false;
        }

        #endregion

        #region "Ad Tag"

        public async Task<DTReturnContainer> GetAdPostTagList(IFormCollection formFields, string AdPostId)
        {
            var Result = IAdService.AdPostTagRepository.GetAllIncluding(ai => ai.AdPostId == AdPostId, ai => ai.Tag, ai => ai.AdPost);

            //var Result = IAdService.AdPostTagRepository.GetData(gd => gd.AdPostId == AdPostId).ToList();
            var adPostImageList = new List<AdPostTagModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Sorting Order"

            //if (filterData.sort == "desc")
            //{
            //    if (filterData.field == "url")
            //        Result = Result.OrderByDescending(ob => ob.).ToList();
            //    else if (filterData.field == "title")
            //        Result = Result.OrderByDescending(ob => ob.Title).ToList();
            //    else if (filterData.field == "createdDate")
            //        Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
            //    else if (filterData.field == "status")
            //        Result = Result.OrderByDescending(ob => ob.Status).ToList();
            //}
            //else if (filterData.sort == "asc")
            //{
            //    if (filterData.field == "url")
            //        Result = Result.OrderBy(ob => ob.Url).ToList();
            //    else if (filterData.field == "title")
            //        Result = Result.OrderBy(ob => ob.Title).ToList();
            //    else if (filterData.field == "createdDate")
            //        Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
            //    else if (filterData.field == "status")
            //        Result = Result.OrderBy(ob => ob.Status).ToList();
            //}

            #endregion

            adPostImageList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = adPostImageList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(adPostImageList.Count, filterData.perpage);
            adPostImageList = _dataTableFilter.CurrentPageItems(adPostImageList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = adPostImageList;
            return container;


            //return IAdService.AdPostTagRepository.GetData(gd => gd.AdPostId == AdPostId).Select(IModelFactory.Create).ToList();
        }

        public async Task<List<TagModel>> LoadTag()
        {
            var val =
                IAdService.TagRepository.GetData(
                    gd => gd.Status == OperationStatus.ACTIVE).Select(s => new TagModel
                {
                    Id = s.Id,
                    Title = s.Title
                }).OrderBy(ob => ob.Title).ToList();
            val.Insert(0, new TagModel { Id = "", Title = "-Select-" });
            return val;
        }

        public async Task<AdPostTagModel> SaveAdTag(AdPostTagModel adPostTagModel)
        {
            var adPostTag = IModelFactory.Create(adPostTagModel);
            adPostTag.RecStatus = OperationStatus.NEW;
            adPostTag.CreatedDate = DateTime.Now;
            IAdService.AdPostTagRepository.Insert(adPostTag);
            IAdService.Commit();
            return adPostTagModel;
        }

        public async Task<bool> DeleteAdTag(string Id)
        {
            IAdService.AdPostTagRepository.DeleteById(Id);
            if (IAdService.Commit() == 1)
                return true;
            return false;
        }
        
        #endregion

        #region "Image"

        public async Task<DTReturnContainer> GetAdPostImageList(IFormCollection formFields, string AdPostId)
        {
            var Result = IAdService.AdImageRepository.GetData(gd => gd.AdPostId == AdPostId).ToList();
            var adPostImageList = new List<AdImageModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "url")
                    Result = Result.OrderByDescending(ob => ob.Url).ToList();
                else if (filterData.field == "title")
                    Result = Result.OrderByDescending(ob => ob.Title).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "url")
                    Result = Result.OrderBy(ob => ob.Url).ToList();
                else if (filterData.field == "title")
                    Result = Result.OrderBy(ob => ob.Title).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            adPostImageList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = adPostImageList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(adPostImageList.Count, filterData.perpage);
            adPostImageList = _dataTableFilter.CurrentPageItems(adPostImageList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = adPostImageList;
            return container;
        }

        public async Task<AdImageModel> InsertAdPostImage(AdImageModel adImageModel)
        {
            var apModel = IModelFactory.Create(adImageModel);
            apModel.RecStatus = OperationStatus.NEW;
            apModel.CreatedDate = DateTime.Now;
            IAdService.AdImageRepository.Insert(apModel);
            IAdService.Commit();
            return adImageModel;
        }

        public async Task<bool> DeleteAdPostImage(string Id)
        {
            IAdService.AdImageRepository.DeleteById(Id);
            if (IAdService.Commit() == 1)
                return true;
            return false;
        }

        public async Task<AdImageModel> GetAdPostImage(string Id)
        {
            return IModelFactory.Create(IAdService.AdImageRepository.GetData(gd => gd.Id == Id).FirstOrDefault());
        }

        #endregion

    }
}
