﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Data.Entities.Identity;
using Sppages.Model.Factories;
using Sppages.Model.Utility;
using Invoice.Service.Interface;
using Sppages.Model.Customer;

namespace TastyQueen.Business.Derived
{
    public class BllAuth : BllAuthBase
    {
        private readonly IConfiguration _configuration;
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllAuth(IAuthService iAuthService, IModelFactory iModelFactory, IConfiguration Configuration) : base(
            iAuthService, iModelFactory)
        {
            _configuration = Configuration;
        }

        public async Task<string> B2BCustomerId()
        {
            var EmployeeId = "";
            var val = IAuthService.B2BCustomerRepository.Get();
            if (val.Count > 0)
                EmployeeId = "GIR-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                EmployeeId = "GIR-100000";
            return EmployeeId;
        }

        public async Task<B2BCustomerModel> SaveB2BCustomer(B2BCustomerModel b2BCustomerModel)
        {
            var fLModel = IModelFactory.Create(b2BCustomerModel);
            fLModel.Id = await B2BCustomerId();
            fLModel.RecStatus = OperationStatus.NEW;
            IAuthService.B2BCustomerRepository.Insert(fLModel);
            IAuthService.Commit();
            b2BCustomerModel.Id = fLModel.Id;
            return b2BCustomerModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IAuthService.B2BCustomerRepository.DeleteById(Id);
            if (IAuthService.Commit() == 1)
                return true;
            return false;
        }

        public async Task<DTReturnContainer> GetUserList(IFormCollection formFields,
            List<ApplicationUser> applicationUsers)
        {
            var Result = applicationUsers;
            var SearchedValue = new List<ApplicationUser>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => w.UserName.ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "userName")
                    Result = Result.OrderByDescending(ob => ob.UserName).ToList();
            }
            else if (filterData.sort == "userName")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.UserName).ToList();
            }

            #endregion

            SearchedValue = Result;
            filterData.total = SearchedValue.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(SearchedValue.Count, filterData.perpage);
            SearchedValue = _dataTableFilter.CurrentPageItems(SearchedValue, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = SearchedValue;
            return container;
        }
    }
}