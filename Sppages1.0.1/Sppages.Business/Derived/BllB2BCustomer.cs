﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.IdentityModel.Protocols;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Data.Entities.Ad;
using Sppages.Data.Entities.Master;
using Sppages.Model.Ad;
using Sppages.Model.Customer;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllB2BCustomer: BllB2BCustomerBase
    {
        public BllB2BCustomer(IB2BCustomerService iB2BCustomerService, IModelFactory iModelFactory) : base(iB2BCustomerService, iModelFactory)
        { }

        DataTableFilter _dataTableFilter = new DataTableFilter();

        #region "B2B Customer"

        public async Task<DTReturnContainer> GetB2BCustomerList(IFormCollection formFields)
        {
            var Result = IB2BCustomerService.B2BCustomerRepository.GetAllIncluding(i => i.Location).ToList();
            var adPostList = new List<B2BCustomerModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name + w.Email + w.MobileNumber + w.PhoneNumber +w.Address).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "email")
                    Result = Result.OrderByDescending(ob => ob.Email).ToList();
                else if (filterData.field == "mobileNumber")
                    Result = Result.OrderByDescending(ob => ob.MobileNumber).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "email")
                    Result = Result.OrderBy(ob => ob.Email).ToList();
                else if (filterData.field == "mobileNumber")
                    Result = Result.OrderBy(ob => ob.MobileNumber).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            adPostList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = adPostList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(adPostList.Count, filterData.perpage);
            adPostList = _dataTableFilter.CurrentPageItems(adPostList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = adPostList;
            return container;
        }        

        #endregion    

    }
}
