﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllFooterLink : BllFooterLinkBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllFooterLink(IMenuService iMenuService, IModelFactory iModelFactory) : base(iMenuService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetFooterLinkList(IFormCollection formFields)
        {
            var Result = IMenuService.FooterLinkRepository.Get().ToList();
            ;

            var footerLinkList = new List<FooterLinkModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            footerLinkList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = footerLinkList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(footerLinkList.Count, filterData.perpage);
            footerLinkList = _dataTableFilter.CurrentPageItems(footerLinkList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = footerLinkList;
            return container;
        }

        public async Task<List<FooterLinkModel>> GetFooterLinkList()
        {
            return IMenuService.FooterLinkRepository.Get().Select(IModelFactory.Create).ToList();
        }

        public async Task<FooterLinkModel> GetFooterLink(string Id)
        {
            return IModelFactory.Create(IMenuService.FooterLinkRepository.Get(Id));
        }

        public async Task<string> GetFooterLinkId()
        {
            var FooterLinkId = "";
            var val = IMenuService.FooterLinkRepository.Get();
            if (val.Count > 0)
                FooterLinkId =
                    "FTL-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                FooterLinkId = "FTL-100000";
            return FooterLinkId;
        }

        public async Task<IList<FooterLinkSummaryModel>> LoadParent(string Id = "")
        {
            var lstFooterLinkSummaryModels = new List<FooterLinkSummaryModel>();
            if (Id == "")
                lstFooterLinkSummaryModels = IMenuService.FooterLinkRepository.GetData(
                    gd => gd.Status == OperationStatus.ACTIVE).Select(s => new FooterLinkSummaryModel
                {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            else
                lstFooterLinkSummaryModels = IMenuService.FooterLinkRepository
                    .GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id != Id).Select(s =>
                        new FooterLinkSummaryModel
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).OrderBy(ob => ob.Name).ToList();
            lstFooterLinkSummaryModels.Insert(0, new FooterLinkSummaryModel {Id = "", Name = "-Select-"});
            return lstFooterLinkSummaryModels;
        }

        public async Task<IList<LocationSummaryModel>> LoadLocation()
        {
            var lstLocationSummaryModels = new List<LocationSummaryModel>();
            lstLocationSummaryModels = IMenuService.LocationRepository.GetData(
                gd => gd.Status == OperationStatus.ACTIVE).Select(s => new LocationSummaryModel
            {
                Id = s.Id,
                Name = s.Name
            }).OrderBy(ob => ob.Name).ToList();
            lstLocationSummaryModels.Insert(0, new LocationSummaryModel {Id = "", Name = "-Select-"});
            return lstLocationSummaryModels;
        }

        public async Task<FooterLinkModel> Insert(FooterLinkModel footerLinkModel)
        {
            var fLModel = IModelFactory.Create(footerLinkModel);
            fLModel.Id = await GetFooterLinkId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMenuService.FooterLinkRepository.Insert(fLModel);
            IMenuService.Commit();
            return footerLinkModel;
        }

        public async Task<FooterLinkModel> Update(FooterLinkModel footerLinkModel)
        {
            var footerLink = IMenuService.FooterLinkRepository.Get(footerLinkModel.Id);
            footerLink.Name = footerLinkModel.Name;
            footerLink.Url = footerLinkModel.Url;
            footerLink.Status = footerLinkModel.Status;
            footerLink.Order = footerLinkModel.Order;
            footerLink.RecStatus = OperationStatus.MODIFY;
            footerLink.ModifiedDate = DateTime.Now;
            IMenuService.FooterLinkRepository.Edit(footerLink);
            IMenuService.Commit();
            return footerLinkModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMenuService.FooterLinkRepository.DeleteById(Id);
            if (IMenuService.Commit() == 1)
                return true;
            return false;
        }
    }
}