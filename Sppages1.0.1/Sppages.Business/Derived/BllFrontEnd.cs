﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Data.Entities.Master;
using Sppages.Model.Ad;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllFrontEnd : BllFrontEndBase
    {

        public BllFrontEnd(IFrontEndService iFrontEndService, IModelFactory iModelFactory) : base(iFrontEndService, iModelFactory)
        {
        }

        public async Task<List<LocationSummaryModel>> GetAllLocation()
        {
            var LocationList = IFrontEndService.LocationRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).
                Select(s => new LocationSummaryModel()
                {
                    Id = s.Id,
                    NoOfAds=s.AdPosts.Count,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            return LocationList;
        }

        public async Task<List<MenuModel>> GetMenu()
        {
            return IFrontEndService.MenuRepository.Get().Select(IModelFactory.Create).ToList();
        }

        public async Task<MenuModel> GetMenuById(string Id)
        {
            return IModelFactory.Create(IFrontEndService.MenuRepository.GetData(gd => gd.Id == Id).FirstOrDefault());
        }

        public async Task<List<TagModel>> GetTag()
        {
            return IFrontEndService.TagRepository.Get().Select(IModelFactory.Create).ToList();
        }

        public async Task<List<FooterLinkModel>> GetFooterLink()
        {
            return IFrontEndService.FooterLinkRepository.Get().OrderBy(ob=>ob.Order).Select(IModelFactory.Create).ToList();
        }

        public async Task<List<AdPostSummaryModel>> GetRecentPost()
        {
            var RecentPostList =
                IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).
                    Select(s => new AdPostSummaryModel
                    {
                        Id = s.Id,
                        Name = (s.Name.Length <= 30) ? s.Name : s.Name.Substring(0, 27) + "...",
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        FeaturedImageUrl = s.AdImages!=null?s.AdImages.FirstOrDefault().Url:"",// (s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : "",
                        CreatedDate = s.CreatedDate
                    }).OrderByDescending(ob => ob.CreatedDate).Take(5).OrderBy(ob => ob.Name).ToList();
            return RecentPostList;
        }

        public async Task<List<AdPostSummaryModel>> GetPopularPost()
        {
            var PopularPostList =
                IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).
                    Select(s => new AdPostSummaryModel
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        FeaturedImageUrl = s.AdImages != null ? s.AdImages.FirstOrDefault().Url : "",//(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : "",
                        CreatedDate = s.CreatedDate
                    }).OrderByDescending(ob => ob.CreatedDate).Take(5).OrderBy(ob => ob.Name).ToList();
            return PopularPostList;
        }

        public async Task<List<AdPostSummaryModel>> GetRandomPost()
        {
            var rand = new System.Random();
            var RandomPostList =
                IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).Skip(rand.Next(5)).
                    Select(s => new AdPostSummaryModel
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        FeaturedImageUrl = s.AdImages != null ? s.AdImages.FirstOrDefault().Url : "",//(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : "",
                        CreatedDate = s.CreatedDate
                    }).OrderByDescending(ob => ob.CreatedDate).Take(5).OrderBy(ob => ob.Name).ToList();
            return RandomPostList;
        }

        public async Task<List<AdPostSummaryModel>> GetRelatedPost(string AdPostId)
        {
            var LocationId = IFrontEndService.AdPostRepository.GetData(gd => gd.Id == AdPostId).FirstOrDefault().LocationId;
            var RelatedPostList = IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.LocationId == LocationId).
                Select(s => new AdPostSummaryModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Day = s.CreatedDate.Value.ToString("dd"),
                    Month = s.CreatedDate.Value.ToString("MMM"),
                    Year = s.CreatedDate.Value.ToString("yyyy"),
                    FeaturedImageUrl = s.AdImages != null ? s.AdImages.FirstOrDefault().Url : "",//(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : "",
                    CreatedDate = s.CreatedDate
                }).OrderByDescending(ob => ob.CreatedDate).Take(5).OrderBy(ob => ob.Name).ToList();
            return RelatedPostList;
        }

        public async Task<List<CommentsModel>> GetLatestComments()
        {
            var val = IFrontEndService.CommentsRepository.GetData().OrderByDescending(ob => ob.CreatedDate).Take(5)
                .Select(s => new CommentsModel
                {
                    Id = s.Id,
                    AdPostId = s.AdPostId,
                    Message = (s.Message.Length > 100) ? s.Message.Substring(0, 90) : s.Message,
                }).ToList();
            return val;
        }

        public async Task<List<CommentsModel>> GetRecentComments(string AdPostId)
        {
            var val = IFrontEndService.CommentsRepository.GetData(gd => gd.AdPostId == AdPostId).OrderByDescending(ob => ob.CreatedDate).Take(5)
                .Select(s => new CommentsModel
                {
                    Id = s.Id,
                    Message = (s.Message.Length > 100) ? s.Message.Substring(0, 90) : s.Message,
                }).ToList();
            return val;
        }

        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();
        public async Task<List<AdPostSummaryModel>> SearchPost(string SearchBy, string Id)
        {
            if (OperationStatus.LOCATION == SearchBy)
            {
                var LocationList =
                    IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.LocationId == Id).
                    Select(s => new AdPostSummaryModel
                    {
                        Id = s.Id,
                        Name = (s.Name.Length <= 40) ? s.Name : s.Name.Substring(0, 37) + "...",
                        LocationId = s.LocationId,
                        LocationName = s.Location.Name,
                        PublishDate = s.CreatedDate,
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        LongDescription = s.LongDescription,
                        FeaturedImage = s.AdImages != null ? s.AdImages.FirstOrDefault().Url : "" //(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : ""
                    }).OrderByDescending(ob => ob.PublishDate).ToList();

                return LocationList;
            }
            else if (OperationStatus.TAG == SearchBy)
            {
                var LocationList =
                    IFrontEndService.AdPostTagRepository.GetData(gd => gd.TagId == Id)
                        .ToList().Join(IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).ToList(),
                            ta => ta.AdPostId, ap => ap.Id, (ta, ap) => new AdPostSummaryModel
                            {
                                Id = ap.Id,
                                Name = (ap.Name.Length <= 40) ? ap.Name : ap.Name.Substring(0, 37) + "...",
                                LocationId = ap.LocationId,
                                LocationName = ap.Location.Name,
                                PublishDate = ap.CreatedDate,
                                Day = ap.CreatedDate.Value.ToString("dd"),
                                Month = ap.CreatedDate.Value.ToString("MMM"),
                                Year = ap.CreatedDate.Value.ToString("yyyy"),
                                LongDescription = ap.LongDescription,
                                FeaturedImage = ap.AdImages != null ? ap.AdImages.FirstOrDefault().Url : "" //(ap.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null)? ap.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : ""
                            }).OrderByDescending(ob => ob.PublishDate).ToList();

                return LocationList;
            }
            return null;
        }

        public async Task<DTSearchReturnContainer> SearchAdByLocTag(int PageSize, int PageNo, string SearchString, string SearchBy, string Id)
        {

            if (OperationStatus.LOCATION == SearchBy)
            {
                var tempSearch =
                    IFrontEndService.AdPostRepository.GetAllIncluding(i => i.AdImages, ii => ii.Location).Where(w=>w.LocationId==Id && w.Status == OperationStatus.ACTIVE).
                        Select(s => new LocSummaryModel
                        {
                            Id = s.Id,
                            Name = (s.Name.Length <= 40) ? s.Name : s.Name.Substring(0, 37) + "...",
                            LocationId = s.LocationId,
                            LocationName = (s.Location != null) ? s.Location.Name : "",
                            PublishDate = s.CreatedDate,
                            Day = s.CreatedDate.Value.ToString("dd"),
                            Month = s.CreatedDate.Value.ToString("MMM"),
                            Year = s.CreatedDate.Value.ToString("yyyy"),
                            LongDescription = s.LongDescription,
                            FeaturedImage = (s.AdImages.Count>0) ? s.AdImages.FirstOrDefault().Url : ""
                        });

                if (SearchString != "" && SearchString != null)
                    tempSearch = tempSearch.Where(w => (w.Name + w.LocationName).ToUpper().Contains(SearchString.ToUpper()));
                var finalSearch = _dataTableFilter.CurrentPageItems(tempSearch.ToList(), PageNo, PageSize).ToList();

                DTSearchReturnContainer dtSearchReturnContainer = new DTSearchReturnContainer();
                dtSearchReturnContainer.TotalRowsCount = tempSearch.Count();
                dtSearchReturnContainer.Data = finalSearch.OrderByDescending(ob => ob.PublishDate).ToList();
                return dtSearchReturnContainer;
            }
            else if (OperationStatus.TAG == SearchBy)
            {
                var tempSearch = IFrontEndService.AdPostTagRepository.GetData(gd => gd.TagId == Id)
                        .ToList().Join(IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE).ToList(),
                            ta => ta.AdPostId, ap => ap.Id, (ta, ap) => new LocSummaryModel
                            {
                                Id = ap.Id,
                                Name = (ap.Name.Length <= 40) ? ap.Name : ap.Name.Substring(0, 37) + "...",
                                LocationId = ap.LocationId,
                                LocationName = ap.Location.Name,
                                PublishDate = ap.CreatedDate,
                                Day = ap.CreatedDate.Value.ToString("dd"),
                                Month = ap.CreatedDate.Value.ToString("MMM"),
                                Year = ap.CreatedDate.Value.ToString("yyyy"),
                                LongDescription = ap.LongDescription,
                                FeaturedImage = ap.AdImages.Count > 0 ? ap.AdImages.FirstOrDefault().Url : "" 
                            });

                if (SearchString != "" && SearchString != null)
                    tempSearch = tempSearch.Where(w => (w.Name + w.LocationName).ToUpper().Contains(SearchString.ToUpper()));
                var finalSearch = _dataTableFilter.CurrentPageItems(tempSearch.ToList(), PageNo, PageSize).ToList();

                DTSearchReturnContainer dtSearchReturnContainer = new DTSearchReturnContainer();
                dtSearchReturnContainer.TotalRowsCount = tempSearch.Count();
                dtSearchReturnContainer.Data = finalSearch.OrderByDescending(ob => ob.PublishDate).ToList();
                return dtSearchReturnContainer;
            }
            return null;         
        }

        public async Task<DTSearchReturnContainer> SearchAd(int PageSize, int PageNo, string SearchString)
        {
            var tempSearch =
                IFrontEndService.AdPostRepository.GetAllIncluding(i => i.AdImages, ii => ii.Location).
                    Select(s => new LocSummaryModel
                    {
                        Id = s.Id,
                        Name = (s.Name.Length <= 40) ? s.Name : s.Name.Substring(0, 37) + "...",
                        LocationId = s.LocationId,
                        LocationName = (s.Location != null) ? s.Location.Name : "",
                        PublishDate = s.CreatedDate,
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        LongDescription = s.LongDescription,
                        FeaturedImage = (s.AdImages.Count>0) ? s.AdImages.FirstOrDefault().Url : ""
                    });

            if (SearchString != "" && SearchString != null)
                tempSearch = tempSearch.Where(w => (w.Name + w.LocationName).ToUpper().Contains(SearchString.ToUpper()));
            var finalSearch = _dataTableFilter.CurrentPageItems(tempSearch.ToList(), PageNo, PageSize).ToList();

            DTSearchReturnContainer dtSearchReturnContainer = new DTSearchReturnContainer();
            dtSearchReturnContainer.TotalRowsCount = tempSearch.Count();
            dtSearchReturnContainer.Data = finalSearch.OrderByDescending(ob => ob.PublishDate).ToList();
            return dtSearchReturnContainer;
        }

        public async Task<List<LocSummaryModel>> GetPost()
        {
            var LocationList =
                IFrontEndService.AdPostRepository.GetAllIncluding(i => i.AdImages, ii => ii.Location).Where(w => w.Status == OperationStatus.ACTIVE).
                    Select(s => new LocSummaryModel
                    {
                        Id = s.Id,
                        Name = (s.Name.Length <= 40) ? s.Name : s.Name.Substring(0,37)+"...",
                        LocationId = s.LocationId,
                        LocationName = (s.Location != null) ? s.Location.Name : "",
                        PublishDate = s.CreatedDate,
                        Day = s.CreatedDate.Value.ToString("dd"),
                        Month = s.CreatedDate.Value.ToString("MMM"),
                        Year = s.CreatedDate.Value.ToString("yyyy"),
                        LongDescription = s.LongDescription,
                        FeaturedImage = (s.AdImages != null) ? s.AdImages.FirstOrDefault().Url : "" //(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : ""
                    }).OrderByDescending(ob => ob.PublishDate).ToList();

            return LocationList;
        }

        public async Task<AdPostSummaryModel> GetPostDetails(string PostId)
        {
            try
            {
                var LocationList =
                    IFrontEndService.AdPostRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id == PostId).
                        Select(s => new AdPostSummaryModel
                        {
                            Id = s.Id,
                            Name = s.Name,
                            LocationId = s.LocationId,
                            LocationName = s.Location!=null?s.Location.Name:"",
                            PublishDate = s.CreatedDate,
                            Day = s.CreatedDate.Value.ToString("dd"),
                            Month = s.CreatedDate.Value.ToString("MMM"),
                            Year = s.CreatedDate.Value.ToString("yyyy"),
                            LongDescription = s.LongDescription,
                            FeaturedImage = s.AdImages !=null? s.AdImages.FirstOrDefault().Url:"",//(s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault() != null) ? s.AdImages.Where(w => w.IsFeatured == true).FirstOrDefault().Url : "",
                            Images = s.AdImages.Select(IModelFactory.Create).ToList()
                        }).FirstOrDefault();

                return LocationList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<List<TagModel>> GetProfileTag(string Id)
        {
            var TagList = IFrontEndService.AdPostTagRepository.GetData(gd => gd.AdPostId == Id).Join(
                IFrontEndService.TagRepository.GetData(gd => gd.Status == OperationStatus.ACTIVE), at => at.TagId, t => t.Id, (at, t) =>
                    new TagModel
                    {
                        Id = t.Id,
                        Title = t.Title
                    }).OrderBy(ob => ob.Title).ToList();
            return TagList;
        }

        public async Task<List<CommentsModel>> GetAllComments(string AdPostId)
        {
            var val = IFrontEndService.CommentsRepository.GetData(gd => gd.AdPostId == AdPostId)
                .Select(s => new CommentsModel
                {
                    Id = s.Id,
                    Message = s.Message,
                    Name = s.Name,
                    EmailAddress = s.EmailAddress,
                    PostedDay = s.CreatedDate.Value.ToString("dd"),
                    PostedMonth = s.CreatedDate.Value.ToString("MMM"),
                    PostedYear = s.CreatedDate.Value.ToString("yyyy"),
                }).ToList();
            return val;
        }

        public async Task<CommentsModel> AddRecentComment(string Name, string Message, string AdPostId, string EmailAddress)
        {
            Comments comments = new Comments();
            comments.Id = Guid.NewGuid().ToString();
            comments.AdPostId = AdPostId;
            comments.Message = Message;
            comments.Name = Name;
            comments.EmailAddress = EmailAddress;
            comments.RecStatus = OperationStatus.NEW;
            comments.CreatedDate = DateTime.Now;
            comments.CreatedBy = "";//User.Identity.GetUserName();

            IFrontEndService.CommentsRepository.Insert(comments);
            IFrontEndService.Commit();

            CommentsModel commentsModel = new CommentsModel();

            commentsModel.Id = comments.Id;
            commentsModel.AdPostId = comments.AdPostId;
            commentsModel.Message = comments.Message;
            commentsModel.Name = comments.Name;
            commentsModel.EmailAddress = comments.EmailAddress;
            commentsModel.PostedDay = comments.CreatedDate.Value.ToString("dd");
            commentsModel.PostedMonth = comments.CreatedDate.Value.ToString("MMM");
            commentsModel.PostedYear = comments.CreatedDate.Value.ToString("yyyy");

            return commentsModel;
        }

    }
}