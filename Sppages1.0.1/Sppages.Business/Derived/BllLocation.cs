﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllLocation: BllLocationBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllLocation(IMasterService iMasterService, IModelFactory iModelFactory) : base(iMasterService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetLocationList(IFormCollection formFields)
        {
            var Result = IMasterService.LocationRepository.Get().ToList();
            var LocationList = new List<LocationModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name + w.ShortDescription + w.LongDescription).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "shortDescription")
                    Result = Result.OrderByDescending(ob => ob.ShortDescription).ToList();
                else if (filterData.field == "longDescription")
                    Result = Result.OrderByDescending(ob => ob.LongDescription).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "shortDescription")
                    Result = Result.OrderByDescending(ob => ob.ShortDescription).ToList();
                else if (filterData.field == "longDescription")
                    Result = Result.OrderByDescending(ob => ob.LongDescription).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            LocationList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = LocationList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(LocationList.Count, filterData.perpage);
            LocationList = _dataTableFilter.CurrentPageItems(LocationList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = LocationList;
            return container;
        }

        public async Task<LocationModel> GetLocation(string Id)
        {
            return IModelFactory.Create(IMasterService.LocationRepository.Get(Id));
        }

        public async Task<string> GetLocationId()
        {
            var LocationId = "";
            var val = IMasterService.LocationRepository.Get();
            if (val.Count > 0)
                LocationId = "LOC-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                LocationId = "LOC-100000";
            return LocationId;
        }

        public async Task<IList<LocationSummaryModel>> LoadParent(string Id = "")
        {
            var lstLocationSummaryModels = new List<LocationSummaryModel>();
            if (Id == "")
                lstLocationSummaryModels = IMasterService.LocationRepository.GetData(
                    gd => gd.Status == OperationStatus.ACTIVE).Select(s => new LocationSummaryModel
                    {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            else
                lstLocationSummaryModels = IMasterService.LocationRepository
                    .GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id != Id).Select(s =>
                        new LocationSummaryModel
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).OrderBy(ob => ob.Name).ToList();
            lstLocationSummaryModels.Insert(0, new LocationSummaryModel { Id = "", Name = "-Select-" });
            return lstLocationSummaryModels;
        }

        public async Task<LocationModel> Insert(LocationModel locationModel)
        {
            var fLModel = IModelFactory.Create(locationModel);
            fLModel.Id = await GetLocationId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMasterService.LocationRepository.Insert(fLModel);
            IMasterService.Commit();
            return locationModel;
        }

        public async Task<LocationModel> Update(LocationModel locModel)
        {
            var LocationModel = IMasterService.LocationRepository.Get(locModel.Id);
            LocationModel.Name = locModel.Name;
            LocationModel.ShortDescription = locModel.ShortDescription;
            LocationModel.LongDescription = locModel.LongDescription;
            LocationModel.MetaKeywords = locModel.MetaKeywords;
            LocationModel.MetaDescription = locModel.MetaDescription;
            LocationModel.ParentId = locModel.ParentId;
            LocationModel.Status = locModel.Status;
            LocationModel.RecStatus = OperationStatus.MODIFY;
            LocationModel.ModifiedDate = DateTime.Now;
            IMasterService.LocationRepository.Edit(LocationModel);
            IMasterService.Commit();
            return locModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMasterService.LocationRepository.DeleteById(Id);
            if (IMasterService.Commit() == 1)
                return true;
            return false;
        }

    }
}
