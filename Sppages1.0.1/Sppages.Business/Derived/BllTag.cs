﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllTag : BllTagBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllTag(IMasterService iMasterService, IModelFactory iModelFactory) : base(iMasterService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetTagList(IFormCollection formFields)
        {
            var Result = IMasterService.TagRepository.Get().ToList();
            var tagList = new List<TagModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Title).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "title")
                    Result = Result.OrderByDescending(ob => ob.Title).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "title")
                    Result = Result.OrderBy(ob => ob.Title).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            tagList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = tagList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(tagList.Count, filterData.perpage);
            tagList = _dataTableFilter.CurrentPageItems(tagList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = tagList;
            return container;
        }

        public async Task<TagModel> GetTag(string Id)
        {
            return IModelFactory.Create(IMasterService.TagRepository.Get(Id));
        }

        public async Task<string> GetTagId()
        {
            var TagId = "";
            var val = IMasterService.TagRepository.Get();
            if (val.Count > 0)
                TagId = "TAG-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                TagId = "TAG-100000";
            return TagId;
        }

        public async Task<TagModel> Insert(TagModel tagModel)
        {
            var fLModel = IModelFactory.Create(tagModel);
            fLModel.Id = await GetTagId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMasterService.TagRepository.Insert(fLModel);
            IMasterService.Commit();
            return tagModel;
        }

        public async Task<TagModel> Update(TagModel tagModel)
        {
            var tag = IMasterService.TagRepository.Get(tagModel.Id);
            tag.Title = tagModel.Title;
            tag.Status = tagModel.Status;
            tag.RecStatus = OperationStatus.MODIFY;
            tag.ModifiedDate = DateTime.Now;
            IMasterService.TagRepository.Edit(tag);
            IMasterService.Commit();
            return tagModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMasterService.TagRepository.DeleteById(Id);
            if (IMasterService.Commit() == 1)
                return true;
            return false;
        }

    }
}