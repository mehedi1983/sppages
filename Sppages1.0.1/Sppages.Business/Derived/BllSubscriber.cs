﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllSubscriber : BllSubscriberBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllSubscriber(IMasterService iMasterService, IModelFactory iModelFactory) : base(iMasterService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetSubscriberList(IFormCollection formFields)
        {
            var Result = IMasterService.SubscriberRepository.Get().ToList();
            var subscriberModelList = new List<SubscriberModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.EmailAddress).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "emailAddress")
                    Result = Result.OrderByDescending(ob => ob.EmailAddress).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "emailAddress")
                    Result = Result.OrderBy(ob => ob.EmailAddress).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            subscriberModelList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = subscriberModelList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(subscriberModelList.Count, filterData.perpage);
            subscriberModelList = _dataTableFilter.CurrentPageItems(subscriberModelList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = subscriberModelList;
            return container;
        }

        public async Task<SubscriberModel> GetSubscriber(string Id)
        {
            return IModelFactory.Create(IMasterService.SubscriberRepository.Get(Id));
        }

        public async Task<string> GetSubscriberId()
        {
            var TagId = "";
            var val = IMasterService.SubscriberRepository.Get();
            if (val.Count > 0)
                TagId = "SUB-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                TagId = "SUB-100000";
            return TagId;
        }

        public async Task<SubscriberModel> Insert(SubscriberModel subscriberModel)
        {
            var fLModel = IModelFactory.Create(subscriberModel);
            fLModel.Id = await GetSubscriberId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMasterService.SubscriberRepository.Insert(fLModel);
            IMasterService.Commit();
            return subscriberModel;
        }

        public async Task<SubscriberModel> Update(SubscriberModel subscriberModel)
        {
            var subscriber = IMasterService.SubscriberRepository.Get(subscriberModel.Id);
            subscriber.EmailAddress = subscriberModel.EmailAddress;
            subscriber.Status = subscriberModel.Status;
            subscriber.RecStatus = OperationStatus.MODIFY;
            subscriber.ModifiedDate = DateTime.Now;
            IMasterService.SubscriberRepository.Edit(subscriber);
            IMasterService.Commit();
            return subscriberModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMasterService.SubscriberRepository.DeleteById(Id);
            if (IMasterService.Commit() == 1)
                return true;
            return false;
        }

    }
}