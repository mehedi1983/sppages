﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllEmployee: BllEmployeeBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllEmployee(IMasterService iMasterService, IModelFactory iModelFactory) : base(iMasterService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetEmployeeList(IFormCollection formFields)
        {
            var Result = IMasterService.EmployeeInformationRepository.Get().ToList();
            var tagList = new List<EmployeeInformationModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name + w.EmailAddress + w.MobileNo).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "emailAddress")
                    Result = Result.OrderByDescending(ob => ob.EmailAddress).ToList();
                else if (filterData.field == "mobileNo")
                    Result = Result.OrderByDescending(ob => ob.MobileNo).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "emailAddress")
                    Result = Result.OrderBy(ob => ob.EmailAddress).ToList();
                else if (filterData.field == "mobileNo")
                    Result = Result.OrderBy(ob => ob.MobileNo).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            tagList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = tagList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(tagList.Count, filterData.perpage);
            tagList = _dataTableFilter.CurrentPageItems(tagList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = tagList;
            return container;
        }

        public async Task<EmployeeInformationModel> GetEmployee(string Id)
        {
            return IModelFactory.Create(IMasterService.EmployeeInformationRepository.Get(Id));
        }

        public async Task<string> GetEmployeeId()
        {
            var EmployeeId = "";
            var val = IMasterService.EmployeeInformationRepository.Get();
            if (val.Count > 0)
                EmployeeId = "EMP-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                EmployeeId = "EMP-100000";
            return EmployeeId;
        }

        public async Task<EmployeeInformationModel> Insert(EmployeeInformationModel employeeInformationModel)
        {
            var fLModel = IModelFactory.Create(employeeInformationModel);
            fLModel.Id = await GetEmployeeId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMasterService.EmployeeInformationRepository.Insert(fLModel);
            IMasterService.Commit();
            return employeeInformationModel;
        }

        public async Task<EmployeeInformationModel> Update(EmployeeInformationModel employeeInformationModel)
        {
            var employeeInformation = IMasterService.EmployeeInformationRepository.Get(employeeInformationModel.Id);
            employeeInformation.Name = employeeInformationModel.Name;
            employeeInformation.EmailAddress = employeeInformationModel.EmailAddress;
            employeeInformation.MobileNo = employeeInformationModel.MobileNo;
            employeeInformation.Address = employeeInformationModel.Address;
            employeeInformation.CityName = employeeInformationModel.CityName;
            employeeInformation.CountryName = employeeInformationModel.CountryName;
            employeeInformation.ZipCode = employeeInformationModel.ZipCode;

            if (employeeInformationModel.Picture != null && employeeInformationModel.Picture != "")
                employeeInformation.Picture = employeeInformationModel.Picture;
            employeeInformation.Status = employeeInformationModel.Status;
            employeeInformation.RecStatus = OperationStatus.MODIFY;
            employeeInformation.ModifiedDate = DateTime.Now;
            IMasterService.EmployeeInformationRepository.Edit(employeeInformation);
            IMasterService.Commit();
            return employeeInformationModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMasterService.EmployeeInformationRepository.DeleteById(Id);
            if (IMasterService.Commit() == 1)
                return true;
            return false;
        }
    }
}
