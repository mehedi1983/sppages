﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Http;
using Sppages.Business.Base;
using Sppages.Business.Helper;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;
using Sppages.Model.Utility;

namespace Sppages.Business.Derived
{
    public class BllMenu : BllMenuBase
    {
        private readonly DataTableFilter _dataTableFilter = new DataTableFilter();

        public BllMenu(IMenuService iMenuService, IModelFactory iModelFactory) : base(iMenuService, iModelFactory)
        {
        }

        public async Task<DTReturnContainer> GetMenuList(IFormCollection formFields)
        {
            var Result = IMenuService.MenuRepository.Get().ToList();
            var menuList = new List<MenuModel>();
            var filterData = new meta();
            filterData = _dataTableFilter.GetFormData(formFields);

            #region "Filter against search"

            if (filterData.SearchString != "")
                Result = Result.Where(w => (w.Id + w.Name + w.Description).ToUpper()
                    .Contains(filterData.SearchString.ToUpper())).ToList();

            #endregion

            #region "Filter against status"

            if (filterData.Status == OperationStatus.ACTIVE || filterData.Status == OperationStatus.INACTIVE)
                Result = Result.Where(w => w.Status == filterData.Status).ToList();

            #endregion

            #region "Sorting Order"

            if (filterData.sort == "desc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderByDescending(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderByDescending(ob => ob.Name).ToList();
                else if (filterData.field == "shortDescription")
                    Result = Result.OrderByDescending(ob => ob.Description).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderByDescending(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderByDescending(ob => ob.Status).ToList();
            }
            else if (filterData.sort == "asc")
            {
                if (filterData.field == "id")
                    Result = Result.OrderBy(ob => ob.Id).ToList();
                else if (filterData.field == "name")
                    Result = Result.OrderBy(ob => ob.Name).ToList();
                else if (filterData.field == "shortDescription")
                    Result = Result.OrderByDescending(ob => ob.Description).ToList();
                else if (filterData.field == "createdDate")
                    Result = Result.OrderBy(ob => ob.CreatedDate).ToList();
                else if (filterData.field == "status")
                    Result = Result.OrderBy(ob => ob.Status).ToList();
            }

            #endregion

            menuList = Result.Select(IModelFactory.Create).ToList();
            filterData.total = menuList.Count;
            filterData.pages = _dataTableFilter.GetTotalNoOfPages(menuList.Count, filterData.perpage);
            menuList = _dataTableFilter.CurrentPageItems(menuList, filterData.page, filterData.perpage);

            var container = new DTReturnContainer();
            container.meta = filterData;
            container.data = menuList;
            return container;
        }

        public async Task<MenuModel> GetMenu(string Id)
        {
            return IModelFactory.Create(IMenuService.MenuRepository.Get(Id));
        }

        public async Task<string> GetMenuId()
        {
            var MenuId = "";
            var val = IMenuService.MenuRepository.Get();
            if (val.Count > 0)
                MenuId =
                    "MEN-" + (TypeUtil.convertToInt(val.Select(s => s.Id.Substring(4, 6)).ToList().Max()) + 1);
            else
                MenuId = "MEN-100000";
            return MenuId;
        }

        public async Task<IList<MenuSummaryModel>> LoadParent(string Id = "")
        {
            var lstMenuSummaryModels = new List<MenuSummaryModel>();
            if (Id == "")
                lstMenuSummaryModels = IMenuService.MenuRepository.GetData(
                    gd => gd.Status == OperationStatus.ACTIVE).Select(s => new MenuSummaryModel
                    {
                    Id = s.Id,
                    Name = s.Name
                }).OrderBy(ob => ob.Name).ToList();
            else
                lstMenuSummaryModels = IMenuService.MenuRepository
                    .GetData(gd => gd.Status == OperationStatus.ACTIVE && gd.Id != Id).Select(s =>
                        new MenuSummaryModel
                        {
                            Id = s.Id,
                            Name = s.Name
                        }).OrderBy(ob => ob.Name).ToList();
            lstMenuSummaryModels.Insert(0, new MenuSummaryModel { Id = "", Name = "-Select-"});
            return lstMenuSummaryModels;
        }

        public async Task<IList<LocationSummaryModel>> LoadLocation()
        {
            var lstLocationSummaryModels = new List<LocationSummaryModel>();
            lstLocationSummaryModels = IMenuService.LocationRepository.GetData(
                gd => gd.Status == OperationStatus.ACTIVE).Select(s => new LocationSummaryModel
            {
                Id = s.Id,
                Name = s.Name
            }).OrderBy(ob => ob.Name).ToList();
            lstLocationSummaryModels.Insert(0, new LocationSummaryModel {Id = "", Name = "-Select-"});
            return lstLocationSummaryModels;
        }

        public async Task<MenuModel> Insert(MenuModel menuModel)
        {
            var fLModel = IModelFactory.Create(menuModel);
            fLModel.Id = await GetMenuId();
            fLModel.RecStatus = OperationStatus.NEW;
            fLModel.CreatedDate = DateTime.Now;
            IMenuService.MenuRepository.Insert(fLModel);
            IMenuService.Commit();
            return menuModel;
        }

        public async Task<MenuModel> Update(MenuModel menuModel)
        {
            var menu = IMenuService.MenuRepository.Get(menuModel.Id);
            menu.Name = menuModel.Name;
            menu.Description = menuModel.Description;
            menu.Order = menuModel.Order;
            menu.Status = menuModel.Status;
            menu.RecStatus = OperationStatus.MODIFY;
            menu.ModifiedDate = DateTime.Now;
            IMenuService.MenuRepository.Edit(menu);
            IMenuService.Commit();
            return menuModel;
        }

        public async Task<bool> Delete(string Id)
        {
            IMenuService.MenuRepository.DeleteById(Id);
            if (IMenuService.Commit() == 1)
                return true;
            return false;
        }
    }
}