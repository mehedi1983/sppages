﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sppages.Business.Utility
{
    public class MessageManager
    {

        #region "Reset Password"

        public static string RESET_PASSWORD_INVALID_USER
        {
            get
            {
                const string val = "Invalid user.";
                return val;
            }
        }

        public static string RESET_PASSWORD_REQUEST_MESSAGE
        {
            get
            {
                const string val = "Password reset request sent successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_FAIL_MESSAGE
        {
            get
            {
                const string val = "Password fail to reset.";
                return val;
            }
        }

        public static string RESET_PASSWORD_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Password reset successfully.";
                return val;
            }
        }

        public static string RESET_PASSWORD_EMAIL_SUBJECT
        {
            get
            {
                const string val = "Password reset request";
                return val;
            }
        }

        #endregion

        #region "Chance password"

        public static string CHANGE_PASSWORD_SUCCESS
        {
            get
            {
                const string val = "Password change successfully.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_FAIL
        {
            get
            {
                const string val = "Password fail to change.";
                return val;
            }
        }

        public static string CHANGE_PASSWORD_INCORRECT_CURRENT_PASSWORD
        {
            get
            {
                const string val = "Incorrect current password.";
                return val;
            }
        }

        #endregion

        #region "Footer Link"

        public static string FOOTER_LINK_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Footer link save successfully.";
                return val;
            }
        }

        public static string FOOTER_LINK_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Footer link fail to save.";
                return val;
            }
        }

        public static string FOOTER_LINK_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Footer link edited successfully.";
                return val;
            }
        }

        public static string FOOTER_LINK_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Footer link fail edit.";
                return val;
            }
        }

        public static string FOOTER_LINK_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Footer link deleted successfully.";
                return val;
            }
        }

        public static string FOOTER_LINK_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Footer link fail to delete.";
                return val;
            }
        }

        #endregion

        #region "Tag"

        public static string TAG_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Tag save successfully.";
                return val;
            }
        }

        public static string TAG_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Tag fail to save.";
                return val;
            }
        }

        public static string TAG_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Tag edited successfully.";
                return val;
            }
        }

        public static string TAG_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Tag fail edit.";
                return val;
            }
        }

        public static string TAG_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Tag deleted successfully.";
                return val;
            }
        }

        public static string TAG_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Tag fail to delete.";
                return val;
            }
        }

        #endregion

        #region "Location"

        public static string LOCATION_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Location save successfully.";
                return val;
            }
        }

        public static string LOCATION_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Location fail to save.";
                return val;
            }
        }

        public static string LOCATION_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Location edited successfully.";
                return val;
            }
        }

        public static string LOCATION_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Location fail edit.";
                return val;
            }
        }

        public static string LOCATION_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Location deleted successfully.";
                return val;
            }
        }

        public static string LOCATION_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Location fail to delete.";
                return val;
            }
        }

        #endregion

        #region "Menu"

        public static string MENU_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Location save successfully.";
                return val;
            }
        }

        public static string MENU_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Menu fail to save.";
                return val;
            }
        }

        public static string MENU_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Menu edited successfully.";
                return val;
            }
        }

        public static string MENU_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Menu fail edit.";
                return val;
            }
        }

        public static string MENU_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Menu deleted successfully.";
                return val;
            }
        }

        public static string MENU_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Menu fail to delete.";
                return val;
            }
        }

        #endregion

        #region "Subscriber"

        public static string SUBSCRIBER_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Subscriber save successfully.";
                return val;
            }
        }

        public static string SUBSCRIBER_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Subscriber fail to save.";
                return val;
            }
        }

        public static string SUBSCRIBER_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Subscriber edited successfully.";
                return val;
            }
        }

        public static string SUBSCRIBER_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Subscriber fail edit.";
                return val;
            }
        }

        public static string SUBSCRIBER_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Subscriber deleted successfully.";
                return val;
            }
        }

        public static string SUBSCRIBER_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Subscriber fail to delete.";
                return val;
            }
        }

        #endregion

        #region "Employee"

        public static string EMPLOYEE_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee save successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail to save.";
                return val;
            }
        }

        public static string EMPLOYEE_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee edited successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail edit.";
                return val;
            }
        }

        public static string EMPLOYEE_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee deleted successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee fail to delete.";
                return val;
            }
        }

        public static string EMPLOYEE_UPLOAD_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee picture uploaded successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee picture fail to uploaded.";
                return val;
            }
        }

        public static string EMPLOYEE_IMAGE_COPY_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Employee picture copy successfully.";
                return val;
            }
        }

        public static string EMPLOYEE_IMAGE_COPY_FAIL_MESSAGE
        {
            get
            {
                const string val = "Employee picture fail to copy.";
                return val;
            }
        }


        #endregion

        #region "AdPost"

        public static string ADPOST_SAVE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post save successfully.";
                return val;
            }
        }

        public static string ADPOST_SAVE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post fail to save.";
                return val;
            }
        }

        public static string ADPOST_EDIT_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post edited successfully.";
                return val;
            }
        }

        public static string ADPOST_EDIT_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post fail edit.";
                return val;
            }
        }

        public static string ADPOST_DELETE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post deleted successfully.";
                return val;
            }
        }

        public static string ADPOST_DELETE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post fail to delete.";
                return val;
            }
        }

        public static string ADPOST_UPLOAD_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post picture uploaded successfully.";
                return val;
            }
        }

        public static string ADPOST_UPLOAD_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post picture fail to uploaded.";
                return val;
            }
        }

        public static string ADPOST_DELETE_IMAGE_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post image deleted successfully.";
                return val;
            }
        }

        public static string ADPOST_DELETE_IMAGE_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post image fail to delete.";
                return val;
            }
        }

        public static string ADPOST_SAVE_TAG_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post tag save successfully.";
                return val;
            }
        }

        public static string ADPOST_SAVE_TAG_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post tag fail to save.";
                return val;
            }
        }

        public static string ADPOST_DELETE_TAG_SUCCESS_MESSAGE
        {
            get
            {
                const string val = "Ad post tag deleted successfully.";
                return val;
            }
        }

        public static string ADPOST_DELETE_TAG_FAIL_MESSAGE
        {
            get
            {
                const string val = "Ad post tag fail to delete.";
                return val;
            }
        }

        #endregion

    }
}
