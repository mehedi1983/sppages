﻿using System;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Sppages.Business.Utility
{
    public class EmailUtil
    {
        //private static IConfiguration Configuration { get; set; }
        public async Task<bool> SendEmail(string To, string Subject, string Body, IConfiguration Configuration, string BCC="", string Attachment="")
        {
            try
            {
                var From = Configuration.GetSection("RegistrationEmail")["EmailAddress"];
                var pwd = Configuration.GetSection("RegistrationEmail")["Password"];
                SmtpClient client = new SmtpClient(Configuration.GetSection("RegistrationEmail")["SmtpClient"]);
                client.Port = Convert.ToInt32(Configuration.GetSection("RegistrationEmail")["Port"]);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                NetworkCredential credentials = new NetworkCredential(From, pwd);
                client.EnableSsl = Convert.ToBoolean(Configuration.GetSection("RegistrationEmail")["EnableSsl"]);
                client.Credentials = credentials;

                // Create the message:
                MailMessage mail = new MailMessage(new MailAddress(From, Configuration.GetSection("ApplicationInformation")["Name"]), new MailAddress(To, "User"));
                mail.Subject = Subject;

                if (BCC != "" && BCC != null)
                {
                    MailAddress addressBCC = new MailAddress(BCC);
                    if (BCC != "" && BCC != null)
                        mail.Bcc.Add(addressBCC);
                }
                mail.Body = Body;

                if (Attachment != "")
                {
                    Attachment attachment = new Attachment(Attachment);
                    mail.Attachments.Add(attachment);
                }

                mail.IsBodyHtml = true;
                client.Send(mail);

                if (Attachment != "")
                    mail.Attachments.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public bool SendEmailWithAttachment(string SenderEmailAddress, string Subject, string Body, string BCC = "", string Attachment1 = "", string Attachment2 = "")
        //{
        //    try
        //    {
        //        var credentialUserName = ConfigurationManager.AppSettings["email.Sender"];
        //        var sentFrom = ConfigurationManager.AppSettings["email.Sender"];
        //        var pwd = ConfigurationManager.AppSettings["email.password"];
        //        System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["email.Host.Name"]);
        //        client.Port = Convert.ToInt32(ConfigurationManager.AppSettings["email.Host.port"]);
        //        client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //        client.UseDefaultCredentials = false;
        //        System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(credentialUserName, pwd);
        //        client.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["email.ssl"]);
        //        client.Credentials = credentials;

        //        // Create the message:
        //        var mail = new System.Net.Mail.MailMessage(sentFrom, SenderEmailAddress);
        //        mail.Subject = Subject;
        //        if (BCC != null)
        //        {
        //            MailAddress addressBCC = new MailAddress(BCC);
        //            if (BCC != "")
        //                mail.Bcc.Add(addressBCC);
        //        }
        //        mail.Body = Body;

        //        if (Attachment1 != "")
        //        {
        //            Attachment attachment1 = new Attachment(Attachment1);
        //            mail.Attachments.Add(attachment1);
        //        }

        //        if (Attachment2 != "")
        //        {
        //            Attachment attachment2 = new Attachment(Attachment2);
        //            mail.Attachments.Add(attachment2);
        //        }

        //        mail.IsBodyHtml = true;
        //        client.Send(mail);

        //        mail.Attachments.Dispose();

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}

        public bool SendEmail(string FromEmail, string FromDisplayName, string ToEmailAddress, string ToDisplayName, string Subject, string Body, IConfiguration Configuration, string BCC = "", string Attachment = "", string Attachment1 = "")
        {
            try
            {
                var credentialUserName = Configuration.GetSection("RegistrationEmail")["EmailAddress"];
                var pwd = Configuration.GetSection("RegistrationEmail")["Password"];
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(Configuration.GetSection("RegistrationEmail")["SmtpClient"]);
                client.Port = Convert.ToInt32(Configuration.GetSection("RegistrationEmail")["Port"]);
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(credentialUserName, pwd);
                client.EnableSsl = Convert.ToBoolean(Configuration.GetSection("RegistrationEmail")["EnableSsl"]);
                client.Credentials = credentials;

                // Create the message:
                MailMessage mail = new MailMessage(new MailAddress(FromEmail, FromDisplayName), new MailAddress(ToEmailAddress, ToDisplayName));
                mail.Subject = Subject;

                if (BCC != "" && BCC != null)
                {
                    MailAddress addressBCC = new MailAddress(BCC);
                    if (BCC != "" && BCC != null)
                        mail.Bcc.Add(addressBCC);
                }
                mail.Body = Body;
                if (Attachment != "")
                {
                    Attachment attachment = new Attachment(Attachment);
                    mail.Attachments.Add(attachment);
                }

                if (Attachment1 != "")
                {
                    Attachment attachment1 = new Attachment(Attachment1);
                    mail.Attachments.Add(attachment1);
                }

                mail.IsBodyHtml = true;
                client.Send(mail);

                if (Attachment != "")
                    mail.Attachments.Dispose();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public string SendEmail(string FromEmail, string FromDisplayName, string ToEmailAddress, string ToDisplayName, string Subject, string Body, string BCC="", string Attachment = "", string Attachment1 = "", string cc="")
        //{
        //    try
        //    {

        //        MailMessage msg = new MailMessage();
        //        msg.To.Add(ToEmailAddress);
        //        MailAddress address = new MailAddress("info@csinvoice.com");
        //        msg.From = address;
        //        msg.Subject = Subject;
        //        msg.Body = Body;

        //        SmtpClient client = new SmtpClient();
        //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        //        client.EnableSsl = false;
        //        client.Host = "mail.csinvoice.com";
        //        client.Port = 25;

        //        NetworkCredential credentials = new NetworkCredential("info@csinvoice.com", "c$123456");
        //        client.UseDefaultCredentials = true;
        //        client.Credentials = credentials;

        //        client.Send(msg);


        //        return "Success";
        //    }
        //    catch (Exception ex)
        //    {
        //        return ((System.Net.Sockets.SocketException)ex.InnerException.InnerException).Message;
        //        throw ex;
        //    }
        //}

    }
}