﻿using System;

namespace Sppages.Business.Utility
{
    public class OperationStatus
    {
        public static string SYSTEM_GENERATED
        {
            get
            {
                const string val = "System Generated";
                return val;
            }
        }
        public static string LOCATION
        {
            get
            {
                const string val = "Location";
                return val;
            }
        }

        public static string TAG
        {
            get
            {
                const string val = "Tag";
                return val;
            }
        }

        public static string REGISTRATION_EMAIL
        {
            get
            {
                const string val = "RegistrationEmail";
                return val;
            }
        }

        public static string SUPPORT_EMAIL
        {
            get
            {
                const string val = "SupportEmail";
                return val;
            }
        }

        public static string PASSWORD_RESET_EMAIL
        {
            get
            {
                const string val = "PasswordResetEmail";
                return val;
            }
        }

        public static string CANCEL
        {
            get
            {
                const string val = "CANCEL";
                return val;
            }
        }

        public static string PREPARE
        {
            get
            {
                const string val = "PREPARE";
                return val;
            }
        }

        public static string CONFIRM
        {
            get
            {
                const string val = "CONFIRM";
                return val;
            }
        }

        public static string UNPAID
        {
            get
            {
                const string val = "UNPAID";
                return val;
            }
        }

        public static string SUBSCRIBER
        {
            get
            {
                const string val = "Subscriber";
                return val;
            }
        }

        public static string UNSUBSCRIBE
        {
            get
            {
                const string val = "Unsubscribe";
                return val;
            }
        }

        public static string ALL
        {
            get
            {
                const string val = "ALL";
                return val;
            }
        }

        public static string YES
        {
            get
            {
                const string val = "Y";
                return val;
            }
        }

        public static string NO
        {
            get
            {
                const string val = "N";
                return val;
            }
        }

        public static string ACTIVE
        {
            get
            {
                const string val = "Active";
                return val;
            }
        }

        public static string INACTIVE
        {
            get
            {
                const string val = "Inactive";
                return val;
            }
        }

        public static string NOT_SET
        {
            get
            {
                const string val = "Not Set";
                return val;
            }
        }

        public static string READ
        {
            get
            {
                const string val = "Read";
                return val;
            }
        }

        public static string FAILURE
        {
            get
            {
                const string val = "Failure";
                return val;
            }
        }

        public static string DENY
        {
            get
            {
                const string val = "Deny";
                return val;
            }
        }

        public static string NEW
        {
            get
            {
                const string val = "N";
                return val;
            }
        }

        public static string MODIFY
        {
            get
            {
                const string val = "M";
                return val;
            }
        }

        public static string DELETE
        {
            get
            {
                const string val = "D";
                return val;
            }
        }

        public static string CUSTOMER_ID
        {
            get
            {
                string val = "CUS-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string EMPLOYEE_ID
        {
            get
            {
                string val = "EMP-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string TRANSACTION_ID
        {
            get
            {
                string val = "GRAN-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string VOUCHER_NO
        {
            get
            {
                string val = "VOU-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string INVOICE_NO
        {
            get
            {
                string val = "INV-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string IMG_ID
        {
            get
            {
                string val = "img-" + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + "-" +
                    DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + DateTime.Now.Millisecond.ToString();
                return val;
            }
        }

        public static string PAYMENT
        {
            get
            {
                const string val = "PAYMENT";
                return val;
            }
        }

        public static string PENDING
        {
            get
            {
                const string val = "Pending";
                return val;
            }
        }

        public static string SEND
        {
            get
            {
                const string val = "Send";
                return val;
            }
        }

        public static string PASTE_IN_CODE
        {
            get
            {
                const string val = "Paste In Code";
                return val;
            }
        }

        public static string IMPORT_JPEG
        {
            get
            {
                const string val = "Import Jpeg";
                return val;
            }
        }

        public static string IMPORT_HTML
        {
            get
            {
                const string val = "Import Html";
                return val;
            }
        }

        public static string DRAFT
        {
            get
            {
                const string val = "Draft";
                return val;
            }
        }

        public static string OUTSTANDING
        {
            get
            {
                const string val = "Outstanding";
                return val;
            }
        }

        public static string PAID
        {
            get
            {
                const string val = "Paid";
                return val;
            }
        }

        public static string PAYMENTSUMMARY
        {
            get
            {
                const string val = "Payment Summary";
                return val;
            }
        }

        public static string INVOICESUMMARY
        {
            get
            {
                const string val = "Invoice Summary";
                return val;
            }
        }
        
        public static string SENT
        {
            get
            {
                const string val = "Sent";
                return val;
            }
        }

        public static string NOT_SENT
        {
            get
            {
                const string val = "Not Sent";
                return val;
            }
        }

        public static string VIEWED
        {
            get
            {
                const string val = "Viewed";
                return val;
            }
        }

        public static string REFERENCE_INDEX_PAGE
        {
            get
            {
                const string val = "Index_Page";
                return val;
            }
        }

        public static string REFERENCE_DETAILS_PAGE
        {
            get
            {
                const string val = "Details_Page";
                return val;
            }
        }

        public static string COMMENTS_TYPE_ADMIN
        {
            get
            {
                const string val = "Admin";
                return val;
            }
        }
        
        public static string COMMENTS_TYPE_CUSTOMER
        {
            get
            {
                const string val = "Customer";
                return val;
            }
        }

        #region "Payment History"
        public static string PAYMENT_HISTORY_PAID
        {
            get
            {
                const string val = "Paid";
                return val;
            }
        }

        public static string PAYMENT_HISTORY_MARKED_SENT
        {
            get
            {
                const string val = "Marked Sent";
                return val;
            }
        }

        public static string PAYMENT_HISTORY_PAYMENT_RECEIPT
        {
            get
            {
                const string val = "Payment Receipt";
                return val;
            }
        }

        #endregion

        #region "Invoice History"

        public static string INVOICE_HISTORY_REMINDER
        {
            get
            {
                const string val = "Reminder";
                return val;
            }
        }

        public static string INVOICE_HISTORY_SENT
        {
            get
            {
                const string val = "Sent";
                return val;
            }
        }

        public static string INVOICE_HISTORY_DRAFT
        {
            get
            {
                const string val = "Draft";
                return val;
            }
        }

        public static string INVOICE_HISTORY_EDIT_DRAFT
        {
            get
            {
                const string val = "Edit Draft";
                return val;
            }
        }

        public static string INVOICE_HISTORY_OUTSTANDING
        {
            get
            {
                const string val = "Outstanding";
                return val;
            }
        }

        public static string INVOICE_HISTORY_EDIT_OUTSTANDING
        {
            get
            {
                const string val = "Edit Outstanding";
                return val;
            }
        }

        public static string INVOICE_HISTORY_PAID
        {
            get
            {
                const string val = "Paid";
                return val;
            }
        }

        public static string INVOICE_HISTORY_MARKED_SENT
        {
            get
            {
                const string val = "Marked Sent";
                return val;
            }
        }

        public static string INVOICE_HISTORY_COMMENTS_BY_CUSTOMER
        {
            get
            {
                const string val = "Comments By Customer";
                return val;
            }
        }

        public static string INVOICE_HISTORY_COMMENTS_BY_COMPANY
        {
            get
            {
                const string val = "Comments By Company";
                return val;
            }
        }

        #endregion

        #region "Customer"

        public static string CUSTOMER_HISTORY_INSERT
        {
            get
            {
                const string val = "Insert";
                return val;
            }
        }

        public static string CUSTOMER_HISTORY_EDIT
        {
            get
            {
                const string val = "Edit";
                return val;
            }
        }

        #endregion
    }
}