﻿using System;
using System.Collections.Generic;
using System.Text;
using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllB2BAdPostBase
    {
        private readonly IAdService _iAdService;
        private readonly IModelFactory _iModelFactory;

        public BllB2BAdPostBase(IAdService iAdService, IModelFactory iModelFactory)
        {
            this._iAdService = iAdService;
            this._iModelFactory = iModelFactory;
        }

        protected IAdService IAdService
        {
            get { return _iAdService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }
    }
}
