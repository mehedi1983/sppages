﻿using Sppages.Model.Factories;
using Invoice.Service.Interface;

namespace Sppages.Business.Base
{
    public class BllAuthBase
    {
        public BllAuthBase(IAuthService iAuthService, IModelFactory iModelFactory)
        {
            IAuthService = iAuthService;
            IModelFactory = iModelFactory;
        }

        protected IAuthService IAuthService { get; }

        protected IModelFactory IModelFactory { get; }
    }
}