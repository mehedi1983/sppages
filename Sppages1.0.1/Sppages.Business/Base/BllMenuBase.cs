﻿using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllMenuBase
    {
        private readonly IMenuService _iMenuService;
        private readonly IModelFactory _iModelFactory;

        public BllMenuBase(IMenuService iMenuService, IModelFactory iModelFactory)
        {
            this._iMenuService = iMenuService;
            this._iModelFactory = iModelFactory;
        }

        protected IMenuService IMenuService
        {
            get { return _iMenuService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }
    }
}