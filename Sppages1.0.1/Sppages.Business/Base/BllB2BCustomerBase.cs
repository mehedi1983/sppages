﻿using System;
using System.Collections.Generic;
using System.Text;
using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllB2BCustomerBase
    {
        private readonly IB2BCustomerService _iB2BCustomerService;
        private readonly IModelFactory _iModelFactory;

        public BllB2BCustomerBase(IB2BCustomerService iB2BCustomerService, IModelFactory iModelFactory)
        {
            this._iB2BCustomerService = iB2BCustomerService;
            this._iModelFactory = iModelFactory;
        }

        protected IB2BCustomerService IB2BCustomerService
        {
            get { return _iB2BCustomerService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }
    }
}
