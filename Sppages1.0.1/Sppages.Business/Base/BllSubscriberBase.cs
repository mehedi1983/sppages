﻿using System;
using System.Collections.Generic;
using System.Text;
using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllSubscriberBase
    {
        private readonly IMasterService _iMasterService;
        private readonly IModelFactory _iModelFactory;
        public BllSubscriberBase(IMasterService iMasterService, IModelFactory iModelFactory)
        {
            this._iMasterService = iMasterService;
            this._iModelFactory = iModelFactory;
        }

        protected IMasterService IMasterService
        {
            get { return _iMasterService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }
    }
}
