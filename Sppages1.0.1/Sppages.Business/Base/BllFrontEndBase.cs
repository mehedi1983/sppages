﻿using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllFrontEndBase
    {
        private readonly IFrontEndService _iFrontEndService;
        private readonly IModelFactory _iModelFactory;
        public BllFrontEndBase(IFrontEndService iFrontEndService, IModelFactory iModelFactory)
        {
            this._iFrontEndService = iFrontEndService;
            this._iModelFactory = iModelFactory;
        }

        protected IFrontEndService IFrontEndService
        {
            get { return _iFrontEndService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }

    }
}