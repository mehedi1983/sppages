﻿using Invoice.Service.Interface;
using Sppages.Model.Factories;

namespace Sppages.Business.Base
{
    public class BllTagBase
    {
        private readonly IMasterService _iMasterService;
        private readonly IModelFactory _iModelFactory;
        public BllTagBase(IMasterService iMasterService, IModelFactory iModelFactory)
        {
            this._iMasterService = iMasterService;
            this._iModelFactory = iModelFactory;
        }

        protected IMasterService IMasterService
        {
            get { return _iMasterService; }
        }

        protected IModelFactory IModelFactory
        {
            get { return _iModelFactory; }
        }

    }
}