﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sppages.UserInterface.Areas.B2B.Controllers
{
    [Area("B2B")]
    [Authorize(Roles = "B2BCustomer")]
    public class B2BDashboardController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}