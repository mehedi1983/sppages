﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "Admin")]
    public class AdminDashboardController : Controller
    {
        [HttpGet]        
        public IActionResult Index()
        {
            return View();
        }
    }
}