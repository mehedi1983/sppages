﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "Admin")]
    public class MsAdminDashboardController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }
    }
}