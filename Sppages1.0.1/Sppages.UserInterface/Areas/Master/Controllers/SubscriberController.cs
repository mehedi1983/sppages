﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class SubscriberController : Controller
    {
        private readonly BllSubscriber _bllSubscriber;

        public SubscriberController(IMasterService iMasterService, IModelFactory iModelFactory)
        {
            _bllSubscriber = new BllSubscriber(iMasterService, iModelFactory);
        }

        public async Task<IActionResult> Index()
        {
            //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value; // will give the user's userId
            //var userName = User.FindFirst(ClaimTypes.Name).Value; // will give the user's userName
            //var userEmail = User.FindFirst(ClaimTypes.Email).Value; // will give the user's Email
            //ViewBag.LoadParent=LoadParent();
            //ViewBag.LoadLocation=LoadLocation();
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> GetSubscriberList(IFormCollection formFields)
        {
            return Json(await _bllSubscriber.GetSubscriberList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetSubscriber(string Id)
        {
            var tag = await _bllSubscriber.GetSubscriber(Id);
            return Json(new
            {
                Success = true,
                Value = tag
            });
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(SubscriberModel subscriberModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (subscriberModel.Id != null)
                    {
                        subscriberModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllSubscriber.Update(subscriberModel);
                    }
                    else
                    {
                        subscriberModel.Id = await _bllSubscriber.GetSubscriberId();
                        subscriberModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllSubscriber.Insert(subscriberModel);
                    }
                    return Json(new { success = true, Message = MessageManager.SUBSCRIBER_SAVE_SUCCESS_MESSAGE });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, Message = MessageManager.SUBSCRIBER_SAVE_FAIL_MESSAGE });
                }
            }
            return Json(new {success = true, Message = MessageManager.SUBSCRIBER_SAVE_FAIL_MESSAGE});
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            try
            {
                var result = await _bllSubscriber.Delete(Id);
                if (result)
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.SUBSCRIBER_DELETE_SUCCESS_MESSAGE
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.SUBSCRIBER_DELETE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.SUBSCRIBER_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}