﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class MenuController : Controller
    {
        private readonly BllMenu _bllMenu;

        public MenuController(IMenuService iMenuService, IModelFactory iModelFactory)
        {
            _bllMenu = new BllMenu(iMenuService, iModelFactory);
        }

        public async Task<IActionResult> Index()
        {
            //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value; // will give the user's userId
            //var userName = User.FindFirst(ClaimTypes.Name).Value; // will give the user's userName
            //var userEmail = User.FindFirst(ClaimTypes.Email).Value; // will give the user's Email
            //ViewBag.LoadParent=LoadParent();
            //ViewBag.LoadLocation=LoadLocation();
            return View();
        }

        [HttpGet]
        public async Task<IList<MenuSummaryModel>> LoadParent()
        {
            return await _bllMenu.LoadParent();
        }

        [HttpGet]
        public async Task<IList<LocationSummaryModel>> LoadLocation()
        {
            return await _bllMenu.LoadLocation();
        }

        [HttpPost]
        public async Task<JsonResult> GetMenuList(IFormCollection formFields)
        {
            return Json(await _bllMenu.GetMenuList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetMenu(string Id)
        {
            var footerLink = await _bllMenu.GetMenu(Id);
            return Json(new
            {
                Success = true,
                Value = footerLink
            });
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(MenuModel menuModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (menuModel.Id != null)
                    {
                        menuModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllMenu.Update(menuModel);
                    }
                    else
                    {
                        menuModel.Id = await _bllMenu.GetMenuId();
                        menuModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllMenu.Insert(menuModel);
                    }
                    return Json(new { success = true, Message = MessageManager.MENU_SAVE_SUCCESS_MESSAGE });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, Message = MessageManager.LOCATION_SAVE_FAIL_MESSAGE });
                }
            }
            return Json(new { success = true, Message = MessageManager.LOCATION_SAVE_FAIL_MESSAGE });
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _bllMenu.Delete(Id);
                    if (result)
                        return Json(new
                        {
                            Success = true,
                            Message = MessageManager.MENU_DELETE_SUCCESS_MESSAGE
                        });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = MessageManager.MENU_DELETE_FAIL_MESSAGE
                    });
                }
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.MENU_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}