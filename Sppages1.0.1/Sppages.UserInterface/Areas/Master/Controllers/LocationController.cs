﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class LocationController : Controller
    {
        private readonly BllLocation _bllLocation;

        public LocationController(IMasterService iMasterService, IModelFactory iModelFactory)
        {
            _bllLocation = new BllLocation(iMasterService, iModelFactory);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IList<LocationSummaryModel>> LoadParent()
        {
            return await _bllLocation.LoadParent();
        }

        [HttpPost]
        public async Task<JsonResult> GetLocationList(IFormCollection formFields)
        {
            return Json(await _bllLocation.GetLocationList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetLocation(string Id)
        {
            var location = await _bllLocation.GetLocation(Id);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(LocationModel locationModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (locationModel.Id != null)
                    {
                        locationModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllLocation.Update(locationModel);
                    }
                    else
                    {
                        locationModel.Id = await _bllLocation.GetLocationId();
                        locationModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllLocation.Insert(locationModel);
                    }
                    return Json(new { success = true, Message = MessageManager.LOCATION_SAVE_SUCCESS_MESSAGE });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, Message = MessageManager.LOCATION_SAVE_FAIL_MESSAGE });
                }
            }
            return Json(new { success = false, Message = MessageManager.LOCATION_SAVE_FAIL_MESSAGE });
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _bllLocation.Delete(Id);
                    if (result)
                        return Json(new
                        {
                            Success = true,
                            Message = MessageManager.LOCATION_DELETE_SUCCESS_MESSAGE
                        });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = MessageManager.FOOTER_LINK_DELETE_FAIL_MESSAGE
                    });
                }
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.FOOTER_LINK_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}