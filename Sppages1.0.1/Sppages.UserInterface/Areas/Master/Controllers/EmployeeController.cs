﻿using System;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class EmployeeController : Controller
    {
        private readonly BllEmployee _bllEmployee;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public EmployeeController(IMasterService iMasterService, IModelFactory iModelFactory,
            IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _bllEmployee = new BllEmployee(iMasterService, iModelFactory);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
        }

        public async Task<IActionResult> Index()
        {
            //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value; // will give the user's userId
            //var userName = User.FindFirst(ClaimTypes.Name).Value; // will give the user's userName
            //var userEmail = User.FindFirst(ClaimTypes.Email).Value; // will give the user's Email
            //ViewBag.LoadParent=LoadParent();
            //ViewBag.LoadLocation=LoadLocation();
            return View();
        }

        public async Task<IActionResult> Save(string Id = "")
        {
            ViewBag.EmployeeId = Id;
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetEmployee(string Id)
        {
            var employee = await _bllEmployee.GetEmployee(Id);
            return Json(new
            {
                Success = true,
                Value = employee
            });
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(IFormFile uploadImage)
        {
            string fileName = "";
            try
            {
                fileName = Guid.NewGuid() + ".jpg";
                if (uploadImage != null)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["TempFolder"], fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await uploadImage.CopyToAsync(stream);
                    }
                    return Json(new
                    {
                        success = true,
                        fileName,
                        uploadedUrl = filePath,
                        Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_SUCCESS_MESSAGE
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    fileName,
                    uploadedUrl = "",
                    Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                success = false,
                fileName,
                uploadedUrl = "",
                Message = MessageManager.EMPLOYEE_UPLOAD_IMAGE_FAIL_MESSAGE
            });
        }

        private bool CopyImage(string FileName, string SourceDir, string DestinationDir)
        {
            try
            {
                SourceDir = Path.Combine(_hostingEnvironment.WebRootPath, SourceDir, FileName);
                DestinationDir = Path.Combine(_hostingEnvironment.WebRootPath, DestinationDir, FileName);
                System.IO.File.Copy(SourceDir, DestinationDir, true);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        [HttpPost]
        public async Task<JsonResult> GetEmployeeList(IFormCollection formFields)
        {
            return Json(await _bllEmployee.GetEmployeeList(formFields));
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(EmployeeInformationModel employeeInformationModel)
        {
            if (ModelState.IsValid)
                try
                {
                    if (employeeInformationModel.Picture != null && employeeInformationModel.Picture != "")
                        CopyImage(employeeInformationModel.Picture,
                            _configuration.GetSection("ImageUpload")["TempFolder"],
                            _configuration.GetSection("ImageUpload")["DestinationFolder"]);

                    if (employeeInformationModel.Id != null)
                    {
                        employeeInformationModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllEmployee.Update(employeeInformationModel);
                        return Json(new
                        {
                            success = true,
                            RedirectUrl = Url.Action("Index", "Employee", new { Area = "Master" }),
                            Message = MessageManager.EMPLOYEE_SAVE_SUCCESS_MESSAGE
                        });
                    }
                    employeeInformationModel.Id = await _bllEmployee.GetEmployeeId();
                    employeeInformationModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                    await _bllEmployee.Insert(employeeInformationModel);
                    return Json(new
                    {
                        success = true,
                        RedirectUrl = Url.Action("Index", "Employee", new { Area = "Master" }),
                        Message = MessageManager.EMPLOYEE_EDIT_SUCCESS_MESSAGE
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        RedirectUrl = Url.Action("Index", "Employee", new { Area = "Master" }),
                        Message = MessageManager.EMPLOYEE_SAVE_FAIL_MESSAGE
                    });
                }
            return Json(new
            {
                success = false,
                RedirectUrl = Url.Action("Index", "Employee", new { Area = "Master" }),
                Message = MessageManager.EMPLOYEE_SAVE_FAIL_MESSAGE
            });
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _bllEmployee.Delete(Id);
                    if (result)
                        return Json(new
                        {
                            Success = true,
                            Message = MessageManager.EMPLOYEE_DELETE_SUCCESS_MESSAGE
                        });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = MessageManager.EMPLOYEE_DELETE_FAIL_MESSAGE
                    });
                }
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.TAG_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}