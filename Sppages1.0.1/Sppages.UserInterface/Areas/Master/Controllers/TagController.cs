﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Areas.Master.Controllers
{
    [Area("Master")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class TagController : Controller
    {
        private readonly BllTag _bllTag;

        public TagController(IMasterService iMasterService, IModelFactory iModelFactory)
        {
            _bllTag = new BllTag(iMasterService, iModelFactory);
        }

        public async Task<IActionResult> Index()
        {
            //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value; // will give the user's userId
            //var userName = User.FindFirst(ClaimTypes.Name).Value; // will give the user's userName
            //var userEmail = User.FindFirst(ClaimTypes.Email).Value; // will give the user's Email
            //ViewBag.LoadParent=LoadParent();
            //ViewBag.LoadLocation=LoadLocation();
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> GetTagList(IFormCollection formFields)
        {
            return Json(await _bllTag.GetTagList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetTag(string Id)
        {
            var tag = await _bllTag.GetTag(Id);
            return Json(new
            {
                Success = true,
                Value = tag
            });
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(TagModel tagModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (tagModel.Id != null)
                    {
                        tagModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllTag.Update(tagModel);
                    }
                    else
                    {
                        tagModel.Id = await _bllTag.GetTagId();
                        tagModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllTag.Insert(tagModel);
                    }
                    return Json(new { success = true, Message = MessageManager.TAG_SAVE_SUCCESS_MESSAGE });
                }
                catch (Exception ex)
                {
                    return Json(new { success = false, Message = MessageManager.TAG_SAVE_FAIL_MESSAGE });
                }
            }
            return Json(new {success = false, Message = MessageManager.TAG_SAVE_FAIL_MESSAGE});
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            try
            {
                var result = await _bllTag.Delete(Id);
                if (result)
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.TAG_DELETE_SUCCESS_MESSAGE
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.TAG_DELETE_FAIL_MESSAGE
                });
            }

            return Json(new
            {
                Success = false,
                Message = MessageManager.TAG_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }
        
    }
}