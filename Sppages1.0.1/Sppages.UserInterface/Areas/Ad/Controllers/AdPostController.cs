﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Ad;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Areas.Ad.Controllers
{
    [Area("Ad")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class AdPostController : Controller
    {
        private readonly BllAdPost _bllAdPost;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConfiguration _configuration;

        public AdPostController(IAdService iAdService, IModelFactory iModelFactory,
            IHostingEnvironment hostingEnvironment, IConfiguration Configuration)
        {
            _bllAdPost = new BllAdPost(iAdService, iModelFactory);
            _hostingEnvironment = hostingEnvironment;
            _configuration = Configuration;
        }

        public IActionResult Index()
        {
            return View();
        }

        #region "Ad Post"

        [HttpGet]
        public async Task<IList<LocationSummaryModel>> LoadLocation()
        {
            return await _bllAdPost.LoadLocation();
        }

        [HttpPost]
        public async Task<JsonResult> GetAdpostList(IFormCollection formFields)
        {
            return Json(await _bllAdPost.GetAdpostList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetAdPost(string Id)
        {
            var employee = await _bllAdPost.GetAdPost(Id);
            return Json(new
            {
                Success = true,
                Value = employee
            });
        }

        public async Task<IActionResult> Save(string Id = "")
        {
            ViewBag.AdPostId = Id;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(AdPostModel adPostModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    if (adPostModel.Id != null)
                    {
                        adPostModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllAdPost.Update(adPostModel);
                        return Json(new
                        {
                            success = true,
                            Operation = "Update",
                            RedirectUrl = Url.Action("Index", "AdPost", new { Area = "Ad" }),
                            Message = MessageManager.ADPOST_EDIT_SUCCESS_MESSAGE
                        });
                    }
                    adPostModel.Id = await _bllAdPost.GetAdPostId();
                    adPostModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                    await _bllAdPost.Insert(adPostModel);
                    ViewBag.AdPostId = adPostModel.Id;
                    return Json(new
                    {
                        success = true,
                        Operation = "Insert",
                        adPostModel.Id,
                        RedirectUrl = Url.Action("Index", "AdPost", new { Area = "Ad" }),
                        Message = MessageManager.ADPOST_SAVE_SUCCESS_MESSAGE
                    });
                }
                catch (Exception ex)
                {
                    return Json(new
                    {
                        success = false,
                        RedirectUrl = Url.Action("Index", "AdPost", new { Area = "Ad" }),
                        Message = MessageManager.ADPOST_SAVE_FAIL_MESSAGE
                    });
                }
            }
            return Json(new
            {
                success = false,
                RedirectUrl = Url.Action("Index", "AdPost", new { Area = "Ad" }),
                Message = MessageManager.ADPOST_SAVE_FAIL_MESSAGE
            });
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            try
            {
                var result = await _bllAdPost.Delete(Id);
                if (result)
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.ADPOST_DELETE_SUCCESS_MESSAGE
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.ADPOST_DELETE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.ADPOST_DELETE_FAIL_MESSAGE
            });
        }
        
        #endregion

        #region "Image"

        [HttpPost]
        public async Task<JsonResult> GetAdPostImageList(IFormCollection formFields, string AdPostId)
        {
            return Json(await _bllAdPost.GetAdPostImageList(formFields, AdPostId));
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(IFormFile uploadImage, string Title, string AdPostId)
        {
            string fileName = "";
            try
            {
                var gId = Guid.NewGuid().ToString();
                fileName = gId + ".jpg";
                if (uploadImage != null)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["AdPostUpload"], fileName);
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await uploadImage.CopyToAsync(stream);
                    }

                    var adImageModel = new AdImageModel();
                    adImageModel.Id = gId;
                    adImageModel.AdPostId = AdPostId;
                    adImageModel.Title = Title;
                    adImageModel.Url = fileName;
                    adImageModel.IsFeatured = false;
                    adImageModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;

                    var val = _bllAdPost.InsertAdPostImage(adImageModel);

                    return Json(new
                    {
                        success = true,
                        fileName,
                        uploadedUrl = filePath,
                        Message = MessageManager.ADPOST_UPLOAD_IMAGE_SUCCESS_MESSAGE
                    });
                }
            }
            catch (Exception e)
            {
                return Json(new
                {
                    success = false,
                    fileName,
                    uploadedUrl = "",
                    Message = MessageManager.ADPOST_UPLOAD_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                success = false,
                fileName,
                uploadedUrl = "",
                Message = MessageManager.ADPOST_UPLOAD_IMAGE_FAIL_MESSAGE
            });
        }

        //[HttpDelete]
        [HttpPost]
        public async Task<JsonResult> RemoveAdPostImage(string Id)
        {
            try
            {
                var tempImage = await _bllAdPost.GetAdPostImage(Id);
                var result = await _bllAdPost.DeleteAdPostImage(Id);

                if (result)
                {
                    var filePath = Path.Combine(_hostingEnvironment.WebRootPath,
                        _configuration.GetSection("ImageUpload")["AdPostUpload"], tempImage.Url);
                    System.IO.File.Delete(filePath);
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.ADPOST_DELETE_IMAGE_SUCCESS_MESSAGE
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.ADPOST_DELETE_IMAGE_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.ADPOST_DELETE_IMAGE_FAIL_MESSAGE
            });
        }

        #endregion

        #region "Ad Tag"

        [HttpGet]
        public async Task<IList<TagModel>> LoadTag()
        {
            return await _bllAdPost.LoadTag();
        }

        [HttpPost]
        public async Task<JsonResult> GetAdPostTagList(IFormCollection formFields, string AdPostId)
        {
            return Json(await _bllAdPost.GetAdPostTagList(formFields, AdPostId));
        }

        [HttpPost]
        public async Task<JsonResult> SaveAdTag(string TagId, string AdPostId)
        {
            try
            {
                var gId = Guid.NewGuid().ToString();
                var adPostTagModel = new AdPostTagModel();
                adPostTagModel.Id = gId;
                adPostTagModel.AdPostId = AdPostId;
                adPostTagModel.TagId = TagId;
                adPostTagModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                var val = _bllAdPost.SaveAdTag(adPostTagModel);
                return Json(new { success = true, Message = MessageManager.ADPOST_SAVE_TAG_SUCCESS_MESSAGE });
            }
            catch (Exception e)
            {
                return Json(new { success = false, Message = MessageManager.ADPOST_SAVE_TAG_FAIL_MESSAGE });
            }
        }

        //[HttpDelete]
        [HttpPost]
        public async Task<JsonResult> RemoveAdTag(string Id)
        {
            try
            {
                var result = await _bllAdPost.DeleteAdTag(Id);
                if (result)
                    return Json(new
                    {
                        Success = true,
                        Message = MessageManager.ADPOST_DELETE_TAG_SUCCESS_MESSAGE
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = MessageManager.ADPOST_DELETE_TAG_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                Message = MessageManager.ADPOST_DELETE_TAG_FAIL_MESSAGE
            });
        }

        #endregion
        
    }
}