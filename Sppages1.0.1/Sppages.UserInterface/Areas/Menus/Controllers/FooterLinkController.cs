﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Business.Utility;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.Model.Menus;

namespace Sppages.UserInterface.Areas.Menus.Controllers
{
    [Area("Menus")]
    [Authorize(Roles = "SMAdmin, Admin")]
    public class FooterLinkController : Controller
    {
        private readonly BllFooterLink _bllFooterLink;

        public FooterLinkController(IMenuService iMenuService, IModelFactory iModelFactory)
        {
            _bllFooterLink = new BllFooterLink(iMenuService, iModelFactory);
        }

        public async Task<IActionResult> Index()
        {
            //var userId = User.FindFirst(ClaimTypes.NameIdentifier).Value; // will give the user's userId
            //var userName = User.FindFirst(ClaimTypes.Name).Value; // will give the user's userName
            //var userEmail = User.FindFirst(ClaimTypes.Email).Value; // will give the user's Email
            //ViewBag.LoadParent=LoadParent();
            //ViewBag.LoadLocation=LoadLocation();
            return View();
        }

        [HttpGet]
        public async Task<IList<FooterLinkSummaryModel>> LoadParent()
        {
            return await _bllFooterLink.LoadParent();
        }

        [HttpGet]
        public async Task<IList<LocationSummaryModel>> LoadLocation()
        {
            return await _bllFooterLink.LoadLocation();
        }

        [HttpPost]
        public async Task<JsonResult> GetFooterLinkList(IFormCollection formFields)
        {
            return Json(await _bllFooterLink.GetFooterLinkList(formFields));
        }

        [HttpGet]
        public async Task<JsonResult> GetFooterLink(string Id)
        {
            var footerLink = await _bllFooterLink.GetFooterLink(Id);
            return Json(new
            {
                Success = true,
                Value = footerLink
            });
        }

        [HttpPost]
        public async Task<JsonResult> InsertUpdate(FooterLinkModel footerLinkModel)
        {
            if (ModelState.IsValid)
                try
                {
                    if (footerLinkModel.Id != null)
                    {
                        footerLinkModel.ModifiedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllFooterLink.Update(footerLinkModel);
                    }
                    else
                    {
                        footerLinkModel.Id = await _bllFooterLink.GetFooterLinkId();
                        footerLinkModel.CreatedBy = User.FindFirst(ClaimTypes.Name).Value;
                        await _bllFooterLink.Insert(footerLinkModel);
                    }
                    return Json(new { success = true, Message = MessageManager.FOOTER_LINK_SAVE_SUCCESS_MESSAGE });
                }
                catch (Exception ex)
                {
                }
            return Json(new { success = true, Message = MessageManager.FOOTER_LINK_SAVE_FAIL_MESSAGE });
        }

        [HttpDelete]
        public async Task<JsonResult> Delete(string Id)
        {
            if (ModelState.IsValid)
                try
                {
                    var result = await _bllFooterLink.Delete(Id);
                    if (result)
                        return Json(new
                        {
                            Success = true,
                            Message = MessageManager.FOOTER_LINK_DELETE_SUCCESS_MESSAGE
                        });
                    return Json(new
                    {
                        Success = false,
                        Message = MessageManager.FOOTER_LINK_DELETE_FAIL_MESSAGE
                    });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            return Json(new
            {
                Success = false,
                Message = MessageManager.FOOTER_LINK_DELETE_FAIL_MESSAGE
            });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}