﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Model.Factories;
using Sppages.Model.Master;

namespace Sppages.UserInterface.Controllers
{
    public class SearchController : Controller
    {
        private readonly BllFrontEnd _bllFrontEnd;

        public SearchController(IFrontEndService iFrontEndService, IModelFactory iModelFactory)
        {
            _bllFrontEnd = new BllFrontEnd(iFrontEndService, iModelFactory);
        }

        public ActionResult Index(string SearchBy, string Id)
        {
            ViewBag.SearchBy = SearchBy;
            ViewBag.Id = Id;
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> SearchAd(int PageSize, int PageNo, string SearchString, string SearchBy, string Id)
        {
            try
            {
                var val = await _bllFrontEnd.SearchAdByLocTag(PageSize, PageNo, SearchString, SearchBy, Id);
                return Json(new
                {
                    Success = true,
                    Value = (List<LocSummaryModel>)val.Data,
                    TotalRowsCount = val.TotalRowsCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }

        [HttpGet]
        public async Task<JsonResult> SearchPost(string SearchBy, string Id)
        {
            var location = await _bllFrontEnd.SearchPost(SearchBy, Id);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

    }
}