﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Model.Factories;
using Sppages.Model.Master;
using Sppages.UserInterface.Models;

namespace Sppages.UserInterface.Controllers
{
    public class HomeController : Controller
    {
        private readonly BllFrontEnd _bllFrontEnd;

        public HomeController(IFrontEndService iFrontEndService, IModelFactory iModelFactory)
        {
            _bllFrontEnd = new BllFrontEnd(iFrontEndService, iModelFactory);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetAllLocation()
        {
            var location = await _bllFrontEnd.GetAllLocation();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetFooterLink()
        {
            var footerLink = await _bllFrontEnd.GetFooterLink();
            return Json(new
            {
                Success = true,
                Value = footerLink
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetMenu()
        {
            var location = await _bllFrontEnd.GetMenu();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetMenuById(string Id)
        {
            var location = await _bllFrontEnd.GetMenuById(Id);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetTag()
        {
            var location = await _bllFrontEnd.GetTag();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }
        
        [HttpGet]
        public async Task<JsonResult> GetRecentPost()
        {
            var location = await _bllFrontEnd.GetRecentPost();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetPopularPost()
        {
            var location = await _bllFrontEnd.GetPopularPost();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetRandomPost()
        {
            var location = await _bllFrontEnd.GetRandomPost();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetRelatedPost(string AdPostId)
        {
            var location = await _bllFrontEnd.GetRelatedPost(AdPostId);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }


        [HttpGet]
        public async Task<JsonResult> GetLatestComments()
        {
            var location = await _bllFrontEnd.GetLatestComments();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetRecentComments(string AdPostId)
        {
            var location = await _bllFrontEnd.GetRecentComments(AdPostId);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }


        [HttpGet]
        public async Task<JsonResult> SearchAd(int PageSize, int PageNo, string SearchString)
        {
            try
            {
                var val = await _bllFrontEnd.SearchAd(PageSize, PageNo, SearchString);
                return Json(new
                {
                    Success = true,
                    Value = (List<LocSummaryModel>)val.Data,
                    TotalRowsCount=val.TotalRowsCount
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false
                });
            }
        }


        [HttpGet]
        public async Task<JsonResult> GetPost()
        {
            var location = await _bllFrontEnd.GetPost();
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        public IActionResult Page(string Id)
        {
            ViewBag.Id = Id;
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult TermsOfUse()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
