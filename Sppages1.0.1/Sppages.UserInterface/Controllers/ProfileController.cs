﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Mvc;
using Sppages.Business.Derived;
using Sppages.Model.Factories;

namespace Sppages.UserInterface.Controllers
{
    public class ProfileController : Controller
    {
        private readonly BllFrontEnd _bllFrontEnd;

        public ProfileController(IFrontEndService iFrontEndService, IModelFactory iModelFactory)
        {
            _bllFrontEnd = new BllFrontEnd(iFrontEndService, iModelFactory);
        }

        public ActionResult Index(string Id)
        {
            ViewBag.Id = Id;
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetPostDetails(string PostId)
        {
            var location = await _bllFrontEnd.GetPostDetails(PostId);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetProfileTag(string Id)
        {
            var location = await _bllFrontEnd.GetProfileTag(Id);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> GetAllComments(string AdPostId)
        {
            var location = await _bllFrontEnd.GetAllComments(AdPostId);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

        [HttpGet]
        public async Task<JsonResult> AddRecentComment(string Name, string Message, string AdPostId, string EmailAddress)
        {
            var location = await _bllFrontEnd.AddRecentComment(Name, Message, AdPostId, EmailAddress);
            return Json(new
            {
                Success = true,
                Value = location
            });
        }

    }
}