﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Invoice.Service.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Configuration;
using Sppages.Business.Utility;
using Sppages.Data.Entities.Identity;
using Sppages.Model.Account;
using Sppages.Model.Customer;
using Sppages.Model.Factories;
using TastyQueen.Business.Derived;

namespace Sppages.UserInterface.Controllers
{
    public class AccountController : Controller
    {
        private readonly BllAuth _bllAuth;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IAuthService iAuthService, IModelFactory iModelFactory, IConfiguration Configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = Configuration;
            _bllAuth = new BllAuth(iAuthService, iModelFactory, Configuration);
        }

        private string GetModelStateErrorMessage()
        {
            var count = 1;
            var ErrorMsg = "";
            foreach (var var in ModelState.ToList())
                if (var.Value.ValidationState == ModelValidationState.Invalid)
                    foreach (var modelError in var.Value.Errors.ToList())
                    {
                        if (ErrorMsg == "")
                            ErrorMsg = count + ". " + modelError.ErrorMessage;
                        else
                            ErrorMsg = count + ". " + ErrorMsg + ", " + modelError.ErrorMessage;
                        count = count + 1;
                    }
            return ErrorMsg;
        }

        #region "Login/Logout"

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            //await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<JsonResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
                try
                {
                    var user = await _userManager.FindByEmailAsync(model.Email);
                    if (user == null)
                        return Json(new { Success = false, Message = "Incorrect username." });

                    var validPassword = await _userManager.CheckPasswordAsync(user, model.Password);
                    if (!validPassword)
                        return Json(new { Success = false, Message = "Incorrect password." });

                    await _signInManager.SignInAsync(user, new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTimeOffset.UtcNow.AddDays(30)
                    });

                    if (await _userManager.IsEmailConfirmedAsync(user))
                    {
                        var DashboardUrl = "";
                        if (await _userManager.IsInRoleAsync(user, "B2CCustomer"))
                        {
                            DashboardUrl = "";
                            //var company = await _bllAuth.GetChefInformation(user.ChefId);
                            //return Json(new
                            //{
                            //    Success = true,
                            //    RedirectUrl = DashboardUrl,
                            //    ChefLogo = company.Picture,
                            //    ChefAddress = company.Address,
                            //    Message = "Login successfully done."
                            //});
                        }
                        if (await _userManager.IsInRoleAsync(user, "B2BCustomer"))
                        {
                            DashboardUrl = Url.Action("Index", "B2BDashboard", new { Area = "B2B" });
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl,
                                Message = "Login successfully done."
                            });
                        }
                        if (await _userManager.IsInRoleAsync(user, "MsAdmin"))
                        {
                            DashboardUrl = Url.Action("Index", "MsAdminDashboard", new { Area = "Master" });
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl,
                                Message = "Login successfully done."
                            });
                        }
                        if (await _userManager.IsInRoleAsync(user, "Admin"))
                        {
                            DashboardUrl = Url.Action("Index", "AdminDashboard", new { Area = "Master" });
                            return Json(new
                            {
                                Success = true,
                                RedirectUrl = DashboardUrl,
                                Message = "Login successfully done."
                            });
                        }
                        return Json(new { Success = false, Message = "Incorrect username or password." });
                    }
                    await _signInManager.SignOutAsync();
                    return Json(new { Success = false, Message = "Please confirm your email address." });
                }
                catch (Exception e)
                {
                    return Json(new { Success = false, Message = "Internal exception occured." });
                }
            return Json(new { Success = false, Message = GetModelStateErrorMessage() });
        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        #endregion

        #region "Register Customer"

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> B2BCustomerUser(RegisterViewModel model)
        {
            if (ModelState.IsValid)
                try
                {
                    var userProfileModel = new B2BCustomerModel();
                    userProfileModel.Name = model.Name;
                    userProfileModel.Email = model.Email;
                    userProfileModel.CreatedBy = OperationStatus.SYSTEM_GENERATED;
                    userProfileModel= await _bllAuth.SaveB2BCustomer(userProfileModel);

                    var user = new ApplicationUser
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        B2BCustomerId = userProfileModel.Id,
                        DisplayName = model.Name
                    };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        //Assign Role to user Here 
                        await _userManager.AddToRoleAsync(user, "B2BCustomer");
                        //Ends Here

                        var emailUtil = new EmailUtil();
                        var Token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, Token },
                            Request.Scheme);

                        //var message = "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>";

                        var message =
                            "<div style=\"width: 15%; float: left; color: transparent;\">---</div> <div style=\"width: 70%; min-height: 400px; background-color: ghostwhite;float: left; \">" +
                            "<div style=\"margin-bottom: 2%; width: 59%; margin-top: 5%; margin-left: 5%;\"><img src=\"" +
                            _configuration.GetSection("ApplicationInformation")["Logo"] + "\"></div>" +
                            "<div style=\"width: 100%; \">" +
                            "<div style =\"width: 5%; float: left; color: transparent;\"> ---</div>" +
                            "<div style =\"width: 88%; background-color: white; float: left; padding: 1%; \">" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Hi " + user.DisplayName + ",</div> " +
                            "<div style =\"width: 100%;\">Greeting from " +
                            _configuration.GetSection("ApplicationInformation")["Name"] + ".</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\"> To continue setting up your account, please click the button below:<br/>" +
                            "<a href=\"" + callbackUrl +
                            "\" style=\" margin-top: 5%; background-color: #4CAF50; border: none; color: white; padding: 10px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 5%;\">Link</a>" +
                            "</div>" +
                            "<div style =\"width: 100%; margin-bottom: 3%;\">Alternatively please click the link below:<br/>" +
                            "<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>" +
                            "</div>" +
                            "<div style =\"width: 100%;\">Thanks</div>" +
                            "<div style =\"width: 100%;\">" +
                            _configuration.GetSection("ApplicationInformation")["Name"] + " Support Team</div>" +
                            "</div> " +
                            "<div style =\"width: 2%; float: left; color: transparent;\">---</div>" +
                            "</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:1%; margin-left: 5%; margin-top: 2%;\">This is a no-reply email. To make an inquiry, please contact our help.</div>" +
                            "<div style=\"width: 100%; float: left; color: gray; margin-bottom:3%; margin-left: 5%;\">© " +
                            _configuration.GetSection("ApplicationInformation")["Name"] +
                            ". All rights reserved.</div>" +
                            "</div> <div style=\"width: 15%; float: left; color: transparent;\">---</div> ";

                        await emailUtil.SendEmail(user.Email, "Verify Your Email Address", message, _configuration);

                        return Json(new
                        {
                            Success = true,
                            RedirectUrl = Url.Action("Confirm", "Account", new { user.Email }),
                            Message = "Sign up successfully complete."
                        });
                    }
                    else
                    {
                        _bllAuth.Delete(userProfileModel.Id);
                        return Json(new { Success = false, Message = result.Errors.ToList().FirstOrDefault().Description });
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            return Json(new { Success = false, Message = GetModelStateErrorMessage() });
        }

        #endregion

        #region "Email Confirmation"

        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string Token)
        {
            if (userId == null || Token == null)
                return RedirectToAction(nameof(HomeController.Index), "Home");
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            var result = await _userManager.ConfirmEmailAsync(user, Token);
            return View();
        }

        [AllowAnonymous]
        public ActionResult Confirm(string email)
        {
            ViewBag.Email = email;
            return View();
        }

        #endregion

        #region "Change Password"

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUser = await _userManager.FindByIdAsync(User.FindFirst(ClaimTypes.NameIdentifier).Value);
                var result =
                    await _userManager.ChangePasswordAsync(currentUser, model.CurrentPassword, model.NewPassword);
                if (result.Succeeded)
                {
                    var user = await _userManager.FindByIdAsync(currentUser.Id);
                    if (user != null)
                        await _signInManager.SignInAsync(user, false);
                    return Json(new
                    {
                        Success = true,
                        RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                        Message = MessageManager.CHANGE_PASSWORD_SUCCESS
                    });
                }
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.CHANGE_PASSWORD_INCORRECT_CURRENT_PASSWORD
                });
            }
            return Json(new
            {
                Success = false,
                RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                Message = GetModelStateErrorMessage()
            });
            return Json(new
            {
                Success = false,
                RedirectUrl = "",
                Message = MessageManager.CHANGE_PASSWORD_FAIL
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ChangePasswordConfirmation()
        {
            return View();
        }

        #endregion

        #region "Reset Password"

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string EmailAddres, string Token)
        {
            ViewBag.EmailAddress = EmailAddres;
            ViewBag.Token = Token;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(model.Email);
                if (user == null)
                    return Json(new
                    {
                        Success = false,
                        RedirectUrl = "",
                        Message = MessageManager.RESET_PASSWORD_INVALID_USER
                    });
                var result = await _userManager.ResetPasswordAsync(user, model.Token, model.NewPassword);
                if (result.Succeeded)
                    return Json(new
                    {
                        Success = true,
                        RedirectUrl = Url.Action("ResetPasswordConfirmation", "Account", null),
                        Message = MessageManager.RESET_PASSWORD_SUCCESS_MESSAGE
                    });
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.RESET_PASSWORD_FAIL_MESSAGE
                });
            }
            return Json(new
            {
                Success = false,
                RedirectUrl = Url.Action("ChangePasswordConfirmation", "Account", null),
                Message = GetModelStateErrorMessage()
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        #endregion

        #region "Forget Password"

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> ForgotPassword(string EmailAddress)
        {
            try
            {
                var user = await _userManager.FindByEmailAsync(EmailAddress);
                if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
                    return Json(new
                    {
                        Success = false,
                        RedirectUrl = "",
                        Message = MessageManager.RESET_PASSWORD_INVALID_USER
                    });

                var Token = await _userManager.GeneratePasswordResetTokenAsync(user);

                var callbackUrl = Url.Action("ResetPassword", "Account", new { EmailAddres = user.Email, Token },
                    Request.Scheme);

                var message =
                    "<div style=\"width: 15%; float: left; color: transparent;\">---</div> <div style=\"width: 70%; min-height: 400px; background-color: ghostwhite;float: left; \">" +
                    "<div style=\"margin-bottom: 2%; width: 59%; margin-top: 5%; margin-left: 5%;\"><img src=\"" +
                    _configuration.GetSection("ApplicationInformation")["Logo"] + "\"></div>" +
                    "<div style=\"width: 100%; \">" +
                    "<div style =\"width: 5%; float: left; color: transparent;\"> ---</div>" +
                    "<div style =\"width: 88%; background-color: white; float: left; padding: 1%; \">" +
                    "<div style =\"width: 100%; margin-bottom: 3%;\">Hi " + user.DisplayName + ",</div> " +
                    "<div style =\"width: 100%; margin-bottom: 3%;\"> Please click on the below button for reset your password<br/>" +
                    "<a href=\"" + callbackUrl +
                    "\" style=\" margin-top: 5%; background-color: #4CAF50; border: none; color: white; padding: 10px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 5%;\">Reset</a>" +
                    "</div>" +
                    "<div style =\"width: 100%; margin-bottom: 3%;\">Alternatively please click the link below:<br/>" +
                    "<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>" +
                    "</div>" +
                    "<div style =\"width: 100%;\">Thanks</div>" +
                    "<div style =\"width: 100%;\">" + _configuration.GetSection("ApplicationInformation")["Name"] +
                    " Support Team</div>" +
                    "</div> " +
                    "<div style =\"width: 2%; float: left; color: transparent;\">---</div>" +
                    "</div>" +
                    "<div style=\"width: 100%; float: left; color: gray; margin-bottom:1%; margin-left: 5%; margin-top: 2%;\">This is a no-reply email. To make an inquiry, please contact our help.</div>" +
                    "<div style=\"width: 100%; float: left; color: gray; margin-bottom:3%; margin-left: 5%;\">Copyright © " +
                    _configuration.GetSection("ApplicationInformation")["Name"] + ". All rights reserved.</div>" +
                    "</div> <div style=\"width: 15%; float: left; color: transparent;\">---</div> ";

                var emailUtil = new EmailUtil();
                await emailUtil.SendEmail(user.Email, MessageManager.RESET_PASSWORD_EMAIL_SUBJECT, message,
                    _configuration);

                return Json(new
                {
                    Success = true,
                    RedirectUrl = Url.Action("ForgotPasswordConfirmation", "Account", null),
                    Message = MessageManager.RESET_PASSWORD_REQUEST_MESSAGE
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Success = false,
                    RedirectUrl = "",
                    Message = MessageManager.RESET_PASSWORD_FAIL_MESSAGE
                });
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        #endregion

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, PhoneNumber = model.PhoneNumber, DisplayName =model.FirstName, FirstName = model.FirstName, LastName = model.LastName };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //Assign Role to user Here 
                    await this._userManager.AddToRoleAsync(user, "Admin");
                    //Ends Here
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }
        
    }
}