﻿
function loadModalForImageUplod() {
    $("#imgImage").attr("src", "");
    $("#imgUploadImage").val('');
    $("#Title").val('');
    $("#upload_image_modal").modal();
}

function uploadImage() {
    var imgFiles = $("#imgUploadImage").get(0).files;
    if (imgFiles.length == '0') {
        alert("Please select a image file.");
        $("#imgUploadImage").focus();
    } else {
        blockUI_Upload_Modal();
        var fd = new FormData();
        fd.append("uploadImage", imgFiles[0]);
        fd.append("Title", $("#Title").val());
        fd.append("AdPostId", $("#Id").val());
        $.ajax({
            type: "POST",
            url: $("#adPostUploadImageUrl").val(),
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success == true) {
                    $("#imgImage").attr("src", "/ApplicationImage/AdPost/" + data.fileName);
                    $('.ad_datatable').mDatatable().search('', 'Status');
                }
                unblockUI_Upload_Modal();
            },
            failure: function (response) {
                unblockUI_Upload_Modal();
            },
            error: function (XMLHttpRequest, textStatus) {
                unblockUI_Upload_Modal();
            }
        });        
    }
}

function DeleteImage(id) {
    blockUI_Page();
    var fd = new FormData();
    fd.append("Id", id);
    $.ajax({
        type: "POST",
        url: '/Ad/AdPost/RemoveAdPostImage',//$("#adPostImageDeleteUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.ad_datatable').mDatatable().search('', 'Status');
            if (data.success)
                alert(data.message);
            else
                alert(data.message);
            unblockUI_Page();
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}
