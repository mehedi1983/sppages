﻿
jQuery(document).ready(function () {
    formValidation();
    loadLocation();
    initEditMode();
    $('#LongDescription').summernote({
        height: 100
    });
});

function initEditMode() {
    if ($("#Id").val() != "") {
        loadAdPost($("#Id").val());
        initAdPostImageDatatable();
        initTagDatatable();
        $("#ad_post_edit").show();
    }
}

function loadLocation() {
    var options = $("#LocationId");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#locationUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t,
                function () {
                    options.append($("<option />").val(this.id).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load location list.';
            alert(msg);
        }
    });
}

function loadAdPost(id) {
    blockUI_Page();
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#adPostLoadUrl").val(),
        data: { "Id": id },
        dataType: "html",
        success: function (result) {
            var parseData = JSON.parse(result);
            var employee = parseData.value;
            $("#Id").val(employee.id);
            $("#Name").val(employee.name);
            $("#Age").val(employee.age);
            $("#LongDescription").summernote("code", employee.longDescription);
            $("#SeoTitle").val(employee.seoTitle);
            $("#MetaKeywords").val(employee.metaKeywords);
            $("#MetaDescription").val(employee.metaDescription);
            $("select#LocationId").val(employee.locationId);
            $("select#Status").val(employee.status);
            unblockUI_Page();
        },
        error: function (error) {
            var msg = 'Fail to load ad post information.';
            unblockUI_Page();
            alert(msg);
        }
    });
}

function formValidation() {
    $("#btnSaveUpdate").on("click",
        function () {
            if ($("#save_information")[0].checkValidity()) {
                saveData();
            } else {
                $("#save_information")[0].reportValidity();
            }
        });
}

function saveData() {
    blockUI_Page();
    var fd = new FormData();
    fd.append("Id", $("#Id").val());
    fd.append("Name", $("#Name").val());
    fd.append("Age", $("#Age").val());
    fd.append("LongDescription", $('#LongDescription').summernote('code'));
    fd.append("MetaKeywords", $("#MetaKeywords").val());
    fd.append("MetaDescription", $("#MetaDescription").val());
    fd.append("LocationId", $("#LocationId :selected").val());
    fd.append("SeoTitle", $("#SeoTitle").val());
    fd.append("Status", $("#Status :selected").val());
    $.ajax({
        type: "POST",
        url: $("#adPostSaveUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if (data.operation == "Update") {
                $("#Id").val("");
                $("#Name").val("");
                $("#Age").val("");
                $("#LongDescription").val("");
                $("#MetaKeywords").val("");
                $("#MetaDescription").val("");
                $("select#Status").val('Active');
                $("select#LocationId").val('');
                window.location.href = data.redirectUrl;
            } else {
                $("#Id").val(data.id);

                $("#adPostImageListUrl").val("/B2B/AdPost/GetAdPostImageList?AdPostId=" + data.id);
                $("#adPostTagListUrl").val("/B2B/AdPost/GetAdPostTagList?AdPostId=" + data.id);
                initEditMode();
            }
            unblockUI_Page();
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });    
}

function loadModalInInsertMode() {
    $("#imgProfilePicture").val("");
    $("#imgImage").attr("src", "");
    $("#upload_image_modal").modal();
}

function removeImage() {
    if (confirm('Are you sure you want to delete?') == true) {
        $("#imgProfilePicture").attr("src", "/images/avatar.jpg");
        $("#Picture").val("");
    }
}

var initAdPostImageDatatable = function () {
    var datatable = $('.ad_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#adPostImageListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'url',
                title: 'Image',
                overflow: 'visible',
                width: 100,
                template: function (row, index, datatable) {
                    return "<img src=\"/ApplicationImage/AdPost/" + row.url + "\" class=\"m--img-rounded m--marginless m--img-centered\" style=\"width: 60%;\" alt=\"\">";
                }
            },
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                width: 50,
                template: function (row, index, datatable) {
                    return "<a onclick=\"javascript:if (confirm('Are you sure you want to delete?') == false) return false; DeleteImage('" +
                        row.id +
                        "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete details\"><i class=\"la la-trash\"></i></a>";
                }
            }
        ]
    });
}

var initTagDatatable = function () {
    var datatable = $('.tag_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#adPostTagListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 5,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'tagName',
                title: 'Tag Name',
                sortable: 'asc', // default sort
                filterable: false // disable or enable filtering
                //template: '{{OrderID}} - {{ShipCountry}}',
            },
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                width: 50,
                template: function (row, index, datatable) {
                    return "<a onclick=\"javascript:if (confirm('Are you sure you want to delete?') == false) return false; DeleteTag('" +
                        row.id +
                        "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete details\"><i class=\"la la-trash\"></i></a>";
                }
            }
        ]
    });
}