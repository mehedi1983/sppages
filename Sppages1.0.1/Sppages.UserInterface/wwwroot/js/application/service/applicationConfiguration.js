﻿
appModule.factory('ApplicationConfiguration', ['$http', function ($http) {
    var configurationService = {};

    configurationService.baseCurrency = function () {
        return "Tk";
    }

    configurationService.currentDate = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }

        if (mm < 10) {
            mm = '0' + mm;
        }

        today = mm +
            '/' +
            dd +
            '/' +
            yyyy +
            ' ' +
            today.getHours() +
            ":" +
            today.getMinutes() +
            ":" +
            today.getSeconds();
        return today;
    }

    configurationService.getStatus = function () {

        return $http.get('/Admin/Configuration/GetStatus');


        //$http({
        //    method: "GET",
        //    url: "/Admin/Configuration/GetStatus",
        //    params: {}
        //}).then(function mySuccess(response) {
        //    var value = response.data.value;
        //    return value;
        //});
    }

    return configurationService;

}]);