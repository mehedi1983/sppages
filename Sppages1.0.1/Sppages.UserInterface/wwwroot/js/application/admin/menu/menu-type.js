﻿
appModule.controller('menuTypeList', ['$http', '$scope', '$rootScope', '$window', 'ApplicationConfiguration',
    function ($http, $scope, $rootScope, $window, ApplicationConfiguration) {
        $scope.path = '/Admin/MenuTypeManagement/';
        $scope.saveUrl = $scope.path + '/Create';
        $scope.searchUrl = $scope.path + 'SearchMenuType';
        $rootScope.formTitle = "Menu Type Management";
        $rootScope.menuTab = "m-menu__item--active-tab";
        $scope.saveUrl = $scope.path + 'Create';
        $scope.deleteUrl = $scope.path + 'DeleteMenuType';
        $scope.insertNewMenuUrl = function () {
            $window.location.href = $scope.saveUrl;
        };

        $scope.searchString = "";
        $rootScope.totalItemCount=0;
        $rootScope.itemViewPerPage= 10;
        $rootScope.pageNo = 1;
        $scope.menuTypeList = [];

        $scope.searchMenuType = function (pageSize, pageNo, searchStr) {
            $http({
                method: "GET",
                url: $scope.searchUrl,
                params: { "PageSize": pageSize, "PageNo": pageNo, "SearchString": searchStr }
            }).then(function mySuccess(response) {
                $scope.menuTypeList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
            });
        }

        $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
            //$rootScope.pageNo = 1;
            //$scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }

        $scope.setSearchString = function () {
            $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }




        $scope.StatusList = [];
        ApplicationConfiguration.getStatus().then(function (response) {
            $scope.StatusList = response.data.value;
        });

        $scope.menuTypeModel = {
            Id: null
            , Title: null
            , Status: null
            , CreatedDate: ApplicationConfiguration.currentDate()
        };

        $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

        $scope.loadMenuTypeInsertModal = function () {
            $("#menu_type_modal").modal();
        }

        $scope.loadMenuTypeUpdateModal = function (id) {
            alert(id);
            $("#menu_type_modal").modal();
        }

        //.............................................................Save.........................................
        $scope.Save = function () {
            if ($scope.menuTypeForm.$valid) {
                blockUI_Modal();
                $http({
                    method: 'POST',
                    url: $scope.saveUrl,
                    params: $scope.menuTypeModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        $scope.menuTypeModel.Id = response.data.menuTypeId;
                        alert(response.data.message);
                        $scope.Action = 'Update';
                    }
                    unblockUI_Modal();
                    $("#menu_type_modal .close").click();
                }), function errorCallback(response) {
                    unblockUI_Modal();
                }
            }
        };
        //..............................................................End.........................................

        //.......................................................Remove Menu Picture................................
        $scope.removeMenuType = function (id) {
            blockUI_Page();
            $http({
                method: "DELETE",
                url: $scope.deleteUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                if (response.data.success == true)
                    initUserDatatable();
                unblockUI_Page();
            }, function myError(response) {
                unblockUI_Page();
            });
        };
        //..............................................................End.........................................

        //$scope.initUserDatatable = function () {
        //    var datatable = $('.menu_datatable').mDatatable({
        //        data: {
        //            type: 'remote',
        //            source: {
        //                read: {
        //                    url: $scope.searchUrl,
        //                    map: function (raw) {
        //                        var dataSet = raw;
        //                        if (typeof raw.data !== 'undefined') {
        //                            dataSet = raw.data;
        //                        }
        //                        return dataSet;
        //                    }
        //                }
        //            },
        //            pageSize: 10,
        //            serverPaging: true,
        //            serverFiltering: true,
        //            serverSorting: true
        //        },

        //        layout: {
        //            scroll: false,
        //            footer: false
        //        },

        //        sortable: true,

        //        pagination: true,

        //        toolbar: {
        //            placement: ['bottom'],
        //            items: {
        //                pagination: {
        //                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
        //                }
        //            }
        //        },

        //        search: {
        //            input: $('#generalSearch')
        //        },

        //        columns: [
        //            {
        //                field: 'id',
        //                title: 'Id',
        //                width: 250
        //            },
        //            {
        //                field: 'title',
        //                title: 'Menu Type',
        //                width: 250
        //            },                
        //            {
        //                field: 'status',
        //                title: 'Status',
        //                sortable: false,
        //                width: 75,
        //                template: function (row) {
        //                    var status = {
        //                        'Active': { 'title': 'Active', 'class': ' m-badge--success' },
        //                        'Inactive': { 'title': 'Inactive', 'class': ' m-badge--danger' },
        //                    };
        //                    return '<span class="m-badge ' +
        //                        status[row.status].class +
        //                        ' m-badge--wide">' +
        //                        status[row.status].title +
        //                        '</span>';
        //                }
        //            },
        //            {
        //                field: 'Actions',
        //                title: 'Actions',
        //                width: 100,
        //                sortable: false,
        //                overflow: 'visible',
        //                template: function (row, index, datatable) {
        //                   // return //" <a ng-click=\"loadMenuTypeInsertModal();\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Edit Menu Type\"><i class=\"la la-edit\"></i></a>" +
        //                    return "<a ng-confirm-click=\"Are you sure to delete this record?\" confirmed-click=\"removeMenuType(" + row.id + ")\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete Menu Type\"><i class=\"la la-trash\"></i></a>";
        //                }
        //            }
        //        ]
        //    });

        //    $('#m_form_status').on('change',
        //        function () {
        //            datatable.search($(this).val(), 'Status');
        //        });
        //};

        //$scope.initUserDatatable();





    }]);


