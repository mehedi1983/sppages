﻿
appModule.controller('menuList', ['$scope', '$rootScope', '$window', 'ApplicationConfiguration', function ($scope, $rootScope, $window, ApplicationConfiguration) {
    $scope.path = '/Admin/MenuManagement/';
    $scope.searchUrl = $scope.path +'SearchMenu';
    $rootScope.formTitle = "Menu Management";
    $rootScope.menuTab = "m-menu__item--active-tab";
    $scope.saveUrl = $scope.path + 'Create';
    $scope.deleteUrl = $scope.path + 'DeleteMenu';
    $scope.insertNewMenuUrl = function() {
        $window.location.href = $scope.saveUrl;
    };

    $scope.baseCurrency = ApplicationConfiguration.baseCurrency();

    //.......................................................Remove Menu Picture................................
    $scope.removeMenu = function (id) {
        blockUI_Page();
        $http({
            method: "DELETE",
            url: $scope.deleteUrl,
            params: { "Id": id }
        }).then(function mySuccess(response) {
            if (response.data.success == true)
                initUserDatatable();
            unblockUI_Page();
        }, function myError(response) {
            unblockUI_Page();
        });
    };
    //..............................................................End.........................................

    $scope.initUserDatatable = function () {
        var datatable = $('.menu_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: $scope.searchUrl,
                        map: function (raw) {
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },

            layout: {
                scroll: false,
                footer: false
            },

            sortable: true,

            pagination: true,

            toolbar: {
                placement: ['bottom'],
                items: {
                    pagination: {
                        pageSizeSelect: [10, 20, 30, 40, 50, 100]
                    }
                }
            },

            search: {
                input: $('#generalSearch')
            },

            columns: [
                {
                    field: 'title',
                    title: 'Menu',
                    width: 250
                },
                {
                    field: 'menuTypeName',
                    title: 'Type',
                    width: 100
                },
                {
                    field: 'servings',
                    title: 'Servings',
                    width: 95,
                    textAlign: 'right',
                    template: function (row, index, datatable) {
                        return "1:" + row.servings;
                    }
                },
                {
                    field: 'price',
                    title: 'Price',
                    width: 60,
                    textAlign: 'right',
                    template: function (row, index, datatable) {
                        return row.price + " "+ $scope.baseCurrency;
                    }
                },
                {
                    field: 'minimumOrderQty',
                    title: 'Minimum Order Qty',
                    textAlign: 'right',
                    width: 155
                },
                
                //{
                //    field: 'availableDateRange',
                //    title: 'Available Date',
                //    sortable: false,
                //    width: 110
                //},
                //{
                //    field: 'availableTimeRange',
                //    title: 'Available Time',
                //    sortable: false,
                //    width: 105
                //},
                {
                    field: 'status',
                    title: 'Status',
                    sortable: false,
                    width: 75,
                    template: function (row) {
                        var status = {
                            'Active': { 'title': 'Active', 'class': ' m-badge--success' },
                            'Inactive': { 'title': 'Inactive', 'class': ' m-badge--danger' },
                        };
                        return '<span class="m-badge ' +
                            status[row.status].class +
                            ' m-badge--wide">' +
                            status[row.status].title +
                            '</span>';
                    }
                },
                {
                    field: 'Actions',
                    title: 'Actions',
                    width: 100,
                    sortable: false,
                    overflow: 'visible',
                    template: function (row, index, datatable) {
                        return " <a href='/Admin/MenuManagement/Create/" + row.id + "' class=\"m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Edit Menu\"><i class=\"la la-edit\"></i></a>" +
                            " <a ng-click=\"\" ng-confirm-click=\"Are you sure to delete this record?\" confirmed-click=\"removeMenu(" + row.id + ")\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete Menu\"><i class=\"la la-trash\"></i></a>";
                    }
                }
            ]
        });

        $('#m_form_status').on('change',
            function () {
                datatable.search($(this).val(), 'Status');
            });
    };

    $scope.initUserDatatable();
}]);


