﻿
appModule.controller('layoutFrontHeaderCtrl', ['$http', '$scope', '$window', 'ApplicationConfiguration', function ($http, $scope, $window, ApplicationConfiguration) {
    $scope.path = '/Home/';
    $scope.locationUrl = $scope.path + 'GetAllLocation';
    $scope.loginUrl = '/Account/Login';
    $scope.signupB2BUrl = '/Account/B2BCustomerUser';
    $scope.privacyUrl = $scope.path + 'Privacy';
    $scope.termsOfUseUrl = $scope.path + 'TermsOfUse';
    $scope.forgetPasswordUrl = '/Account/ForgotPassword';
    $scope.LocationList = [];
    $scope.loginModel = {
        Email: null
        , Password: null
    };
    $scope.signupModel = {
        Name: null
        , Email: null
        , Password: null
        , ConfirmPassword: null
        , IAgree: false
        , CreatedDate: ApplicationConfiguration.currentDate()
    };

    $scope.getForgetPasswordUrl = function () {
        $window.location.href = $scope.forgetPasswordUrl;
    };

    $scope.getPrivacyUrl = function () {
        $window.location.href = $scope.privacyUrl;
    };

    $scope.getTermsOfUseUrl = function () {
        $window.location.href = $scope.termsOfUseUrl;
    };

    $scope.loadModalInInsertMode = function () {
        $("#sign_up_modal").modal();
    };

    $scope.loginModal = function () {
        $("#login_modal").modal();
    };

    $scope.login = function () {
        if ($scope.loginModelForm.$valid) {
            blockUI_login_Modal();
            $http({
                method: 'POST',
                url: $scope.loginUrl,
                params: $scope.loginModel,
                headers: { 'Content-Type': "application/json" },
                dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $("#lblMsg").append(response.data.message);
                    $window.location.href = response.data.redirectUrl;
                }
                unblockUI_login_Modal();
            }), function errorCallback(response) {
                unblockUI_login_Modal();
            };
        }
    };

    $scope.b2BCustomerSignup = function () {
        if ($scope.signupModelForm.$valid) {
            blockUI_signup_Modal();
            $http({
                method: 'POST',
                url: $scope.signupB2BUrl,
                params: $scope.signupModel,
                headers: { 'Content-Type': "application/json" },
                dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $window.location.href = response.data.redirectUrl;
                }  
                $("#lblModalSignupMsg").append();
                $("#lblModalSignupMsg").append(response.data.message);
                unblockUI_signup_Modal();
            }), function errorCallback(response) {
                unblockUI_signup_Modal();
            };
        };
    };

    $scope.GetLocation = function () {
        $http({
            method: "GET",
            url: $scope.locationUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.LocationList = value;
        });
    };

    $scope.GetLocation();

}]);

appModule.controller('layoutFrontFooterCtrl', ['$http', '$scope', '$window', function ($http, $scope, $window) {
    $scope.path = '/Home/';
    $scope.tagUrl = $scope.path + 'GetTag';
    $scope.footerLinkUrl = $scope.path + 'GetFooterLink';
    $scope.menuUrl = $scope.path + 'GetMenu';
    $scope.recentCommentsUrl = $scope.path + 'GetLatestComments';
    $scope.recentPostsUrl = $scope.path + 'GetRecentPost';
    $scope.pageUrl = $scope.path + 'Page';
    $scope.MenuList = [];
    $scope.TagList = [];
    $scope.FooterLinkList = [];
    $scope.RecentCommentsList = [];
    $scope.RecentPostsList = [];

    $scope.getPageUrl = function (Id) {
        $window.location.href = $scope.pageUrl + "?Id="+ Id;
    };

    $scope.GetMenu = function () {
        $http({
            method: "GET",
            url: $scope.menuUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.MenuList = value;
        });
    };

    $scope.GetFooterLink = function () {
        $http({
            method: "GET",
            url: $scope.footerLinkUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.FooterLinkList = value;
        });
    };

    $scope.GetTag = function () {
        $http({
            method: "GET",
            url: $scope.tagUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.TagList = value;
        });
    };

    $scope.GetRecentComments = function () {
        $http({
            method: "GET",
            url: $scope.recentCommentsUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.RecentCommentsList = value;
        });
    };

    $scope.GetRecentPosts = function () {
        $http({
            method: "GET",
            url: $scope.recentPostsUrl,
            params: {}
        }).then(function mySuccess(response) {
            var value = response.data.value;
            $scope.RecentPostsList = value;
        });
    };

    $scope.GetMenu();
    $scope.GetTag();
    $scope.GetRecentComments();
    $scope.GetRecentPosts();
    $scope.GetFooterLink();
}]);