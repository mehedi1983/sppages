﻿
appModule.controller("newMenuInformation", ['$scope', '$rootScope', '$window', '$http', '$location', '$sce',
    function ($scope, $rootScope, $window, $http, $location, $sce) {
        $scope.menuId = $location.absUrl().substring($location.absUrl().lastIndexOf("/")).replace("/", "");
        $rootScope.formTitle = "";
        $rootScope.menuTab = "m-menu__item--active-tab";
        $scope.imgImage = null;
        $scope.path = '/Menu/Management';
        $scope.defaultValuePath = $scope.path + '/LoadFormDefault';
        $scope.getMenuInformationPath = $scope.path + '/GetMenu';
        $scope.saveUrl = $scope.path + '/Create';
        $scope.deleteMenuPictureUrl = $scope.path + '/DeleteMenuPicture';
        $scope.uploadMenuPictureUrl = $scope.path + '/UploadMenuPicture';
        $scope.menuScheduleInsertUrl = $scope.path + '/InsertSchedule';
        $scope.menuScheduleDeleteUrl = $scope.path + '/DeleteSchedule';
        $scope.menuCancellationPolicyInsertUrl = $scope.path + '/InsertCancellationPolicy';
        $scope.menuCancellationPolicyDeleteUrl = $scope.path + '/DeleteCancellationPolicy';
        $scope.backUrl = $scope.path + '/Index';
        $scope.imageBrowsingPath = "/ApplicationImage/Menu/";
        $scope.menuPictureModels = [];
        $scope.menuScheduleModels = [];
        $scope.menuCancellationPolicyModels = [];

        $scope.back = function () {
            $window.location.href = $scope.backUrl;
        };

        $scope.currentDate = function () {
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = mm +
                '/' +
                dd +
                '/' +
                yyyy +
                ' ' +
                today.getHours() +
                ":" +
                today.getMinutes() +
                ":" +
                today.getSeconds();
            return today;
        };

        $scope.rDate = function (cDate) {

            var today = new Date(cDate);

            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd;
            }

            if (mm < 10) {
                mm = '0' + mm;
            }

            today = mm + '/' + dd + '/' + yyyy + ' ' + today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

            return today;
        }

        $scope.menuInformation = {
            Id: null
            , ChefId: null
            , Title: null
            , Description: null
            , Servings: null
            , Price: null
            , MinimumOrderQty: null
            , SpecialNote: null
            , MenuTypeId: null
            , Status: null
            , AvailableFromDate: null
            , AvailableToDate: null

            , CreatedDate: $scope.currentDate()
        };

        $scope.menuSchedule = {
            Id: null
            , MenuId: null
            , StartDate: null
            , EndDate: null
            , StartTime: null
            , EndTime: null
            , CreatedDate: $scope.currentDate()
        };

        $scope.menuCancellationPolicy = {
            Id: null
            , MenuId: null
            , HoursBefore: null
            , Percentage: null
            , CreatedDate: $scope.currentDate()
        };

        $scope.MenuTypeList = [];
        $scope.StatusList = [];

        $scope.loadImageUploadModal = function () {
            $scope.imgImage = null;
            $("#imgProfilePicture").val("");
            $("#imgImage").attr("src", "");
            $("#imgUploadImage").val('');
            $("#upload_image_modal").modal();
        }

        $scope.loadScheduleModal = function () {
            $("#new_schedule_modal").modal();
        }

        $scope.loadCancellationPolicyModal = function () {
            $("#new_cancellation_policy_modal").modal();
        }

        //............................................. Get form default value in insert mode.......................
        $scope.getFormDefaultValue = function () {
            $http({
                method: "GET",
                url: $scope.defaultValuePath,
                params: {}
            }).then(function mySuccess(response) {
                var value = response.data.value;
                $scope.MenuTypeList = value.menuType;
                //$scope.StatusList = value.status;
            });
        };
        //..............................................................End.........................................

        //............................................. Get form default value in insert mode.......................
        $scope.getMenuInformation = function (id) {
            $http({
                method: "GET",
                url: $scope.getMenuInformationPath,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                var value = response.data.value;
                $scope.menuInformation.Id = value.id;
                $scope.menuInformation.ChefId = value.chefId;
                $scope.MenuTypeList = value.menuFormDefaultModel.menuType;
                //$scope.StatusList = value.menuFormDefaultModel.status;
                $scope.menuInformation.Title = value.title;
                $scope.menuInformation.MenuTypeId = { id: value.menuTypeId, value: value.menuTypeName };
                $scope.menuInformation.Price = value.price;
                $scope.menuInformation.Servings = value.servings;
                $scope.menuInformation.MinimumOrderQty = value.minimumOrderQty;
                $scope.menuInformation.SpecialNote = value.specialNote;
                $scope.menuInformation.Description = value.description;

                if (value.status == 'Active') {
                    $scope.StatusList.push({ id: '1', value: 'Active' });
                    $scope.menuInformation.Status = { id: '1', value: 'Active' };
                } else {
                    $scope.StatusList.push({ id: '1', value: 'Inactive' });
                    $scope.menuInformation.Status = { id: '1', value: 'Inactive' };
                }

                $scope.menuPictureModels = value.menuPictures;
                $scope.menuScheduleModels = value.schedules;
                $scope.menuCancellationPolicyModels = value.cancellationPolicyModels;
            });
        };
        //..............................................................End.........................................

        //............................................. Form Insert or Update mode check............................
        if ($scope.menuId == "" || $scope.menuId == "Create") {
            $scope.getFormDefaultValue();
            $scope.Action = 'Save';
            $rootScope.formTitle = "Insert New Menu";
            $scope.StatusList.push({ id: '2', value: 'Inactive' });
            $scope.menuInformation.Status = { id: '2', value: 'Inactive' };

        } else {
            $scope.getMenuInformation($scope.menuId);
            $scope.Action = 'Update';
            $rootScope.formTitle = "Update Menu";
        }
        //..............................................................End.........................................

        //.............................................................Save.........................................
        $scope.Save = function () {
            if ($scope.menuInformationForm.$valid) {
                blockUI_Page();
                $http({
                    method: 'POST',
                    url: $scope.saveUrl,
                    params: $scope.menuInformation,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        $scope.menuInformation.Id = response.data.menuId;
                        alert(response.data.message);
                        $scope.Action = 'Update';
                    }
                    unblockUI_Page();
                }), function errorCallback(response) {
                    unblockUI_Page();
                }
            }
        };
        //..............................................................End.........................................

        //.............................................................Clear........................................
        $scope.Clear = function () {
            $scope.Action = 'Save';
            $scope.menuInformation = {};
            $('#Description').val("");
            return true;
        }
        //..............................................................End.........................................

        //.......................................................Upload Menu Picture................................
        $scope.uploadMenuPicture = function () {
            var imgFiles = $("#imgUploadImage").get(0).files;

            if (imgFiles.length == '0') {
                alert("Please select a image file.");
                $("#imgUploadImage").focus();
            } else {
                blockUI_Upload_Modal();

                var fd = new FormData();
                fd.append("ProfilePicture", imgFiles[0], imgFiles[0].name);
                fd.append("MenuId", $scope.menuInformation.Id);
                fd.append("cDate", $scope.currentDate());

                $http.post($scope.uploadMenuPictureUrl,
                    fd,
                    {
                        transformRequest: angular.identity,
                        headers: { 'Content-Type': undefined }
                    }).then(function (response) {
                        if (response.data.success == true) {
                            $scope.imgImage = $scope.imageBrowsingPath + response.data.menuPictureModel.fileName;
                            $scope.imgProfilePicture = $scope.imageBrowsingPath + response.data.menuPictureModel.fileName;
                            $scope.menuPictureModels.push(response.data.menuPictureModel);
                        }
                        unblockUI_Upload_Modal();                        
                    }, function errorCallback(response) {
                        unblockUI_Upload_Modal();
                    });
            }
        };
        //..............................................................End.........................................

        //.......................................................Remove Menu Picture................................
        $scope.removeMenuPicture = function (id, $index) {
            blockUI_Upload_Modal();
            $http({
                method: "DELETE",
                url: $scope.deleteMenuPictureUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                if (response.data.success == true)
                    $scope.menuPictureModels.splice($index, 1);
                unblockUI_Upload_Modal();
            }, function myError(response) {
                unblockUI_Upload_Modal();
            });
        };
        //..............................................................End.........................................

        //.......................................................Upload Schedule....................................
        $scope.addSchedule = function () {
            blockUI_Modal();
            $scope.menuSchedule.MenuId = $scope.menuInformation.Id;
            $scope.menuSchedule.StartDate = $scope.rDate($('#DateRange').data('daterangepicker').startDate);
            $scope.menuSchedule.EndDate = $scope.rDate($('#DateRange').data('daterangepicker').endDate);
            $scope.menuSchedule.StartTime = $("#StartTime").val();
            $scope.menuSchedule.EndTime = $("#EndTime").val();
            $http({
                method: 'POST',
                url: $scope.menuScheduleInsertUrl,
                params: $scope.menuSchedule,
                dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $scope.menuScheduleModels.push(response.data.scheduleModel);
                    alert(response.data.message);
                }
                unblockUI_Modal();
                $("#new_schedule_modal .close").click();
            }), function errorCallback(response) {
                unblockUI_Modal();
            }
        };
        //..............................................................End.........................................

        //.......................................................Remove Schedule....................................
        $scope.removeSchedule = function (id, $index) {
            blockUI_Modal();
            $http({
                method: "DELETE",
                url: $scope.menuScheduleDeleteUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                if (response.data.success == true)
                    $scope.menuScheduleModels.splice($index, 1);
                unblockUI_Modal();
            }, function myError(response) {
                unblockUI_Modal();
            });
        };
        //..............................................................End.........................................

        //.......................................................Insert Cancellation Policy................................
        $scope.addCancellationPolicy = function () {
            if ($scope.cancellationPolicyForm.$valid) {
                blockUI_Modal();
                $scope.menuCancellationPolicy.MenuId = $scope.menuInformation.Id;
                $http({
                    method: 'POST',
                    url: $scope.menuCancellationPolicyInsertUrl,
                    params: $scope.menuCancellationPolicy,
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        $scope.menuCancellationPolicyModels.push(response.data.menuCancellationPolicyModel);
                        $scope.menuCancellationPolicy = [];
                        alert(response.data.message);
                    }
                    unblockUI_Modal();
                    $("#new_cancellation_policy_modal .close").click();
                }), function errorCallback(response) {
                    unblockUI_Modal();
                }
            }           
        };
        //..............................................................End.........................................

        //.......................................................Remove Cancellation Policy................................
        $scope.removeCancellationPolicy = function (id, $index) {
            blockUI_Modal();
            $http({
                method: "DELETE",
                url: $scope.menuCancellationPolicyDeleteUrl,
                params: { "Id": id }
            }).then(function mySuccess(response) {
                if (response.data.success == true)
                    $scope.menuCancellationPolicyModels.splice($index, 1);
                unblockUI_Modal();
            }, function myError(response) {
                unblockUI_Modal();
            });
        };
        //..............................................................End.........................................

    }]);
























//function loadModalInInsertMode() {
//    $("#imgProfilePicture").val("");
//    $("#imgImage").attr("src", "");
//    $("#imgUploadImage").val('');
//    $("#upload_image_modal").modal();
//}

//function initSelectBox() {
//    $('#CityId').select2({
//        placeholder: "Select a city"
//    });

//    $('#CityId').on('select2:select', function (e) {
//        loadLocation($('#CityId').val());
//    });

//    $('#LocationId').select2({
//        placeholder: "Select a location"
//    });

//    $('#LocationId').on('select2:select', function (e) {

//    });

//}

//function loadChefInformation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#getChefUrl").val(),
//        data: { "Id": $("#Id").val() },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;
//            $("#Id").val(value.id);
//            $("#Name").val(value.name);
//            $("#Email").val(value.email);
//            $("#PhoneNumber").val(value.phoneNumber);
//            $("#MobileNumber").val(value.mobileNumber);
//            $("#Address").val(value.address);
//            $("#ZipCode").val(value.zipCode);
//            $("#Website").val(value.website);

//            $("#CityId").empty();
//            $.each(value.citySummaryModels,
//                function () {
//                    $("#CityId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#CityId").select2("val", [value.cityId]);

//            $("#LocationId").empty();
//            $.each(value.locationSummaryModels,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#LocationId").select2("val", [value.locationId]);

//            if (value.picture != null && value.picture != "")
//                $("#imgProfilePicture").attr("src", "/ApplicationImage/CompanyLogo/" + value.picture);

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load chef information.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function loadLocation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#loadLocationUrl").val(),
//        data: { "CityId": id },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;

//            $("#LocationId").empty();
//            $.each(value,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load location.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function formValidation() {
//    $("#btnSaveUpdate").on("click",
//        function () {
//            if ($("#save_information")[0].checkValidity()) {
//                saveData();
//            } else {
//                $("#save_information")[0].reportValidity();
//            }
//        });
//}

//function saveData() {
//    blockUI_Page_Saving_Info();
//    var fd = new FormData();
//    fd.append("Id", $("#Id").val());
//    fd.append("Name", $("#Name").val());
//    fd.append("Email", $("#Email").val());
//    fd.append("PhoneNumber", $("#PhoneNumber").val());
//    fd.append("MobileNumber", $("#MobileNumber").val());
//    fd.append("Address", $("#Address").val());
//    fd.append("LocationId", $('#LocationId').val());
//    fd.append("ZipCode", $("#ZipCode").val());
//    $.ajax({
//        type: "POST",
//        url: $("#saveChefInfoUrl").val(),
//        data: fd,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (data) {
//            if (data.success) {
//                jsonObj = [];
//                item = {};
//                item["ChefLogo"] = data.chefLogo;
//                item["ChefAddress"] = data.chefAddress;
//                item["DashboardUrl"] = data.redirectUrl;
//                item["ChefId"] = data.chefId;
//                jsonObj.push(item);
//                var jsonString = JSON.stringify(jsonObj);
//                sessionStorage.setItem("ChefBasicInfo", jsonString);

//                document.location.href = data.redirectUrl;
//            }
//            unblockUI_Page_Loading_Info();
//        },
//        failure: function (response) {
//            unblockUI_Page_Loading_Info();
//        },
//        error: function (XMLHttpRequest, textStatus) {
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}