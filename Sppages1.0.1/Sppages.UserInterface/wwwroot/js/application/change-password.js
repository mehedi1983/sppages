﻿appModule.controller('changePasswordCtrl', ['$http', '$scope', '$window', function ($http, $scope, $window) {

    $scope.changePasswordModel = {
         CurrentPassword: null
        , NewPassword: null
        , ConfirmPassword: null
    };

    $scope.getHomeUrl = function () {
        $window.location.href = "/Home/";
    };

    $scope.saveChangePassword = function () {
        if ($scope.changePasswordform.$valid) {
            blockUI_Upload_Modal();
                $http({
                    method: 'POST',
                    url: '/Account/ChangePassword',
                    params: $scope.changePasswordModel,
                    headers: { 'Content-Type': "application/json" },
                    dataType: 'JSON'
                }).then(function successCallback(response) {
                    if (response.data.success == true) {
                        $("#lblMsg").append(response.data.message);
                        $window.location.href = response.data.redirectUrl;
                    }
                    unblockUI_Upload_Modal();
                }), function errorCallback(response) {
                    unblockUI_Upload_Modal();
                };
        }
    };

}]);