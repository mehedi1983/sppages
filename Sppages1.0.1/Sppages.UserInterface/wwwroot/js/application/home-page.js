﻿
appModule.controller("homePageCtrl", ['$scope', '$rootScope', '$http',
    function ($scope, $rootScope, $http) {
        $rootScope.formTitle = "Home Page";
        $scope.path = '/Home';
        $scope.getAdPostPath = $scope.path + '/GetPost';
        $scope.searchUrl = $scope.path + '/SearchAd';
        $scope.getAdPostList = [];



        $scope.searchString = "";
        $rootScope.totalItemCount = 0;
        $rootScope.itemViewPerPage = 10;
        $rootScope.pageNo = 1;
        $scope.menuTypeList = [];

        $scope.searchMenuType = function (pageSize, pageNo, searchStr) {
            $http({
                method: "GET",
                url: $scope.searchUrl,
                params: { "PageSize": pageSize, "PageNo": pageNo, "SearchString": searchStr }
            }).then(function mySuccess(response) {
                $scope.getAdPostList = response.data.value;
                $scope.totalItemCount = response.data.totalRowsCount;
            });
        }

        $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);

        $scope.setViewPerPage = function (no) {
            $rootScope.itemViewPerPage = no;
            //$rootScope.pageNo = 1;
            //$scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }

        $scope.getPageNo = function (pNo) {
            $rootScope.pageNo = pNo;
            $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }

        $scope.setSearchString = function () {
            $scope.searchMenuType($rootScope.itemViewPerPage, $rootScope.pageNo, $scope.searchString);
        }














        //............................................. Get form default value in insert mode.......................
        $scope.getFormDefaultValue = function () {
            $http({
                method: "GET",
                url: $scope.getAdPostPath,
                params: {}
            }).then(function mySuccess(response) {
                var value = response.data.value;
                $scope.getAdPostList = value;

            });
        };
        //..............................................................End.........................................        


        //$scope.getFormDefaultValue();

    }]);
























//function loadModalInInsertMode() {
//    $("#imgProfilePicture").val("");
//    $("#imgImage").attr("src", "");
//    $("#imgUploadImage").val('');
//    $("#upload_image_modal").modal();
//}

//function initSelectBox() {
//    $('#CityId').select2({
//        placeholder: "Select a city"
//    });

//    $('#CityId').on('select2:select', function (e) {
//        loadLocation($('#CityId').val());
//    });

//    $('#LocationId').select2({
//        placeholder: "Select a location"
//    });

//    $('#LocationId').on('select2:select', function (e) {

//    });

//}

//function loadChefInformation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#getChefUrl").val(),
//        data: { "Id": $("#Id").val() },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;
//            $("#Id").val(value.id);
//            $("#Name").val(value.name);
//            $("#Email").val(value.email);
//            $("#PhoneNumber").val(value.phoneNumber);
//            $("#MobileNumber").val(value.mobileNumber);
//            $("#Address").val(value.address);
//            $("#ZipCode").val(value.zipCode);
//            $("#Website").val(value.website);

//            $("#CityId").empty();
//            $.each(value.citySummaryModels,
//                function () {
//                    $("#CityId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#CityId").select2("val", [value.cityId]);

//            $("#LocationId").empty();
//            $.each(value.locationSummaryModels,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });
//            $("#LocationId").select2("val", [value.locationId]);

//            if (value.picture != null && value.picture != "")
//                $("#imgProfilePicture").attr("src", "/ApplicationImage/CompanyLogo/" + value.picture);

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load chef information.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function loadLocation(id) {
//    blockUI_Page_Loading_Info();
//    $.ajax({
//        type: "GET",
//        cache: false,
//        url: $("#loadLocationUrl").val(),
//        data: { "CityId": id },
//        dataType: "html",
//        success: function (result) {
//            var parseData = JSON.parse(result);
//            var value = parseData.value;

//            $("#LocationId").empty();
//            $.each(value,
//                function () {
//                    $("#LocationId").append($("<option />").val(this.id).text(this.name));
//                });

//            unblockUI_Page_Loading_Info();
//        },
//        error: function (error) {
//            var msg = 'Fail to load location.';
//            alert(msg);
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}

//function formValidation() {
//    $("#btnSaveUpdate").on("click",
//        function () {
//            if ($("#save_information")[0].checkValidity()) {
//                saveData();
//            } else {
//                $("#save_information")[0].reportValidity();
//            }
//        });
//}

//function saveData() {
//    blockUI_Page_Saving_Info();
//    var fd = new FormData();
//    fd.append("Id", $("#Id").val());
//    fd.append("Name", $("#Name").val());
//    fd.append("Email", $("#Email").val());
//    fd.append("PhoneNumber", $("#PhoneNumber").val());
//    fd.append("MobileNumber", $("#MobileNumber").val());
//    fd.append("Address", $("#Address").val());
//    fd.append("LocationId", $('#LocationId').val());
//    fd.append("ZipCode", $("#ZipCode").val());
//    $.ajax({
//        type: "POST",
//        url: $("#saveChefInfoUrl").val(),
//        data: fd,
//        cache: false,
//        contentType: false,
//        processData: false,
//        success: function (data) {
//            if (data.success) {
//                jsonObj = [];
//                item = {};
//                item["ChefLogo"] = data.chefLogo;
//                item["ChefAddress"] = data.chefAddress;
//                item["DashboardUrl"] = data.redirectUrl;
//                item["ChefId"] = data.chefId;
//                jsonObj.push(item);
//                var jsonString = JSON.stringify(jsonObj);
//                sessionStorage.setItem("ChefBasicInfo", jsonString);

//                document.location.href = data.redirectUrl;
//            }
//            unblockUI_Page_Loading_Info();
//        },
//        failure: function (response) {
//            unblockUI_Page_Loading_Info();
//        },
//        error: function (XMLHttpRequest, textStatus) {
//            unblockUI_Page_Loading_Info();
//        }
//    });
//}