﻿appModule.controller('forgetPasswordCtrl', ['$http', '$scope', '$window', function ($http, $scope, $window) {

    $scope.forgetPasswordModel = {
        EmailAddress: null
    };

    $scope.getHomeUrl = function () {
        $window.location.href = "/Home/";
    };

    $scope.forgetPassword = function () {
        if ($scope.forgetPasswordForm.$valid) {
            blockUI_Upload_Modal();
            $http({
                method: 'POST',
                url: '/Account/ForgotPassword',
                params: $scope.forgetPasswordModel,
                headers: { 'Content-Type': "application/json" },
                dataType: 'JSON'
            }).then(function successCallback(response) {
                if (response.data.success == true) {
                    $("#lblMsg").append(response.data.message);
                    $window.location.href = response.data.redirectUrl;
                }
                unblockUI_Upload_Modal();
            }), function errorCallback(response) {
                unblockUI_Upload_Modal();
            };
        }
    };

}]);