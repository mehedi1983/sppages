﻿
function loadModalForAddTag() {
    loadTag();
    $("#add_tag_modal").modal();
}

function loadTag() {
    $("#TagId").empty();
    var options = $("#TagId");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#adPostLoadTagUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t,
                function () {
                    options.append($("<option />").val(this.id).text(this.title));
                });
        },
        error: function (error) {
            var msg = 'Fail to load tag list.';
            alert(msg);
        }
    });
}

function saveAdTag() {
    if ($("#TagId").val() == "") {
        alert("Please select tag.");
        $("#TagId").focus();
    } else {
        blockUI_Upload_Modal();
        var fd = new FormData();
        fd.append("TagId", $("#TagId").val());
        fd.append("AdPostId", $("#Id").val());
        $.ajax({
            type: "POST",
            url: $("#adPostSaveTagUrl").val(),
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success == true) {
                    $("#add_tag_modal .close").click();
                    $('.tag_datatable').mDatatable().search('', 'Status');
                }
                unblockUI_Upload_Modal();
            },
            failure: function (response) {
                unblockUI_Upload_Modal();
            },
            error: function (XMLHttpRequest, textStatus) {
                unblockUI_Upload_Modal();
            }
        });        
    }
}

function DeleteTag(id) {
    blockUI_Page();
    var fd = new FormData();
    fd.append("Id", id);
    $.ajax({
        type: "POST",
        url: '/Ad/AdPost/RemoveAdTag',//$("#adPostDeleteTagUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.tag_datatable').mDatatable().search('', 'Status');
            if (data.success)
                alert(data.message);
            else
                alert(data.message);
            unblockUI_Page();
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}