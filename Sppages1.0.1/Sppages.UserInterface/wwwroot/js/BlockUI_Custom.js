﻿
function blockUI_Page() {
    mApp.blockPage({
        overlayColor: '#000000',
        type: 'loader',
        state: 'primary',
        message: 'Processing...'
    });
}

function unblockUI_Page() {
    mApp.unblockPage();
}

function blockUI_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Saving...'
        });
}

function unblockUI_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}

function blockUI_Upload_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Uploading...'
        });
}

function unblockUI_Upload_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}


function blockUI_signup_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Signup Processing...'
        });
}

function unblockUI_signup_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}


function blockUI_login_Modal() {
    mApp.block('.blockui_modal .modal-content',
        {
            overlayColor: '#000000',
            type: 'loader',
            state: 'primary',
            message: 'Login Processing...'
        });
}

function unblockUI_login_Modal() {
    mApp.unblock('.blockui_modal .modal-content');
}