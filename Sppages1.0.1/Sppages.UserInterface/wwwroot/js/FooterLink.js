﻿
jQuery(document).ready(function () {
    initFooterLinkDatatable();
    //loadLocation();
    //loadParent();
    formValidation();
});

var initFooterLinkDatatable = function () {
    var datatable = $('.footer_link_datatable').mDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: $("#footerLinkListUrl").val(),
                    map: function (raw) {
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    }
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true
        },

        layout: {
            scroll: false,
            footer: false
        },

        sortable: true,

        pagination: true,

        toolbar: {
            placement: ['bottom'],
            items: {
                pagination: {
                    pageSizeSelect: [10, 20, 30, 40, 50, 100]
                }
            }
        },

        search: {
            input: $('#generalSearch')
        },

        columns: [
            {
                field: 'id',
                title: 'Id',
                sortable: 'asc', // default sort
                filterable: false // disable or enable filtering
                //template: '{{OrderID}} - {{ShipCountry}}',
            },
            {
                field: 'name',
                title: 'Name'
            },
            {
                field: 'url',
                title: 'Url'
            },
            {
                field: 'order',
                title: 'Order'
            },
            {
                field: 'createdDate',
                title: 'Created Date',
            },
            {
                field: 'status',
                title: 'Status',
                //sortable: false,
                template: function (row) {
                    var status = {
                        'Active': { 'title': 'Active', 'class': ' m-badge--success' },
                        'Inactive': { 'title': 'Inactive', 'class': ' m-badge--danger' },
                    };
                    return '<span class="m-badge ' +
                        status[row.status].class +
                        ' m-badge--wide">' +
                        status[row.status].title +
                        '</span>';
                }
            },
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    return "<a href=\"javascript: loadModalInEditMode('" +
                        row.id +
                        "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Edit details\"><i class=\"la la-edit\"></i></a> <a onclick=\"javascript:if (confirm('Are you sure you want to delete?') == false) return false; DeleteFooterLink('" +
                        row.id +
                        "');\" class=\"m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill\" title=\"Delete details\"><i class=\"la la-trash\"></i></a>";
                }
            }
        ]
    });

    $('#m_form_status').on('change',
        function () {
            datatable.search($(this).val(), 'Status');
        });

    //$('#m_form_type').on('change', function () {
    //    datatable.search($(this).val(), 'Type');
    //});

    //$('#m_form_status, #m_form_type').selectpicker();

};

function loadLocation() {
    var options = $("#LocationId");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#loadLocationUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t,
                function () {
                    options.append($("<option />").val(this.id).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load location list.';
            alert(msg);
        }
    });
}

function loadParent() {
    var options = $("#ParentId");
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#loadParentUrl").val(),
        data: {},
        dataType: "html",
        success: function (result) {
            var t = JSON.parse(result);
            $.each(t,
                function () {
                    options.append($("<option />").val(this.id).text(this.name));
                });
        },
        error: function (error) {
            var msg = 'Fail to load parent list.';
            alert(msg);
        }
    });
}

function formValidation() {
    $("#btnSave").on("click",
        function () {
            if ($("#save_information")[0].checkValidity()) {
                saveData();
            } else {
                $("#save_information")[0].reportValidity();
            }
        });
}

function loadModalInInsertMode() {
    $("#Id").val("");
    $("#Name").val("");
    $("#Url").val("");
    $("#Order").val("");
    $("select#Status").val('Active');
    $("#m_maxlength_modal").modal();
}

function loadModalInEditMode(id) {
    $.ajax({
        type: "GET",
        cache: false,
        url: $("#getFooterListUrl").val(),
        data: { "Id": id },
        dataType: "html",
        success: function (result) {
            var parseData = JSON.parse(result);
            var footerLink = parseData.value;
            $("#Id").val(footerLink.id);
            $("#Name").val(footerLink.name);
            $("#Url").val(footerLink.url);
            $("#Order").val(footerLink.order);
            $("select#LocationId").val(footerLink.locationId);
            $("select#ParentId").val(footerLink.parentId);
            $("select#Status").val(footerLink.status);
            $("#m_maxlength_modal").modal();
        },
        error: function (error) {
            var msg = 'Fail to load footer link.';
            alert(msg);
        }
    });
}

function saveData() {
    blockUI_Modal();
    var fd = new FormData();
    fd.append("Id", $("#Id").val());
    fd.append("Name", $("#Name").val());
    fd.append("Url", $("#Url").val());
    fd.append("Order", $("#Order").val());
    fd.append("Status", $("#Status :selected").val());
    $.ajax({
        type: "POST",
        url: $("#insertUpdateUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $("#m_maxlength_modal .close").click();
            $("#Id").val("");
            $("#Name").val("");
            $("select#LocationId").val('');
            $("select#ParentId").val('');
            $("select#Status").val('Active');
            unblockUI_Modal();
            $('.footer_link_datatable').mDatatable().search('', 'Status');
        },
        failure: function (response) {
            unblockUI_Modal();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Modal();
        }
    });
}

function DeleteFooterLink(id) {
    blockUI_Page();
    var fd = new FormData();
    fd.append("Id", id);
    $.ajax({
        type: "DELETE",
        url: $("#deleteUrl").val(),
        data: fd,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.footer_link_datatable').mDatatable().search('', 'Status');
            if (data.success)
                alert(data.message);
            else
                alert(data.message);
            unblockUI_Page();
        },
        failure: function (response) {
            unblockUI_Page();
        },
        error: function (XMLHttpRequest, textStatus) {
            unblockUI_Page();
        }
    });
}